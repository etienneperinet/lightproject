package com.mffpdrf.lightproject.Events;

public interface OnMergeEventListener{
    void onMergeDone();
    void onMergeFailed(String error);
}
