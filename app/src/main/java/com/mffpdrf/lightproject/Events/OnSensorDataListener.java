package com.mffpdrf.lightproject.Events;

public interface OnSensorDataListener {
    void onLightDataChanged(Float lightValue);
    void onNewSensorAccuracyForLightChanged(Integer accuracy);
    void onPressureDataChanged(Float pressureValue);
    void onNewSensorAccuracyForPressureChanged(Integer accuracy);
    void onSensorForAngleChanged(Float angleX, Float angleY);
    void onNewSensorAccuracyForAngleChanged(Integer accuracy);
}
