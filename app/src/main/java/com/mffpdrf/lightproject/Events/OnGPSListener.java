package com.mffpdrf.lightproject.Events;

import android.location.Location;

public interface OnGPSListener {
    void onNewLocationDetected(Location locationResult) throws Exception;
}
