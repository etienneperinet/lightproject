package com.mffpdrf.lightproject.View.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.mffpdrf.lightproject.Constants.RequestCodeConstants;
import com.mffpdrf.lightproject.Controllers.Sensor.SensorController;
import com.mffpdrf.lightproject.Controllers.SettingsController;
import com.mffpdrf.lightproject.R;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class SettingsActivity extends AppCompatActivity {
    Button buttonBack;
    Button buttonApply;
    Button buttonGoToAdjust;

    EditText editTextFrequency;
    EditText editTextNumber;
    TextView textViewMaximumTime;
    EditText editTextMaximumOrientation;

    RadioButton automatic;
    RadioButton manual;

    SettingsController settingsController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        setActivityItems();
        settingsController = new SettingsController(this);
        loadSettings();
        setListener();

        if (!requestExternalStoragePermissions()) {
            buttonGoToAdjust.setEnabled(false);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == RequestCodeConstants.EXTERNAL_STORAGE_PERMISSIONS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                buttonGoToAdjust.setEnabled(true);
            }
        }
    }

    private void setActivityItems() {
        buttonBack = findViewById(R.id.activity_settings_btn_back);
        buttonApply = findViewById(R.id.activity_settings_btn_apply);
        buttonGoToAdjust = findViewById(R.id.activity_settings_btn_go_to_adjuster);

        editTextFrequency = findViewById(R.id.activity_settings_etn_recording_frequency);
        editTextNumber = findViewById(R.id.activity_settings_etn_recording_amount);
        textViewMaximumTime = findViewById(R.id.activity_settings_tv_recording_time_show);
        editTextMaximumOrientation = findViewById(R.id.activity_settings_et_maximum_orientation);

        automatic = findViewById(R.id.activity_settings_rb_automatic);
        manual = findViewById(R.id.activity_settings_rb_manual);
    }

    private void setListener(){
        setButtonListener();
        setEditTextListener();
    }


    private void setEditTextListener(){
        TextWatcher frequencyTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                validateFrequency();
            }
        };

        TextWatcher numberTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                validateNumber();
            }
        };

        TextWatcher maximumOrientationTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                validateMaximumOrientation();
            }
        };

        editTextNumber.addTextChangedListener(numberTextWatcher);
        editTextFrequency.addTextChangedListener(frequencyTextWatcher);
        editTextMaximumOrientation.addTextChangedListener(maximumOrientationTextWatcher);
    }


    private void setButtonListener(){
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { finishActivity(); }
        });
        buttonApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { applySettingsChanges(); }
        });
        buttonGoToAdjust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { goToAdjustActivity(); }
        });
    }

    private void validateFrequency() {
        if (validFrequency()) {
            //frequency = Integer.valueOf(editTextFrequency.getText().toString());
            editTextFrequency.setTextColor(Color.WHITE);
        } else {
            //editTextFrequency.setText(frequency.toString());
            editTextFrequency.setTextColor(Color.RED);
        }
        updateTextViewTime();
    }

    private Boolean validFrequency() {
        Boolean valid = false;
        String etFrequency = editTextFrequency.getText().toString();
        Integer frequency = 0;
        if (!etFrequency.isEmpty()) {
            frequency = Integer.valueOf(etFrequency);
            if (frequency >= 1000 && frequency <= 50000) {
                valid = true;
            }
        }
        return valid;
    }

    private void validateNumber() {
        if (validNumber()) {
            editTextNumber.setTextColor(Color.WHITE);
        } else {
            editTextNumber.setTextColor(Color.RED);
        }
        updateTextViewTime();
    }

    private Boolean validNumber() {
        Boolean valid = false;
        String etNumber = editTextNumber.getText().toString();
        Integer number = 0;
        if (!etNumber.isEmpty()) {
            number = Integer.valueOf(etNumber);
            if (number >= 1) {
                valid = true;
            }
        }
        return valid;
    }

    private void validateMaximumOrientation() {
        if (validMaximumOrientation()) {
            editTextMaximumOrientation.setTextColor(Color.WHITE);
        } else {
            editTextMaximumOrientation.setTextColor(Color.RED);
        }
    }

    private Boolean validMaximumOrientation() {
        Boolean valid = false;
        String etNumber = editTextMaximumOrientation.getText().toString();
        Integer number = 0;
        if (!etNumber.isEmpty()) {
            number = Integer.valueOf(etNumber);
            if (number >= 0 && number <= 180) {
                valid = true;
            }
        }
        return valid;
    }
    
    private void loadSettings(){
        editTextFrequency.setText( String.valueOf(SettingsController.getMillisecondsBetweenEachRecording()));
        editTextNumber.setText (String.valueOf(SettingsController.getNumberOfRecords()));
        textViewMaximumTime.setText(String.valueOf(settingsController.getTimeWithNumberOfRecord()) + " ms");
        editTextMaximumOrientation.setText(String.valueOf(SettingsController.getMaximumOrientation()));
        setCheckRadioButton();
    }

    private void checkRadioButtons(){
        if (automatic.isChecked()){
            SettingsController.setEndOfRecordingModeChosen(SettingsController.EndOfRecordingEnum.automatic);
        } else {
            SettingsController.setEndOfRecordingModeChosen(SettingsController.EndOfRecordingEnum.manual);
        }
    }

    private void setCheckRadioButton(){
        if (SettingsController.EndOfRecordingEnum.automatic == SettingsController.getEndOfRecordingModeChosen()){
            automatic.setChecked(true);
            manual.setChecked(false);
        } else {
            automatic.setChecked(false);
            manual.setChecked(true);
        }
    }

    private Boolean areEditTextFull(){
        Boolean areEditTextFull = true;
        if (editTextNumber.getText().equals("")){
            areEditTextFull = false;
            editTextNumber.setText (String.valueOf(SettingsController.getNumberOfRecords()));
        }
        if (editTextFrequency.getText().equals("")){
            areEditTextFull = false;
            editTextFrequency.setText(String.valueOf(SettingsController.getMillisecondsBetweenEachRecording()));
        }
        updateTextViewTime();
        return areEditTextFull;
    }

    private void updateTextViewTime(){
        textViewMaximumTime.setText(String.valueOf(getTimeWithNumberOfRecord()) + " ms");
    }

    public Integer getTimeWithNumberOfRecord(){
        Integer valueToReturn = 0;
        if (!editTextFrequency.getText().toString().equals("") && !editTextNumber.getText().toString().equals("")){
            valueToReturn = Integer.valueOf(editTextFrequency.getText().toString()) * Integer.valueOf(editTextNumber.getText().toString());
        }
        return valueToReturn;
    }

    private void applySettingsChanges(){
        checkRadioButtons();
        if (areEditTextFull()){
            if (validFrequency() && validNumber() && validMaximumOrientation()) {
                SettingsController.setMillisecondsBetweenEachRecording(Integer.valueOf(editTextFrequency.getText().toString()));
                SettingsController.setNumberOfRecords(Integer.valueOf(editTextNumber.getText().toString()));
                SettingsController.setMaximumOrientation(Integer.valueOf(editTextMaximumOrientation.getText().toString()));
                settingsController.createFile();
                Toast.makeText(this, "Changement enregistré!", Toast.LENGTH_LONG).show();
                finish();
            } else {
                if (!validFrequency()) {
                    Toast.makeText(this, "La fréquence doit être entre 250 et 10 000 milisecondes", Toast.LENGTH_LONG).show();
                }
                if (!validNumber()) {
                    Toast.makeText(this, "Le nombre d'enregistrement doit être supérieur à 1", Toast.LENGTH_LONG).show();
                }
                if (!validMaximumOrientation()) {
                    Toast.makeText(this, "Le degré maximum d'orientation doit être entre 0 et 180", Toast.LENGTH_LONG).show();
                }
            }
        } else {
            Toast.makeText(this, "Veuillez remplir toutes les zones de texte.", Toast.LENGTH_LONG).show();
        }
    }

    private Boolean requestExternalStoragePermissions() {
        Boolean permissionsGranted = false;
        List<String> permissionsRequest = getDeniedExternalStoragePermissions();

        if (!permissionsRequest.isEmpty()) {
            Toast.makeText(this, "Vous devez activer les permissions aux fichiers pour pouvoir importer un fichier d'ajustement de données", Toast.LENGTH_LONG).show();
            requestPermissions(permissionsRequest.toArray(new String[permissionsRequest.size()]), RequestCodeConstants.EXTERNAL_STORAGE_PERMISSIONS);
        } else {
            permissionsGranted = true;
        }

        return permissionsGranted;
    }

    private List<String> getDeniedExternalStoragePermissions() {
        List<String> permissionsDenied = new ArrayList<>();
        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            permissionsDenied.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            permissionsDenied.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        return permissionsDenied;
    }

    private void goToAdjustActivity() {
        Intent goToAdjustDataActivityIntent = new Intent(this, AdjustDataActivity.class);
        startActivity(goToAdjustDataActivityIntent);
    }

    private void finishActivity() {
        finish();
    }
}