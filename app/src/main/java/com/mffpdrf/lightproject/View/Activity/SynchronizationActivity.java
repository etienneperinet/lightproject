package com.mffpdrf.lightproject.View.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.mffpdrf.lightproject.Constants.BluetoothConstants;
import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Constants.RequestCodeConstants;
import com.mffpdrf.lightproject.Controllers.BluetoothController;
import com.mffpdrf.lightproject.Controllers.DatabaseController;
import com.mffpdrf.lightproject.Controllers.Sensor.DataProcessingController;
import com.mffpdrf.lightproject.Controllers.Sensor.SensorAdjusterController;
import com.mffpdrf.lightproject.Models.DatabaseModels.Cellphone;
import com.mffpdrf.lightproject.Models.DatabaseModels.ReferenceSession;
import com.mffpdrf.lightproject.R;
import com.mffpdrf.lightproject.Services.BluetoothConnectionService;
import com.mffpdrf.lightproject.Services.BluetoothServerService;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class SynchronizationActivity extends AppCompatActivity implements BluetoothServerService.OnReferenceDeviceConnectedListener, BluetoothConnectionService.OnReferenceSessionCreatedListener {
    Button buttonCancel;

    BluetoothController bluetoothController;

    BluetoothServerService bluetoothServerService;

    private BluetoothConstants.ConnectionMode connectionMode;

    private boolean connected = false;

    Handler handler;
    Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_synchronization);
        savedInstanceState = getIntent().getExtras();
        setConnectionMode(savedInstanceState);
        setPhoneInformation();
        buttonCancel = findViewById(R.id.activity_synchronization_btn_cancel);

        if (connectionMode == BluetoothConstants.ConnectionMode.sessionCreation || requestExternalStoragePermissions()) {
            if (requestLocationPermissions()) {
                tryStartingBluetooth();
            }
        }

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { finishActivity(); }
        });

        if (connectionMode == BluetoothConstants.ConnectionMode.sessionCreation) {
            SensorAdjusterController.askForCSV(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (connected) {
            finish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == RequestCodeConstants.EXTERNAL_STORAGE_PERMISSIONS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (requestLocationPermissions()) {
                    tryStartingBluetooth();
                }
            }
        } else if (requestCode == RequestCodeConstants.LOCATION_PERMISSIONS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                tryStartingBluetooth();
            }
        } else if (requestCode == RequestCodeConstants.BLUETOOTH_PERMISSIONS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startBluetooth();
            }
        }
    }

    private void setConnectionMode(Bundle savedInstanceState){
        String connectionModeString = savedInstanceState.getString(BluetoothConstants.INTENT_CONNECTION_MODE);
        if (connectionModeString != null) {
            if (connectionModeString.equals(BluetoothConstants.INTENT_SESSION_CREATION)) {
                connectionMode = BluetoothConstants.ConnectionMode.sessionCreation;
            } else {
                connectionMode = BluetoothConstants.ConnectionMode.dataMerging;
            }
        }
    }

    private void setPhoneInformation() {
        TextView tvPhoneModel = findViewById(R.id.activity_synchronization_tv_phone_model);
        tvPhoneModel.setText("(" + Settings.Secure.getString(getContentResolver(), "bluetooth_name") + ")");
    }

    private Boolean requestLocationPermissions() {
        Boolean permissionsGranted = false;
        List<String> permissionsRequest = getDeniedLocationPermissions();

        if (!permissionsRequest.isEmpty()) {
            Toast.makeText(this, "Vous devez activer les permissions de localisation pour pouvoir utiliser le Bluetooth", Toast.LENGTH_LONG).show();
            requestPermissions(permissionsRequest.toArray(new String[permissionsRequest.size()]), RequestCodeConstants.LOCATION_PERMISSIONS);
        } else {
            permissionsGranted = true;
        }

        return permissionsGranted;
    }

    private List<String> getDeniedLocationPermissions() {
        List<String> permissionsDenied = new ArrayList<>();
        if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionsDenied.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionsDenied.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        return permissionsDenied;
    }

    private Boolean requestBluetoothPermissions() {
        Boolean permissionsGranted = false;
        List<String> permissionsRequest = getDeniedBluetoothPermissions();

        if (!permissionsRequest.isEmpty()) {
            Toast.makeText(this, "Vous devez activer les permissions Bluetooth pour pouvoir connecter les téléphones", Toast.LENGTH_LONG).show();
            requestPermissions(permissionsRequest.toArray(new String[permissionsRequest.size()]), RequestCodeConstants.BLUETOOTH_PERMISSIONS);
        } else {
            permissionsGranted = true;
        }

        return permissionsGranted;
    }

    private List<String> getDeniedBluetoothPermissions() {
        List<String> permissionsDenied = new ArrayList<>();
        if (checkSelfPermission(Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED) {
            permissionsDenied.add(Manifest.permission.BLUETOOTH);
        }
        if (checkSelfPermission(Manifest.permission.BLUETOOTH_ADMIN) != PackageManager.PERMISSION_GRANTED) {
            permissionsDenied.add(Manifest.permission.BLUETOOTH_ADMIN);
        }
        return permissionsDenied;
    }

    private Boolean requestExternalStoragePermissions() {
        Boolean permissionsGranted = false;
        List<String> permissionsRequest = getDeniedExternalStoragePermissions();

        if (!permissionsRequest.isEmpty()) {
            Toast.makeText(this, "Vous devez activer les permissions aux fichiers pour pouvoir transférer les données", Toast.LENGTH_LONG).show();
            requestPermissions(permissionsRequest.toArray(new String[permissionsRequest.size()]), RequestCodeConstants.EXTERNAL_STORAGE_PERMISSIONS);
        } else {
            permissionsGranted = true;
        }

        return permissionsGranted;
    }

    private List<String> getDeniedExternalStoragePermissions() {
        List<String> permissionsDenied = new ArrayList<>();
        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            permissionsDenied.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            permissionsDenied.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        return permissionsDenied;
    }

    private void finishActivity() {
        stopBluetooth();
        finish();
    }

    private void tryStartingBluetooth() {
        if (requestBluetoothPermissions()) {
            startBluetooth();
        }
    }

    private void startBluetooth() {
        bluetoothController = new BluetoothController(this);
        bluetoothController.enableBluetooth();
        bluetoothController.enablePhoneDiscoverability();
        bluetoothServerService = BluetoothServerService.getInstance(this);
        bluetoothServerService.setOnServerConnectedListener(this);
    }

    private void stopBluetooth() {
        if (bluetoothController != null) {
            bluetoothController.stop();
        }
        if (bluetoothServerService != null) {
            bluetoothServerService.destroyInstance();
        }
    }

    private void goToReferenceIdleActivity(){
        stopBluetooth();

        Intent goToReferenceIdleActivityIntent = new Intent(this, ReferenceIdleActivity.class);
        startActivity(goToReferenceIdleActivityIntent);
    }

    private void goToMergeDataActivity(){
        Intent goToMergeDataActivityIntent = new Intent(this, MergeDataActivity.class);

        goToMergeDataActivityIntent.putExtra(BluetoothConstants.INTENT_CELLPHONE_MODE, BluetoothConstants.INTENT_REFERENCE);
        startActivity(goToMergeDataActivityIntent);
    }

    private void createSession() {
        Cellphone cellphone = new Cellphone(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
        ReferenceSession referenceSession = new ReferenceSession();
        referenceSession.setDate(LocalDate.now());
        referenceSession.setCellphone(cellphone);

        DatabaseController databaseController = DatabaseController.getInstance(this);
        databaseController.addCellphone(cellphone);
        Long referenceSessionID = databaseController.addReferenceSession(referenceSession);
        referenceSession.setID(referenceSessionID);

        bluetoothServerService.write(referenceSession.toJSON().toString().getBytes());
    }

    @Override
    public void onConnectionReference() {
        connected = true;
        if (connectionMode == BluetoothConstants.ConnectionMode.sessionCreation) {
            bluetoothServerService.setSessionCreatedListener(this);
            createSession();
        } else {
            goToMergeDataActivity();
        }
    }

    @Override
    public void onSessionCreated(Long timeToWait) {
        handler = new Handler(Looper.getMainLooper());
        runnable = new Runnable() {
            @Override
            public void run() {
                startTimer();
                goToReferenceIdleActivity();
            }
        };
        handler.postDelayed(runnable, timeToWait);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startTimer(){
        DataProcessingController dataProcessingController = DataProcessingController.getInstance(this, DatabaseConstants.CellphoneMode.reference);
        dataProcessingController.turnOffRecording();
    }
}