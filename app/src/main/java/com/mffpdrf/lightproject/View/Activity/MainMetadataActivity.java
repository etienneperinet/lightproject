package com.mffpdrf.lightproject.View.Activity;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Controllers.CellphoneModeFileController;
import com.mffpdrf.lightproject.Controllers.DatabaseController;
import com.mffpdrf.lightproject.Controllers.GPSController;
import com.mffpdrf.lightproject.Controllers.Sensor.DataProcessingController;
import com.mffpdrf.lightproject.Controllers.Sensor.SensorController;
import com.mffpdrf.lightproject.Models.DatabaseModels.Site;
import com.mffpdrf.lightproject.R;
import com.mffpdrf.lightproject.View.Dialog.PreparationToRecordDataDialog;
import com.mffpdrf.lightproject.View.SensorDataListenerDisplay;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainMetadataActivity extends AppCompatActivity implements PreparationToRecordDataDialog.OnStartRecordingButtonPressListener {
    private static final Integer READ_REQUEST_CODE = 24;
    private Integer dataSavingTypeChosen;

    private Button buttonMobileRecording;
    private Button buttonImmobileRecording;
    private Button buttonUpdateData;
    private Button buttonStopSession;

    private EditText etSite;
    private EditText etPrefix;
    private EditText etMicrosite;
    private EditText etNote;

    private DatabaseController databaseController;

    SensorDataListenerDisplay dataView;
    SensorController sensorController;

    GPSController gpsController ;
    private double latitude;
    private double longitude;

    PreparationToRecordDataDialog dialog;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_metadata);

        sensorController = SensorController.getInstance(this);
        dataView = SensorDataListenerDisplay.getInstance(this, (TextView) findViewById(R.id.activity_main_metadata_tv_light));
        sensorController.setOnSensorDataListener(dataView);
        databaseController = DatabaseController.getInstance(this);
        gpsController = new GPSController(this);

        CellphoneModeFileController cellphoneModeFileController = new CellphoneModeFileController(this);
        cellphoneModeFileController.writeFile(DatabaseConstants.CellphoneMode.main);

        setButtons();
        setEditTexts();
        AskForPermission();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //removeNote();
        //removePrefix();
        dataView = SensorDataListenerDisplay.getInstance(this, (TextView) findViewById(R.id.activity_main_metadata_tv_light));
        dataView.deactivateAlarm();
        sensorController.setOnSensorDataListener(dataView);
    }

    private void loadNewMicrositeData(){
        String microsite = etMicrosite.getText().toString();
        if (etMicrosite.getText() != null && !microsite.equals("")) {
            Integer micrositeInteger = Integer.parseInt(microsite);
            micrositeInteger = micrositeInteger + 1;
            microsite = micrositeInteger.toString();
            etMicrosite.setText(microsite);
        }
    }

    private void removeNote(){
        if (etNote.getText() != null) {
            etNote.setText("");
        }
    }

    private void removePrefix(){
        if (etPrefix.getText() != null) {
            etPrefix.setText("");
        }
    }




    private void AskForPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }
    }

    private void setButtons(){
        //buttonSettings = findViewById(R.id.activity_main_metadata_btn_settings);

        buttonUpdateData = findViewById(R.id.activity_main_metadata_btn_update_data);
        buttonStopSession = findViewById(R.id.activity_main_metadata_btn_end_session);

        buttonMobileRecording = findViewById(R.id.activity_main_metadata_btn_mobile_recording);
        buttonImmobileRecording = findViewById(R.id.activity_main_metadata_btn_immobile_recording);

        setListener();
    }

    private void setEditTexts(){
        etSite = findViewById(R.id.activity_main_metadata_et_site);
        etPrefix = findViewById(R.id.activity_main_metadata_et_prefix);
        etMicrosite = findViewById(R.id.activity_main_metadata_etn_microsite);
        etNote = findViewById(R.id.activity_main_metadata_et_note);
    }

    private void setListener(){
        buttonUpdateData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { goToMainDataUpdateActivity(); }
        });

        buttonStopSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { endSession(); }
        });

        buttonMobileRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { createDialogToStartRecording(1); }
        });

        buttonImmobileRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {  createDialogToStartRecording(2); }
        });
    }

    private void goToSettingsActivity(){
        Intent goToSettingsActivityIntent = new Intent(this, SettingsActivity.class);
        startActivity(goToSettingsActivityIntent);
    }

    private void goToMainDataUpdateActivity(){
        Intent goToMainDataUpdateActivityIntent = new Intent(this, MainUpdateDataActivity.class);
        startActivity(goToMainDataUpdateActivityIntent);
    }

    private void endSession(){
        DataProcessingController.destroyInstance();
        finish();
    }

    private boolean checkEditText(){
        Boolean notNullFiledOk = true;

        if (etSite.getText().toString().trim().isEmpty()){
            notNullFiledOk = false;
        }else if (etMicrosite.getText().toString().trim().isEmpty()){
            notNullFiledOk = false;
        }

        return notNullFiledOk;
    }

    private void createDialogToStartRecording(Integer data_saving_type){
        if (checkEditText()){
            dataSavingTypeChosen = data_saving_type;
            dialog = new PreparationToRecordDataDialog(this);
            dialog.setListener(this);
            askForLocation();
        }else {
            Toast.makeText(this, "Veuillez remplir les zones de texte nommées Site et Microsite.", Toast.LENGTH_SHORT).show();
        }
    }

    private void askForLocation(){
        gpsController.callAfterLocationUpdate(new GPSController.afterLocationUpdate() {
            @Override
            public void onLocationUpdate(Location locationResult) {
                longitude = locationResult.getLongitude();
                latitude = locationResult.getLatitude();
            }
        });
    }

    @Override
    public void onStartRecordingButtonPress() {
        dialog.closeDialog();
        Site siteUsed = databaseController.getSite(etSite.getText().toString());
        if (siteUsed == null){
            siteUsed = new Site();
            siteUsed.setID(databaseController.addSite(etSite.getText().toString()));
        }

        DataProcessingController dataProcessingController = DataProcessingController.getInstance(this, DatabaseConstants.CellphoneMode.main);
        dataProcessingController.setIDToUseOnRecord(databaseController.addMicrosite(etPrefix.getText().toString(), Integer.parseInt(etMicrosite.getText().toString()), etNote.getText().toString(), dataSavingTypeChosen, siteUsed.getID(), latitude, longitude));
        goToMainDataRecordingActivity();
    }

    private void goToMainDataRecordingActivity(){
        Intent goToMainDataRecordingActivityIntent = new Intent(this, MainDataRecordingActivity.class);
        startActivityForResult(goToMainDataRecordingActivityIntent, READ_REQUEST_CODE);

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            loadNewMicrositeData();
        } else if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_CANCELED){
            Toast.makeText(this, "Les données du microsite #" + etMicrosite.getText().toString() + " ont bien été détruite", Toast.LENGTH_LONG);
        }
    }

}