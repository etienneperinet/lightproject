package com.mffpdrf.lightproject.View.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mffpdrf.lightproject.Constants.BluetoothConstants;
import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Constants.RequestCodeConstants;
import com.mffpdrf.lightproject.Controllers.BluetoothController;
import com.mffpdrf.lightproject.Controllers.DatabaseController;
import com.mffpdrf.lightproject.Controllers.Sensor.DataProcessingController;
import com.mffpdrf.lightproject.Controllers.Sensor.SensorAdjusterController;
import com.mffpdrf.lightproject.Models.DatabaseModels.Cellphone;
import com.mffpdrf.lightproject.Models.DatabaseModels.MainSession;
import com.mffpdrf.lightproject.Models.DatabaseModels.Observer;
import com.mffpdrf.lightproject.Models.DatabaseModels.Project;
import com.mffpdrf.lightproject.Models.DatabaseModels.ReferenceSession;
import com.mffpdrf.lightproject.R;
import com.mffpdrf.lightproject.Services.BluetoothClientService;
import com.mffpdrf.lightproject.View.RecyclerView.RecyclerViewAdapterPhones;

import android.Manifest;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class PhoneSearchActivity extends AppCompatActivity implements RecyclerViewAdapterPhones.OnItemListener, BluetoothController.OnBluetoothChangeListener, BluetoothClientService.OnClientDeviceConnectedListener
{
    Button buttonCancel;
    Button buttonSynchronize;

    BluetoothController bluetoothController;

    BluetoothClientService bluetoothClientService;

    public RecyclerView recyclerView;
    private RecyclerView.Adapter recyclerViewAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private Integer selectedDeviceIndex;

    private View selectedView;

    private BluetoothConstants.ConnectionMode connectionMode;

    private boolean connected = false;

    Handler handler;
    Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_search);
        savedInstanceState = getIntent().getExtras();
        setConnectionMode(savedInstanceState);
        bluetoothController = new BluetoothController(this);

        if (requestLocationPermissions()) {
            tryStartingBluetooth();
        }

        setLayoutItems();
        setListener();
        if (connectionMode == BluetoothConstants.ConnectionMode.sessionCreation) {
            SensorAdjusterController.askForCSV(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (connected) {
            finish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == RequestCodeConstants.LOCATION_PERMISSIONS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                tryStartingBluetooth();
            }
        } else if (requestCode == RequestCodeConstants.BLUETOOTH_PERMISSIONS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startBluetooth();
            }
        }
    }

    private void setConnectionMode(Bundle savedInstanceState){
        String connectionModeString = savedInstanceState.getString(BluetoothConstants.INTENT_CONNECTION_MODE);
        if (connectionModeString.equals(BluetoothConstants.INTENT_DATA_MERGING)) {
            connectionMode = BluetoothConstants.ConnectionMode.dataMerging;
            Log.d("connectionMode", "setConnectionMode: dataMerging");
        } else {
            connectionMode = BluetoothConstants.ConnectionMode.sessionCreation;
            Log.d("connectionMode", "setConnectionMode: sessionCreation");
        }
    }

    private void setLayoutItems(){
        buttonCancel = findViewById(R.id.activity_phone_search_btn_cancel);
        buttonSynchronize = findViewById(R.id.activity_phone_search_btn_synchronize);

        recyclerView = findViewById(R.id.activity_phone_search_rv_phone_list);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        recyclerViewAdapter = new RecyclerViewAdapterPhones(bluetoothController.phonesDiscovered, this, this);
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    private void setListener(){
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { finishActivity(); }
        });

        buttonSynchronize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedDeviceIndex != null) {
                    synchronizePhone();
                } else {
                    Toast.makeText(PhoneSearchActivity.this, "Vous devez sélectionner un appareil", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void synchronizePhone() {
        connected = true;
        BluetoothDevice bluetoothDevice = bluetoothController.phonesDiscovered.get(selectedDeviceIndex);
        bluetoothDevice.createBond();
        bluetoothClientService = BluetoothClientService.getInstance(this, bluetoothDevice);
        bluetoothClientService.setOnClientConnectedListener(this);
    }

    private void createSession(ReferenceSession referenceSession) {
        DatabaseController databaseController = DatabaseController.getInstance(this);

        databaseController.addReferenceSession(referenceSession);

        Project project = databaseController.getSelectedProject();

        Cellphone cellphone = new Cellphone(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
        databaseController.addCellphone(cellphone);

        databaseController.addCellphone(referenceSession.getCellphone());

        Observer observer = databaseController.getSelectedObserver();

        MainSession mainSession = new MainSession(project, cellphone, observer, referenceSession);

        Long mainSessionID = databaseController.addMainSession(mainSession);
        mainSession.setID(mainSessionID);
        databaseController.setSelectedMainSession(mainSession);
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    private void finishActivity() {
        stopBluetooth();
        finish();
    }

    private void tryStartingBluetooth() {
        if (requestBluetoothPermissions()) {
            startBluetooth();
        }
    }

    private void startBluetooth() {
        bluetoothController.enableBluetooth();
        bluetoothController.discoverPhones();
        bluetoothController.setOnBluetoothChange(this);
    }

    private void stopBluetooth() {
        bluetoothController.stop();
        if (bluetoothClientService != null) {
            bluetoothClientService.destroyInstance();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onItemClick(Integer position, View view) {
        if (selectedView != null) {
            selectedView.setBackground(null);
        }
        selectedView = view;
        selectedView.setBackgroundColor(getResources().getColor(R.color.grey_700, getTheme()));
        selectedView.getBackground().setAlpha(75);
        selectedDeviceIndex = position;
    }

    @Override
    public void onPhoneDiscovered(BluetoothDevice device) {
        recyclerViewAdapter.notifyDataSetChanged();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startTimer(){
        DataProcessingController dataProcessingController = DataProcessingController.getInstance(this, DatabaseConstants.CellphoneMode.main);
        dataProcessingController.turnOffRecording();
    }

    @Override
    public void onSessionCreation(ReferenceSession referenceSession, Long timeToWait) {
        createSession(referenceSession);

        handler = new Handler(Looper.getMainLooper());
        runnable = new Runnable() {
            @Override
            public void run() {
                startTimer();
                goToMainMetadataActivity();
            }
        };
        handler.postDelayed(runnable, timeToWait);
    }

    @Override
    public void onClientSideConnection() {
        String TAG = "devicesConnected";
        Log.d(TAG, "onClientSideConnection: " + connectionMode);
        if (connectionMode == BluetoothConstants.ConnectionMode.dataMerging){
            goToMergeDataActivity();
        }
    }

    private Boolean requestLocationPermissions() {
        Boolean permissionsGranted = false;
        List<String> permissionsRequest = getDeniedLocationPermissions();

        if (!permissionsRequest.isEmpty()) {
            Toast.makeText(this, "Vous devez activer les permissions de localisation pour pouvoir utiliser le Bluetooth", Toast.LENGTH_LONG).show();
            requestPermissions(permissionsRequest.toArray(new String[permissionsRequest.size()]), RequestCodeConstants.LOCATION_PERMISSIONS);
        } else {
            permissionsGranted = true;
        }

        return permissionsGranted;
    }

    private List<String> getDeniedLocationPermissions() {
        List<String> permissionsDenied = new ArrayList<>();
        if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionsDenied.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionsDenied.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        return permissionsDenied;
    }

    private Boolean requestBluetoothPermissions() {
        Boolean permissionsGranted = false;
        List<String> permissionsRequest = getDeniedBluetoothPermissions();

        if (!permissionsRequest.isEmpty()) {
            Toast.makeText(this, "Vous devez activer les permissions Bluetooth pour pouvoir connecter les téléphones", Toast.LENGTH_LONG).show();
            requestPermissions(permissionsRequest.toArray(new String[permissionsRequest.size()]), RequestCodeConstants.BLUETOOTH_PERMISSIONS);
        } else {
            permissionsGranted = true;
        }

        return permissionsGranted;
    }

    private List<String> getDeniedBluetoothPermissions() {
        List<String> permissionsDenied = new ArrayList<>();
        if (checkSelfPermission(Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED) {
            permissionsDenied.add(Manifest.permission.BLUETOOTH);
        }
        if (checkSelfPermission(Manifest.permission.BLUETOOTH_ADMIN) != PackageManager.PERMISSION_GRANTED) {
            permissionsDenied.add(Manifest.permission.BLUETOOTH_ADMIN);
        }
        return permissionsDenied;
    }

    private void goToMainMetadataActivity() {
        stopBluetooth();
        Intent goToMainMetadataActivityIntent = new Intent(this, MainMetadataActivity.class);
        startActivity(goToMainMetadataActivityIntent);
    }

    private void goToMergeDataActivity() {
        Intent goToMergeDataActivityIntent = new Intent(this, MergeDataActivity.class);
        goToMergeDataActivityIntent.putExtra(BluetoothConstants.INTENT_CELLPHONE_MODE, BluetoothConstants.INTENT_MAIN);
        startActivity(goToMergeDataActivityIntent);
    }
}