package com.mffpdrf.lightproject.View.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.Controllers.DatabaseController;
import com.mffpdrf.lightproject.Models.DatabaseModels.GraphMultiplierPoint;
import com.mffpdrf.lightproject.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class AdjustDataActivity extends AppCompatActivity {
    private static final Integer PERMISSION_REQUEST_STORAGE = 69;
    private static final Integer READ_REQUEST_CODE = 42;
    private static final String TAG = "AdjustDataActivity";
    private Dialog dialog;

    Button buttonBack;
    Button buttonFindCSV;
    TextView textViewInformationButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adjust_data);
        buttonBack = findViewById(R.id.activity_adjust_data_btn_back);
        buttonFindCSV = findViewById(R.id.activity_adjust_data_btn_load_csv_file);
        textViewInformationButton = findViewById(R.id.textViewInformationButton);
        setListener();
    }

    private void setListener() {
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishActivity();
            }
        });

        buttonFindCSV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lookForTheFile();
            }
        });

        textViewInformationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayDialog();
            }
        });
    }

    private void displayDialog(){
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_information_csv);
        dialog.setTitle("CSV Information");

        Button ok = dialog.findViewById(R.id.buttonOk);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void lookForTheFile(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_STORAGE);
            }else {
                performFileSearch();
            }
        }
    }

    private void performFileSearch(){
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("text/*");
        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if ( data != null ){
                Uri uri = data.getData();
                useFile(uri);
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void useFile(Uri uri){
        try {
            InputStream inputStream = getContentResolver().openInputStream(uri);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, Charset.defaultCharset()));
            String line = "";
            List<GraphMultiplierPoint> points = new ArrayList<>();
            try {
                Boolean multiplierIsSecondColumn = isSecondColumnMultiplier(reader.readLine());
                if (multiplierIsSecondColumn == null){
                    Toast.makeText(this, "Veuillez mettre un titre significatif à vos colones dans le csv.", Toast.LENGTH_LONG).show();
                }
                while ((line = reader.readLine()) != null){
                    points.add(readPoint(line, multiplierIsSecondColumn));
                }

                if (points.size() > 0) {
                    insertIntoBD(points);
                }

            }catch (IOException e){
                Toast.makeText(this, "Erreur lors de la lecture du fichier sur la ligne : " + line +".", Toast.LENGTH_LONG).show();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Boolean isSecondColumnMultiplier(String titlesOfColumns){
        Boolean answer = null;
        try {
            String[] tokens = csvLineSplit(titlesOfColumns);
            if (tokens[1].toUpperCase().contains("MULTI")) {
                answer = true;
            }else if (tokens[0].toUpperCase().contains("MULTI")){
                answer = false;
            }else {
                answer = null;
            }
        }catch (Exception e){
            Toast.makeText(this, "Erreur lors de la lecture du fichier." , Toast.LENGTH_LONG);
        }

        return answer;
    }

    private String[] csvLineSplit(String line){
        String splitter = "";
        String[] tokens = null;
        if (line.contains(":")){
            splitter = ":";
        } else if (line.contains(" ")){
            splitter = " ";
        } else if (line.contains("|")){
            splitter = "|";
        } else if (line.contains(";")){
            splitter = ";";
        } else if (line.contains(",")) {
            splitter = ",";
        } else {
            Toast.makeText(this, "Erreur lors de la lecture du titre du fichier." , Toast.LENGTH_LONG);
        }
        tokens = line.split(splitter);
        for (int i = 0 ; i<tokens.length ; i++){
            tokens[i] = tokens[i].replace(",", ".");
        }
        return tokens;
    }

    private GraphMultiplierPoint readPoint(String line, Boolean multiplierIsSecondColumn){
        GraphMultiplierPoint point = null;
        try {
            String[] tokens = csvLineSplit(line);
            if (multiplierIsSecondColumn){
                point =  new GraphMultiplierPoint(Float.parseFloat(tokens[0]), Float.parseFloat(tokens[1]));
            }else {
                point =  new GraphMultiplierPoint(Float.parseFloat(tokens[1]), Float.parseFloat(tokens[0]));
            }
            Log.d(TAG, "readPoint : " + point.toString());
        } catch (Exception e){
            Toast.makeText(this, "Fichier CSV impossible à lire, veuillez vous assurer que le séparateur figure parmis ces caractère: \"| , ; : [espace]\".", Toast.LENGTH_LONG).show();
        }
        return point;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void insertIntoBD(List<GraphMultiplierPoint> points){
        DatabaseController databaseController = DatabaseController.getInstance(this);
        Long idGraph = databaseController.addGraphMultiplier(LocalDateTime.now(), Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
        for (GraphMultiplierPoint point : points){
            databaseController.addGraphPointMultiplier(point.getDataMultiplier(), point.getDataSeen(), idGraph);
        }
        Toast.makeText(this, String.valueOf(points.size()) + " multiplicateurs ajoutés!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_STORAGE){
            lookForTheFile();
        }
    }

    private void finishActivity() {
        finish();
    }
}