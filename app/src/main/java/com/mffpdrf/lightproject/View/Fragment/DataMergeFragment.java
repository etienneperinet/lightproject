package com.mffpdrf.lightproject.View.Fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.Toast;

import com.mffpdrf.lightproject.Constants.BluetoothConstants;
import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Controllers.CellphoneModeFileController;
import com.mffpdrf.lightproject.R;
import com.mffpdrf.lightproject.View.Activity.InitialActivity;
import com.mffpdrf.lightproject.View.Activity.PhoneSearchActivity;
import com.mffpdrf.lightproject.View.Activity.SynchronizationActivity;

import java.util.ArrayList;
import java.util.List;

public class DataMergeFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private RadioButton rbDataKeeper;
    private RadioButton rbDataSender;

    public DataMergeFragment() {
        // Required empty public constructor
    }

    public static DataMergeFragment newInstance(String param1, String param2) {
        DataMergeFragment fragment = new DataMergeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_data_merge, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rbDataKeeper = getView().findViewById(R.id.fragment_data_merge_rb_data_keeper);
        rbDataSender = getView().findViewById(R.id.fragment_data_merge_rb_data_sender);
        disableFeatures();
    }

    public void disableFeatures() {
        CellphoneModeFileController cellphoneModeFileController = new CellphoneModeFileController(getContext());
        DatabaseConstants.CellphoneMode cellphoneMode = cellphoneModeFileController.readFile();

        if (cellphoneMode != null) {
            if (cellphoneMode == DatabaseConstants.CellphoneMode.main) {
                rbDataSender.setChecked(true);
                rbDataSender.setEnabled(true);
                rbDataKeeper.setEnabled(false);
            } else if (cellphoneMode == DatabaseConstants.CellphoneMode.reference) {
                rbDataKeeper.setChecked(true);
                rbDataKeeper.setEnabled(true);
                rbDataSender.setEnabled(false);
            }
        }
    }

    public void goToSpecificActivity() {
        if (rbDataKeeper.isChecked()) {
            goToSynchronizationActivity();
        } else {
            goToPhoneSearchActivity();
        }
    }

    private void goToPhoneSearchActivity(){
        Intent goToPhoneSearchActivityIntent = new Intent(getContext(), PhoneSearchActivity.class);
        goToPhoneSearchActivityIntent.putExtra(BluetoothConstants.INTENT_CONNECTION_MODE, BluetoothConstants.INTENT_DATA_MERGING);
        startActivity(goToPhoneSearchActivityIntent);
    }

    private void goToSynchronizationActivity() {
        Intent goToSynchronizationActivityIntent = new Intent(getContext(), SynchronizationActivity.class);
        goToSynchronizationActivityIntent.putExtra(BluetoothConstants.INTENT_CONNECTION_MODE, BluetoothConstants.INTENT_DATA_MERGING);
        startActivity(goToSynchronizationActivityIntent);
    }
}