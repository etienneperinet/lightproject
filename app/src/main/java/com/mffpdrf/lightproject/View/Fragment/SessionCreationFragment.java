package com.mffpdrf.lightproject.View.Fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mffpdrf.lightproject.Constants.BluetoothConstants;
import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Constants.RequestCodeConstants;
import com.mffpdrf.lightproject.Controllers.CellphoneModeFileController;
import com.mffpdrf.lightproject.Controllers.DatabaseController;
import com.mffpdrf.lightproject.Models.DatabaseModels.Observer;
import com.mffpdrf.lightproject.Models.DatabaseModels.Project;
import com.mffpdrf.lightproject.R;
import com.mffpdrf.lightproject.View.Activity.ObserverCreationActivity;
import com.mffpdrf.lightproject.View.Activity.PhoneSearchActivity;
import com.mffpdrf.lightproject.View.Activity.ProjectCreationActivity;
import com.mffpdrf.lightproject.View.Activity.SynchronizationActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SessionCreationFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private TextView tvObserver;
    private TextView tvProject;

    private Button btnCreateProject;
    private Button btnCreateObserver;

    private Spinner spinnerObservers;
    private Spinner spinnerProjects;

    private RadioGroup rgDataSavingType;
    private RadioButton rbMainPhone;
    private RadioButton rbReferencePhone;

    public SessionCreationFragment() {
        // Required empty public constructor
    }

    public static SessionCreationFragment newInstance(String param1, String param2) {
        SessionCreationFragment fragment = new SessionCreationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_session_creation, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvObserver = getView().findViewById(R.id.fragment_session_creation_tv_observer);
        tvProject = getView().findViewById(R.id.fragment_session_creation_tv_project);
        spinnerObservers = getView().findViewById(R.id.fragment_session_creation_spn_observer);
        spinnerProjects = getView().findViewById(R.id.fragment_session_creation_spn_project);
        btnCreateProject = getView().findViewById(R.id.fragment_session_creation_btn_project);
        btnCreateObserver = getView().findViewById(R.id.fragment_session_creation_btn_observer);
        rgDataSavingType = getView().findViewById(R.id.fragment_session_creation_rbg_data_saving_type);
        rbMainPhone = getView().findViewById(R.id.fragment_session_creation_rb_main_phone);
        rbReferencePhone = getView().findViewById(R.id.fragment_session_creation_rb_reference_phone);

        btnCreateProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToProjectCreationActivity();
            }
        });

        btnCreateObserver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToObserverCreationActivity();
            }
        });

        rgDataSavingType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                Log.d("TAG", "onCheckedChanged: " + checkedId);

                View radioButton = rgDataSavingType.findViewById(checkedId);
                int index = rgDataSavingType.indexOfChild(radioButton);

                switch (index) {
                    case 0:
                        enableObjects();
                        break;
                    case 1:
                        disableObjects();
                        break;
                }
            }
        });

        populateSpinners();
        disableFeatures();
    }

    @Override
    public void onResume() {
        super.onResume();
        populateSpinners();
    }

    private void enableObjects() {
        tvObserver.setEnabled(true);
        tvProject.setEnabled(true);
        spinnerObservers.setEnabled(true);
        spinnerProjects.setEnabled(true);
        btnCreateObserver.setEnabled(true);
        btnCreateProject.setEnabled(true);
    }

    private void disableObjects() {
        tvObserver.setEnabled(false);
        tvProject.setEnabled(false);
        spinnerObservers.setEnabled(false);
        spinnerProjects.setEnabled(false);
        btnCreateObserver.setEnabled(false);
        btnCreateProject.setEnabled(false);
    }

    public void populateSpinners(){
        DatabaseController databaseController;
        databaseController = DatabaseController.getInstance(getContext());

        File databaseFile = getContext().getDatabasePath(DatabaseConstants.DATABASE_NAME);

        if (databaseFile.exists()) {
            ArrayAdapter<Observer> observersAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, databaseController.getObservers());
            observersAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerObservers.setAdapter(observersAdapter);

            ArrayAdapter<Project> projectsAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, databaseController.getProjects());
            projectsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerProjects.setAdapter(projectsAdapter);
        } else {
            spinnerObservers.setAdapter(null);
            spinnerProjects.setAdapter(null);
        }
    }

    public void disableFeatures() {
        CellphoneModeFileController cellphoneModeFileController = new CellphoneModeFileController(getContext());
        DatabaseController databaseController = DatabaseController.getInstance(getContext());
        DatabaseConstants.CellphoneMode cellphoneMode = cellphoneModeFileController.readFile();

        if (databaseController.requiresMerge()) {
            if (cellphoneMode != null) {
                if (cellphoneMode == DatabaseConstants.CellphoneMode.main) {
                    rbMainPhone.setChecked(true);
                    rbReferencePhone.setEnabled(false);
                } else if (cellphoneMode == DatabaseConstants.CellphoneMode.reference) {
                    rbReferencePhone.setChecked(true);
                    rbMainPhone.setEnabled(false);
                }
            }
        } else {
            rbMainPhone.setEnabled(true);
            rbReferencePhone.setEnabled(true);
        }
    }

    public void goToSpecificActivity() {
        if (rbMainPhone.isChecked()) {
            if (spinnerProjects.getAdapter() != null && spinnerObservers.getAdapter() != null && spinnerProjects.getAdapter().getCount() != 0 && spinnerObservers.getAdapter().getCount() != 0) {
                DatabaseController databaseController = DatabaseController.getInstance(getContext());
                databaseController.setSelectedObserver((Observer)spinnerObservers.getSelectedItem());
                databaseController.setSelectedProject((Project)spinnerProjects.getSelectedItem());
                goToPhoneSearchActivity();
            } else {
                Toast.makeText(getContext(), "Vous devez sélectionner un projet et un observateur", Toast.LENGTH_LONG).show();
            }
        } else {
            goToSynchronizationActivity();
        }
    }

    private void goToPhoneSearchActivity(){
        Intent goToPhoneSearchActivityIntent = new Intent(getContext(), PhoneSearchActivity.class);
        goToPhoneSearchActivityIntent.putExtra(BluetoothConstants.INTENT_CONNECTION_MODE, BluetoothConstants.INTENT_SESSION_CREATION);
        startActivity(goToPhoneSearchActivityIntent);
    }

    private void goToSynchronizationActivity() {
        Intent goToSynchronizationActivityIntent = new Intent(getContext(), SynchronizationActivity.class);
        goToSynchronizationActivityIntent.putExtra(BluetoothConstants.INTENT_CONNECTION_MODE, BluetoothConstants.INTENT_SESSION_CREATION);
        startActivity(goToSynchronizationActivityIntent);
    }

    private void goToProjectCreationActivity(){
        Intent goToProjectCreationActivityIntent = new Intent(getContext(), ProjectCreationActivity.class);
        startActivity(goToProjectCreationActivityIntent);
    }

    private void goToObserverCreationActivity(){
        Intent goToObserverCreationActivityIntent = new Intent(getContext(), ObserverCreationActivity.class);
        startActivity(goToObserverCreationActivityIntent);
    }
}