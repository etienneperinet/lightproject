package com.mffpdrf.lightproject.View.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.mffpdrf.lightproject.Constants.BluetoothConstants;
import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.Controllers.CellphoneModeFileController;
import com.mffpdrf.lightproject.Controllers.DatabaseController;
import com.mffpdrf.lightproject.Controllers.Merger.MergerFileController;
import com.mffpdrf.lightproject.Events.OnMergeEventListener;
import com.mffpdrf.lightproject.R;
import com.mffpdrf.lightproject.Services.BluetoothClientService;
import com.mffpdrf.lightproject.Services.BluetoothServerService;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MergeDataActivity extends AppCompatActivity implements OnMergeEventListener {
    Dialog dialog;
    Button btnCancel;
    DatabaseConstants.CellphoneMode cellphoneMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        super.onCreate(savedInstanceState);

        savedInstanceState = getIntent().getExtras();
        setCellphoneMode(savedInstanceState);

        setContentView(R.layout.activity_merge_data);
        btnCancel = findViewById(R.id.activity_merge_data_btn_cancel);

        btnCancel.setEnabled(false);
        btnCancel.setVisibility(View.INVISIBLE);

        setListener();
    }

    private void setCellphoneMode(Bundle savedInstanceState) {
        String connectionModeString = savedInstanceState.getString(BluetoothConstants.INTENT_CELLPHONE_MODE);
        if (connectionModeString.equals(BluetoothConstants.INTENT_MAIN)) {
            cellphoneMode = DatabaseConstants.CellphoneMode.main;
            BluetoothClientService bluetoothClientService = BluetoothClientService.getInstance(this, null);
            bluetoothClientService.createMergerSenderController(this);
        } else {
            cellphoneMode = DatabaseConstants.CellphoneMode.reference;
            BluetoothServerService bluetoothServerService = BluetoothServerService.getInstance(this);
            bluetoothServerService.createMergerReceiver(this);
        }
    }

    private void setListener(){
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //finish and stop merger
            }
        });
    }

    @Override
    public void onMergeDone() {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (cellphoneMode == DatabaseConstants.CellphoneMode.reference) {
                        Toast.makeText(MergeDataActivity.this, "La base de données à été exporté avec succès", Toast.LENGTH_LONG).show();
                    }

                    DatabaseController databaseController = DatabaseController.getInstance(MergeDataActivity.this);
                    databaseController.deleteData();

                    MergerFileController mergerFileController = new MergerFileController(MergeDataActivity.this);
                    mergerFileController.mergeCompleted();

                    CellphoneModeFileController cellphoneModeFileController = new CellphoneModeFileController(MergeDataActivity.this);
                    cellphoneModeFileController.writeFile(null);

                    dialog = new Dialog(MergeDataActivity.this);
                    dialog.setContentView(R.layout.dialog_merge);
                    dialog.setTitle("Merge done");

                    Button ok = dialog.findViewById(R.id.dialog_merge_btn_merge_confirmation);

                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            finish();
                        }
                    });
                    dialog.show();
                }
            });
        } catch (Exception exception) {
            Log.e(TagConstants.GENERIC_ERROR, "onMergeDone: ", exception);
        }
    }

    @Override
    public void onMergeFailed(String error) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog = new Dialog(MergeDataActivity.this);
                    dialog.setContentView(R.layout.dialog_merge);
                    dialog.setTitle("Merge Failed");

                    Button ok = dialog.findViewById(R.id.dialog_merge_btn_merge_confirmation);
                    TextView textView = dialog.findViewById(R.id.dialog_merge_tv_merge_message);
                    textView.setText(error);
                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            finish();
                        }
                    });
                    dialog.show();
                }
            });
        } catch (Exception exception) {
            Log.e(TagConstants.GENERIC_ERROR, "onMergeDone: ", exception);
        }
    }
}