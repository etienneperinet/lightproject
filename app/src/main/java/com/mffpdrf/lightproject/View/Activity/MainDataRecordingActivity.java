package com.mffpdrf.lightproject.View.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Controllers.Sensor.DataProcessingController;
import com.mffpdrf.lightproject.Controllers.Sensor.SensorController;
import com.mffpdrf.lightproject.Controllers.SettingsController;
import com.mffpdrf.lightproject.R;
import com.mffpdrf.lightproject.View.SensorDataListenerDisplay;
import com.mffpdrf.lightproject.View.TimerDisplay;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainDataRecordingActivity extends AppCompatActivity {
    TimerDisplay timerDisplay;

    Button buttonCancelRecording;
    Button buttonStopRecording;

    SensorDataListenerDisplay dataView;
    SensorController sensorController;

    DataProcessingController dataProcessingController;

    Boolean wasTheRecordingCancel;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_data_recording);
        SettingsController settingsControllerInitializer = new SettingsController(this);
        sensorController = SensorController.getInstance(this);

        dataView = SensorDataListenerDisplay.getInstance(this);

        dataView.setTextViewLightValue((TextView) findViewById(R.id.activity_main_data_recording_tv_light));
        dataView.setTextViewLatitudeValue((TextView) findViewById(R.id.activity_main_data_recording_tv_latitude));
        dataView.setTextViewLongitudeValue((TextView) findViewById(R.id.activity_main_data_recording_tv_longitude));

        dataProcessingController = DataProcessingController.getInstance(this,
                (TextView) findViewById(R.id.activity_main_data_recording_tv_recordings_since_beginning),
                DatabaseConstants.CellphoneMode.main);

        buttonCancelRecording = findViewById(R.id.activity_main_data_recording_btn_cancel_recording);
        buttonStopRecording = findViewById(R.id.activity_main_data_recording_btn_stop_recording);
        setListener();
        startRecording();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    private void setListener(){
        buttonCancelRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelRecording();
            }
        });

        buttonStopRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { stopRecording(); }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startRecording(){
        timerDisplay = new TimerDisplay(findViewById(R.id.activity_main_data_recording_tv_time_since_beginning));
        dataProcessingController.loadDataForFirstTimingMain();
        timerDisplay.startTimer();
    }



    private void stopRecording() {
        dataProcessingController.turnOffRecording();
        dataView.deactivateAlarm();
        wasTheRecordingCancel = false;
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }

    private void cancelRecording(){
        dataProcessingController.cancelRecording();
        dataProcessingController.turnOffRecording();
        dataView.deactivateAlarm();
        wasTheRecordingCancel = true;
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED,returnIntent);
        finish();
    }
}