package com.mffpdrf.lightproject.View.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mffpdrf.lightproject.Models.DatabaseModels.Microsite;
import com.mffpdrf.lightproject.Models.DatabaseModels.Site;
import com.mffpdrf.lightproject.R;
import com.mffpdrf.lightproject.View.RecyclerView.ExpandableRecyclerView.MicrositeParcelable;
import com.mffpdrf.lightproject.View.RecyclerView.ExpandableRecyclerView.MicrositeViewHolder;
import com.mffpdrf.lightproject.View.RecyclerView.ExpandableRecyclerView.SiteExpandableGroup;
import com.mffpdrf.lightproject.View.RecyclerView.ExpandableRecyclerView.SiteGroupViewHolder;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.util.List;

public class ExpandableRecyclerViewAdapterMicrosite extends ExpandableRecyclerViewAdapter<SiteGroupViewHolder, MicrositeViewHolder> {

    public ExpandableRecyclerViewAdapterMicrosite(List<? extends ExpandableGroup> groups) {
        super(groups);
    }


    @Override
    public SiteGroupViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.expandable_recyclerview_site, parent, false);
        return new SiteGroupViewHolder(view);
    }

    @Override
    public MicrositeViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.expandable_recyclerview_microsite, parent, false);
        return new MicrositeViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(MicrositeViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        final MicrositeParcelable microsite = (MicrositeParcelable) group.getItems().get(childIndex);
        holder.bind(microsite, this);
    }

    @Override
    public void onBindGroupViewHolder(SiteGroupViewHolder holder, int flatPosition, ExpandableGroup group) {
        final SiteExpandableGroup site = (SiteExpandableGroup) group;
        holder.bind(site, this);
    }
}
