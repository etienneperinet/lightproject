package com.mffpdrf.lightproject.View.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.mffpdrf.lightproject.Controllers.DatabaseController;
import com.mffpdrf.lightproject.R;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ProjectCreationActivity extends AppCompatActivity {
    private Button buttonAddProject;
    private Button buttonBack;

    private EditText numberEditText;
    private EditText nameEditText;

    private DatabaseController databaseController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_creation);
        buttonAddProject = findViewById(R.id.activity_project_creation_btn_add);
        buttonBack = findViewById(R.id.activity_project_creation_btn_back);
        numberEditText = findViewById(R.id.activity_project_creation_pt_project_number);
        nameEditText = findViewById(R.id.activity_project_creation_pt_project_name);
        databaseController = DatabaseController.getInstance(this);
        setListener();
    }

    private void setListener(){
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { finishActivity(); }
        });

        buttonAddProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { createProject(); }
        });
    }

    private void createProject(){
        String number = numberEditText.getText().toString();
        String name = nameEditText.getText().toString();
        if (!number.isEmpty() && !name.isEmpty()){
            if (databaseController.projectExists(number)) {
                Toast.makeText(this, "Un projet avec ce numéro existe déjà", Toast.LENGTH_LONG).show();
            } else {
                databaseController.addProject(number, name);
                Toast.makeText(this, "Le projet a été ajouté avec succès", Toast.LENGTH_LONG).show();
                finishActivity();
            }
        } else {
            Toast.makeText(this, "Un projet doit avoir numéro et un nom", Toast.LENGTH_LONG).show();
        }
    }

    private void finishActivity(){
        finish();
    }
}