package com.mffpdrf.lightproject.View.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mffpdrf.lightproject.Controllers.DatabaseController;
import com.mffpdrf.lightproject.Models.DatabaseModels.Microsite;
import com.mffpdrf.lightproject.Models.DatabaseModels.Site;
import com.mffpdrf.lightproject.R;
import com.mffpdrf.lightproject.View.RecyclerView.ExpandableRecyclerView.MicrositeParcelable;
import com.mffpdrf.lightproject.View.RecyclerView.ExpandableRecyclerView.SiteExpandableGroup;
import com.mffpdrf.lightproject.View.RecyclerView.ExpandableRecyclerViewAdapterMicrosite;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

public class MainUpdateDataActivity extends AppCompatActivity {
    Button buttonBack;
    RecyclerView recyclerView;

    DatabaseController databaseController = DatabaseController.getInstance(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_data_update);
        buttonBack = findViewById(R.id.activity_main_data_update_btn_back);
        recyclerView = findViewById(R.id.activity_main_data_update_rv_result);
        setListener();
        setRecyclerView();
    }

    private void setRecyclerView(){
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ArrayList<SiteExpandableGroup> siteExpandableGroupArrayList = fillSiteExpandableGroupList();
        ExpandableRecyclerViewAdapterMicrosite adapter = new ExpandableRecyclerViewAdapterMicrosite(siteExpandableGroupArrayList);
        recyclerView.setAdapter(adapter);
    }

    private ArrayList<SiteExpandableGroup> fillSiteExpandableGroupList(){
        ArrayList<SiteExpandableGroup> siteExpandableGroupArrayList = new ArrayList<>();
        List<Site> allSitesToShow = databaseController.getSites();
        int i = 0;
        for (Site currentSite : allSitesToShow){
            SiteExpandableGroup currentSiteExpandable = new SiteExpandableGroup(createMicrositeParcelableListFromSite(currentSite.getID()),
                    allSitesToShow.get(i));
            siteExpandableGroupArrayList.add(currentSiteExpandable);
            i++;
        }

        return siteExpandableGroupArrayList;
    }

    private List<MicrositeParcelable> createMicrositeParcelableListFromSite(Long siteID){
        List<Microsite> allMicrositesToShow = databaseController.getMicrositesFromSite(siteID);

        List<MicrositeParcelable> micrositeParcelableList = new ArrayList<>();
        for(Microsite currentMicrosite : allMicrositesToShow){
            micrositeParcelableList.add(new MicrositeParcelable(currentMicrosite));
        }
        return micrositeParcelableList;
    }

    private void setListener(){
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { finishActivity(); }
        });
    }

    private void finishActivity(){
        finish();
    }
}