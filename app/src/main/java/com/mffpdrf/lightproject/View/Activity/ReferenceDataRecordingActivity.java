package com.mffpdrf.lightproject.View.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Controllers.Sensor.DataProcessingController;
import com.mffpdrf.lightproject.Controllers.Sensor.SensorController;
import com.mffpdrf.lightproject.Controllers.SettingsController;
import com.mffpdrf.lightproject.R;
import com.mffpdrf.lightproject.View.SensorDataListenerDisplay;
import com.mffpdrf.lightproject.View.TimerDisplay;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ReferenceDataRecordingActivity extends AppCompatActivity {
    TimerDisplay timerDisplay;

    Button buttonStop;

    SensorDataListenerDisplay dataView;
    SensorController sensorController;

    DataProcessingController dataProcessingController;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        super.onCreate(savedInstanceState);
        SettingsController settingsControllerInitializer = new SettingsController(this);

        setContentView(R.layout.activity_reference_data_recording);
        buttonStop = findViewById(R.id.activity_reference_data_recording_btn_stop_recording);
        setListener();
        sensorController = SensorController.getInstance(this);
        dataView = SensorDataListenerDisplay.getInstance(this);

        dataView.setTextViewLightValue((TextView) findViewById(R.id.activity_reference_data_recording_tv_light));

        dataProcessingController = DataProcessingController.getInstance(this,
                (TextView) findViewById(R.id.textViewNumberOfRecordingSinceBeginningR),
                DatabaseConstants.CellphoneMode.reference);
        startRecording();
    }

    @Override
    public void onBackPressed() {
        dataProcessingController.turnOffRecording();
        dataView.deactivateAlarm();
        super.onBackPressed();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startRecording(){
        timerDisplay = new TimerDisplay(findViewById(R.id.textViewTimeSinceBeginningR));
        dataProcessingController.loadDataForFirstTimingReference();
        timerDisplay.startTimer();
    }

    private void setListener() {
        buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { stopRecording(); }
        });
    }

    private void stopRecording() {
        dataProcessingController.turnOffRecording();
        dataView.deactivateAlarm();
        goToReferenceIdleActivity();
    }

    private void goToReferenceIdleActivity(){
        finish();
    }
}