package com.mffpdrf.lightproject.View.RecyclerView.ExpandableRecyclerView;

import com.mffpdrf.lightproject.Models.DatabaseModels.Site;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class SiteExpandableGroup extends ExpandableGroup<MicrositeParcelable> {
    Site site;

    public SiteExpandableGroup(List<MicrositeParcelable> items, Site site) {
        super(site.getName(), items);
        this.site = site;
    }
}
