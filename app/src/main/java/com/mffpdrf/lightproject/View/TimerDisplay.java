package com.mffpdrf.lightproject.View;

import android.os.Handler;
import android.os.Looper;
import android.widget.TextView;

public class TimerDisplay {
    private TextView timerTextView;
    private Integer timeSpent;
    private Handler handler;

    public TimerDisplay(TextView textView) {
        this.timerTextView = textView;
    }

    public void startTimer(){
        timeSpent = 0;
        handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                updateTime();
                handler.postDelayed(this, 1000);
            }
        }, 1000);
    }

    private void updateTime(){
        timeSpent++;

        if (timeSpent >= 60) {
            if (timeSpent >= 3600) {
                Integer hours = timeSpent / 3600;
                Integer secondsRemainder = timeSpent % 3600;
                Integer minutes = secondsRemainder / 60;
                Integer seconds = secondsRemainder % 60;
                timerTextView.setText(hours + "h " + minutes + "m " + seconds + "s");
            } else {
                Integer minutes = timeSpent / 60;
                Integer seconds = timeSpent % 60;
                timerTextView.setText(minutes + "m " + seconds + "s");
            }
        } else {
            timerTextView.setText(timeSpent + "s");
        }
        //timerTextView.setText(timeSpent + " sec");
    }
}
