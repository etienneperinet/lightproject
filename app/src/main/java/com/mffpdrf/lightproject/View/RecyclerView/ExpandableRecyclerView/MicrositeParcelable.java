package com.mffpdrf.lightproject.View.RecyclerView.ExpandableRecyclerView;

import android.os.Parcel;
import android.os.Parcelable;

import com.mffpdrf.lightproject.Models.DatabaseModels.Microsite;

public class MicrositeParcelable implements Parcelable {
    Microsite microsite;

    public MicrositeParcelable(Microsite microsite) {
        this.microsite = microsite;
    }

    protected MicrositeParcelable(Parcel in) {
        this.microsite.setNumber(in.readInt());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(microsite.getNumber().toString());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MicrositeParcelable> CREATOR = new Creator<MicrositeParcelable>() {
        @Override
        public MicrositeParcelable createFromParcel(Parcel in) {
            return new MicrositeParcelable(in);
        }

        @Override
        public MicrositeParcelable[] newArray(int size) {
            return new MicrositeParcelable[size];
        }
    };
}
