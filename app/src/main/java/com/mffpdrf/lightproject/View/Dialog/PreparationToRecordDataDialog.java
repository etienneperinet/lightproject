package com.mffpdrf.lightproject.View.Dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Controllers.DatabaseController;
import com.mffpdrf.lightproject.Controllers.GPSController;
import com.mffpdrf.lightproject.Controllers.Sensor.DataProcessingController;
import com.mffpdrf.lightproject.Controllers.Sensor.SensorController;
import com.mffpdrf.lightproject.R;
import com.mffpdrf.lightproject.View.Activity.MainDataRecordingActivity;
import com.mffpdrf.lightproject.View.Activity.ReferenceDataRecordingActivity;
import com.mffpdrf.lightproject.View.SensorDataListenerDisplay;


public class PreparationToRecordDataDialog {
    OnStartRecordingButtonPressListener listener;

    Dialog dialog;
    private Context context;

    private SensorController sensorController;
    private SensorDataListenerDisplay dataView;
    private DatabaseController databaseController = DatabaseController.getInstance(null);
    private GPSController gpsController;

    private double latitude;
    private double longitude;

    private Button buttonStart;
    private Button buttonCancel;
    private CheckBox checkBoxForce;

    private TextView textViewYAngle;
    private TextView textViewXAngle;
    private TextView textViewLightValue;

    //region Attribute used to add Reference informations
    private Long referenceSessionID;
    //endregion

    public PreparationToRecordDataDialog(Context context) {
        this.context = context;
        sensorController = SensorController.getInstance(context);
        createControllers();
        createDialog();
    }

    public void setListener(OnStartRecordingButtonPressListener listener) {
        this.listener = listener;
    }

    private void createControllers(){
        sensorController = SensorController.getInstance(context);
        gpsController = new GPSController(context);
        askForLocation();
    }

    private void createDialog(){
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_position_phone_correctly);
        dialog.setTitle("Préparation à l'enregistrement");

        setAllElements();
        setListener();
        dialog.show();
    }

    private void setAllElements(){
        buttonCancel = dialog.findViewById(R.id.dialog_position_phone_correctly_btn_cancel_recording);
        buttonStart = dialog.findViewById(R.id.dialog_position_phone_correctly_btn_start_recording);
        checkBoxForce = dialog.findViewById(R.id.dialog_position_phone_correctly_cb_force_start_recording);

        textViewYAngle = dialog.findViewById(R.id.dialog_position_phone_correctly_tv_y_phone_angle_value);
        textViewXAngle = dialog.findViewById(R.id.dialog_position_phone_correctly_tv_x_phone_angle_value);
        textViewLightValue = dialog.findViewById(R.id.dialog_position_phone_correctly_tv_light_value);
    }

    private void setListener(){
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        continueButtonListener();
        setDataListener();
    }

    private void setDataListener(){
        dataView = SensorDataListenerDisplay.getInstance(context);
        dataView.setTextViewAngleX(textViewXAngle);
        dataView.setTextViewAngleY(textViewYAngle);
        dataView.setTextViewLightValue(textViewLightValue);
        dataView.activateAlarm();
        sensorController.setOnSensorDataListener(dataView);
    }

    private Boolean continueWithoutChecking(){
        return checkBoxForce.isChecked();
    }

    private void continueButtonListener(){
        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (continueWithoutChecking()){
                    continueToNextActivity();
                }else if (dataView.isAngleOk()){
                    continueToNextActivity();
                }else {
                    Toast.makeText(context, "L'angle du téléphone n'est pas approprié.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void continueToNextActivity(){
        listener.onStartRecordingButtonPress();
    }

    private void goToReferenceDataRecordingActivity(){
        Intent goToReferenceDataRecordingActivityIntent = new Intent(context, ReferenceDataRecordingActivity.class);
        context.startActivity(goToReferenceDataRecordingActivityIntent);
    }

    private void startRecordingAsReference(){
        DataProcessingController dataProcessingController = DataProcessingController.getInstance(context, DatabaseConstants.CellphoneMode.reference);
        dataProcessingController.setIDToUseOnRecord(databaseController.addReferencePosition(latitude, longitude));
        goToReferenceDataRecordingActivity();
    }

    private void askForLocation(){
        gpsController.callAfterLocationUpdate(new GPSController.afterLocationUpdate() {
            @Override
            public void onLocationUpdate(Location locationResult) {
                longitude = locationResult.getLongitude();
                latitude = locationResult.getLatitude();
            }
        });
    }

    public void closeDialog(){
        dialog.dismiss();
    }

    public interface OnStartRecordingButtonPressListener{
        void onStartRecordingButtonPress();
    }
}