package com.mffpdrf.lightproject.View.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.mffpdrf.lightproject.Controllers.DatabaseController;
import com.mffpdrf.lightproject.R;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ObserverCreationActivity extends AppCompatActivity {
    private Button buttonAddObserver;
    private Button buttonBack;

    private EditText firstNameEditText;
    private EditText lastNameEditText;
    private EditText employeeCodeEditText;

    private DatabaseController databaseController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_observer_creation);
        buttonAddObserver = findViewById(R.id.activity_observer_creation_btn_add);
        buttonBack = findViewById(R.id.activity_observer_creation_btn_back);
        firstNameEditText = findViewById(R.id.activity_observer_creation_pt_first_name);
        lastNameEditText = findViewById(R.id.activity_observer_creation_pt_last_name);
        employeeCodeEditText = findViewById(R.id.activity_observer_creation_pt_code);
        databaseController = DatabaseController.getInstance(this);
        setListener();
    }

    private void setListener(){
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { finishActivity(); }
        });

        buttonAddObserver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { createObserver(); }
        });
    }

    private void createObserver(){
        String firstName = firstNameEditText.getText().toString();
        String lastName = lastNameEditText.getText().toString();
        String employeeCode = employeeCodeEditText.getText().toString();

        if (!firstName.isEmpty() && !lastName.isEmpty() && !employeeCode.isEmpty()){
            if (databaseController.observerExists(employeeCode)) {
                Toast.makeText(this, "Un observateur avec ce code exite déjà", Toast.LENGTH_LONG).show();
            } else {
                databaseController.addObserver(firstName, lastName, employeeCode);
                Toast.makeText(this, "Le projet a été ajouté avec succès", Toast.LENGTH_LONG).show();
                finishActivity();
            }
        } else {
            Toast.makeText(this, "Un observateur doit avoir un prénom, un nom et un code d'employé", Toast.LENGTH_LONG).show();
        }
    }

    private void finishActivity(){
        finish();
    }
}