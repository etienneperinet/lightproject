package com.mffpdrf.lightproject.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.mffpdrf.lightproject.Controllers.DatabaseController;
import com.mffpdrf.lightproject.View.Fragment.DataMergeFragment;
import com.mffpdrf.lightproject.View.Fragment.SessionCreationFragment;

public class FragmentAdapter extends FragmentStateAdapter {
    private SessionCreationFragment sessionCreationFragment;
    private DataMergeFragment dataMergeFragment;

    public FragmentAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    public SessionCreationFragment getSessionCreationFragment() {
        return sessionCreationFragment;
    }

    public DataMergeFragment getDataMergeFragment() {
        return dataMergeFragment;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {

        switch (position) {
            case 1:
                DatabaseController databaseController = DatabaseController.getInstance(null);

                if (databaseController.requiresMerge()) {
                    dataMergeFragment = new DataMergeFragment();
                    return dataMergeFragment;
                }

            default:
                sessionCreationFragment = new SessionCreationFragment();
                return sessionCreationFragment;
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}