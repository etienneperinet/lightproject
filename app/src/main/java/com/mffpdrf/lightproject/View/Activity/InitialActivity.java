package com.mffpdrf.lightproject.View.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Controllers.DatabaseController;
import com.mffpdrf.lightproject.Controllers.Sensor.DataProcessingController;
import com.mffpdrf.lightproject.Controllers.Sensor.SensorAdjusterController;
import com.mffpdrf.lightproject.Models.DatabaseModels.GraphMultiplierPoint;
import com.mffpdrf.lightproject.R;
import com.mffpdrf.lightproject.View.Fragment.DataMergeFragment;
import com.mffpdrf.lightproject.View.Fragment.SessionCreationFragment;
import com.mffpdrf.lightproject.View.FragmentAdapter;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.time.LocalDateTime;
import java.util.List;

public class InitialActivity extends AppCompatActivity {
    TabLayout tabLayout;
    ViewPager2 viewPager2;
    FragmentAdapter fragmentAdapter;

    private Button buttonSearchPhone;
    private Button buttonSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //DatabaseController.getInstance(this).deleteData();
        //deleteDatabase(DatabaseConstants.DATABASE_NAME);

        //DatabaseController.getInstance(this).export();

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial);
        tabLayout = findViewById(R.id.activity_initial_tl_connection_type);
        viewPager2 = findViewById(R.id.activity_initial_vp_connection_type);
        buttonSearchPhone = findViewById(R.id.activity_initial_btn_search_phone);
        buttonSettings = findViewById(R.id.activity_initial_btn_settings);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentAdapter = new FragmentAdapter(fragmentManager, getLifecycle());
        viewPager2.setAdapter(fragmentAdapter);

        viewPager2.setUserInputEnabled(false);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager2.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        manageTabs();
        setListener();
        //AskForPermission();

        //SensorAdjusterController.askForCSV(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        manageTabs();

        SessionCreationFragment sessionCreationFragment = fragmentAdapter.getSessionCreationFragment();
        if (sessionCreationFragment != null) {
            sessionCreationFragment.populateSpinners();
            sessionCreationFragment.disableFeatures();
        }

        DataMergeFragment dataMergeFragment = fragmentAdapter.getDataMergeFragment();
        if (dataMergeFragment != null) {
            dataMergeFragment.disableFeatures();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void insertIntoBD(List<GraphMultiplierPoint> points) {
        DatabaseController databaseController = DatabaseController.getInstance(this);
        Long idGraph = databaseController.addGraphMultiplier(LocalDateTime.now(), null);
        for (GraphMultiplierPoint point : points) {
            databaseController.addGraphPointMultiplier(point.getDataMultiplier(), point.getDataSeen(), idGraph);
        }
    }

    private void setListener() {
        buttonSearchPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSpecificActivity();
            }
        });

        buttonSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSettingsActivity();
            }
        });
    }

    private void manageTabs() {
        DatabaseController databaseController = DatabaseController.getInstance(this);
        if (tabLayout.getTabCount() == 0) {
            tabLayout.addTab(tabLayout.newTab().setText("Création de session"));
            if (databaseController.requiresMerge()) {
                tabLayout.addTab(tabLayout.newTab().setText("Transfert de données"));
            }
        } else if (tabLayout.getTabCount() == 1 && databaseController.requiresMerge()) {
            tabLayout.removeAllTabs();
            tabLayout.addTab(tabLayout.newTab().setText("Création de session"));
            tabLayout.addTab(tabLayout.newTab().setText("Transfert de données"));
        } else if (tabLayout.getTabCount() == 2 && !databaseController.requiresMerge()) {
            tabLayout.removeAllTabs();
            tabLayout.addTab(tabLayout.newTab().setText("Création de session"));
        }
    }

    private void goToSpecificActivity() {
        if (viewPager2.getCurrentItem() == 0) {
            SessionCreationFragment sessionCreationFragment = fragmentAdapter.getSessionCreationFragment();
            if (sessionCreationFragment != null) {
                sessionCreationFragment.goToSpecificActivity();
            }
        } else {
            DataMergeFragment dataMergeFragment = fragmentAdapter.getDataMergeFragment();
            if (dataMergeFragment != null) {
                dataMergeFragment.goToSpecificActivity();
            }
        }
    }

    private void goToSettingsActivity() {
        Intent goToSettingsActivityIntent = new Intent(this, SettingsActivity.class);
        startActivity(goToSettingsActivityIntent);
    }
}