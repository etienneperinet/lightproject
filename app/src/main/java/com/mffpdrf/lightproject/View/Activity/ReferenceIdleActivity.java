package com.mffpdrf.lightproject.View.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Controllers.CellphoneModeFileController;
import com.mffpdrf.lightproject.Controllers.DatabaseController;
import com.mffpdrf.lightproject.Controllers.GPSController;
import com.mffpdrf.lightproject.Controllers.Sensor.DataProcessingController;
import com.mffpdrf.lightproject.R;
import com.mffpdrf.lightproject.View.Dialog.PreparationToRecordDataDialog;
import com.mffpdrf.lightproject.View.SensorDataListenerDisplay;

import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ReferenceIdleActivity extends AppCompatActivity implements PreparationToRecordDataDialog.OnStartRecordingButtonPressListener {
    Button buttonStartRecording;
    Button buttonStopSession;
    PreparationToRecordDataDialog dialog;

    DataProcessingController dataProcessingController;
    SensorDataListenerDisplay dataView;

    Long referenceSessionID;

    GPSController gpsController;
    private double latitude;
    private double longitude;

    DatabaseController databaseController = DatabaseController.getInstance(this);

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reference_idle);
        buttonStartRecording = findViewById(R.id.activity_reference_idle_btn_start_recording);
        buttonStopSession = findViewById(R.id.activity_reference_idle_btn_stop_session);

        dataView = SensorDataListenerDisplay.getInstance(this);
        dataView.setTextViewLatitudeValue(findViewById(R.id.activity_reference_idle_tv_latitude));
        dataView.setTextViewLongitudeValue(findViewById(R.id.activity_reference_idle_tv_longitude));
        dataView.setTextViewLightValue(findViewById(R.id.activity_reference_idle_tv_light));

        CellphoneModeFileController cellphoneModeFileController = new CellphoneModeFileController(this);
        cellphoneModeFileController.writeFile(DatabaseConstants.CellphoneMode.reference);
        gpsController = new GPSController(this);
        startTimer();
        setListener();
    }

    @Override

    protected void onResume() {
        super.onResume();
        startTimer();
        dataView = SensorDataListenerDisplay.getInstance(this, (TextView) findViewById(R.id.activity_reference_idle_tv_light));
        dataView.deactivateAlarm();
    }

    public void onBackPressed() {
        //super.onBackPressed();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startTimer(){
        dataProcessingController = DataProcessingController.getInstance(this, DatabaseConstants.CellphoneMode.reference);
        dataProcessingController.turnOffRecording();
    }

    private void setListener(){
        buttonStartRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { createDialogToStartRecording(); }
        });

        buttonStopSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { endSession(); }
        });
    }

    private void createDialogToStartRecording(){
        askForLocation();
        dialog = new PreparationToRecordDataDialog(this);
        dialog.setListener(this);
    }

    private void endSession(){
        DataProcessingController.destroyInstance();
        finish();
    }


    private void goToReferenceDataRecordingActivity(){
        Intent goToReferenceDataRecordingActivityIntent = new Intent(this, ReferenceDataRecordingActivity.class);
        startActivity(goToReferenceDataRecordingActivityIntent);
    }


    private void askForLocation(){
        gpsController.callAfterLocationUpdate(new GPSController.afterLocationUpdate() {
            @Override
            public void onLocationUpdate(Location locationResult) {
                longitude = locationResult.getLongitude();
                latitude = locationResult.getLatitude();
            }
        });
    }



    @Override
    public void onStartRecordingButtonPress() {
        dialog.closeDialog();
        DataProcessingController dataProcessingController = DataProcessingController.getInstance(this, DatabaseConstants.CellphoneMode.reference);
        dataProcessingController.setIDToUseOnRecord(databaseController.addReferencePosition(latitude, longitude));
        goToReferenceDataRecordingActivity();
    }
}