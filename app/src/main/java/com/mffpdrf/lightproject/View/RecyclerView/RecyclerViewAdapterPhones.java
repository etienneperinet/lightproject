package com.mffpdrf.lightproject.View.RecyclerView;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mffpdrf.lightproject.R;

import java.util.List;

public class RecyclerViewAdapterPhones extends RecyclerView.Adapter<RecyclerViewAdapterPhones.PhoneViewHolder> {
    private List<BluetoothDevice> bluetoothDevices;
    private Context context;
    private  OnItemListener onItemListener;

    public RecyclerViewAdapterPhones(List<BluetoothDevice> bluetoothDevices, Context context, OnItemListener onItemListener) {
        this.bluetoothDevices = bluetoothDevices;
        this.context = context;
        this.onItemListener = onItemListener;
    }

    @NonNull
    @Override
    public PhoneViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_item_phone, parent, false);
        return new PhoneViewHolder(view, onItemListener);
    }

    @Override
    public void onBindViewHolder(@NonNull PhoneViewHolder holder, int position) {
        holder.tvPhoneModel.setText(bluetoothDevices.get(position).getName());
        holder.tvMacAddress.setText(bluetoothDevices.get(position).getAddress());
    }

    @Override
    public int getItemCount() {
        return bluetoothDevices.size();
    }

    public class PhoneViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tvPhoneModel;
        TextView tvMacAddress;
        OnItemListener onItemListener;

        public PhoneViewHolder(@NonNull View itemView, OnItemListener onItemListener) {
            super(itemView);
            tvPhoneModel = itemView.findViewById(R.id.recycler_view_item_phone_tv_phone_model);
            tvMacAddress = itemView.findViewById(R.id.recycler_view_item_phone_tv_mac_address);
            itemView.setOnClickListener(this);

            this.onItemListener = onItemListener;
        }

        @Override
        public void onClick(View view) {
            onItemListener.onItemClick(getAdapterPosition(), view);
        }
    }

    public boolean phoneExists(BluetoothDevice phone) {
        return bluetoothDevices.contains(phone);
    }

    public interface OnItemListener{
        void onItemClick(Integer position, View view);
    }
}