package com.mffpdrf.lightproject.View.RecyclerView.ExpandableRecyclerView;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mffpdrf.lightproject.Controllers.DatabaseController;
import com.mffpdrf.lightproject.Models.DatabaseModels.Microsite;
import com.mffpdrf.lightproject.R;
import com.mffpdrf.lightproject.View.RecyclerView.ExpandableRecyclerViewAdapterMicrosite;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class MicrositeViewHolder extends ChildViewHolder {
    private TextView textViewName;
    private TextView textViewMovement;
    private TextView textViewGood;
    private TextView textViewNote;
    private TextView textViewAverageData;

    private Button buttonDelete;

    public MicrositeViewHolder(View itemView) {
        super(itemView);
        textViewName = itemView.findViewById(R.id.textViewMicrositeNumberPrefix);
        textViewMovement = itemView.findViewById(R.id.textViewMovement);
        textViewGood= itemView.findViewById(R.id.textViewGood);
        textViewNote= itemView.findViewById(R.id.textViewNote);
        textViewAverageData= itemView.findViewById(R.id.textViewAverageData);

        buttonDelete = itemView.findViewById(R.id.buttonDeleteMicroste);
    }

    public void bind(MicrositeParcelable micrositeParcelable, ExpandableRecyclerViewAdapterMicrosite adapter){

        textViewName.setText("Microsite : " + micrositeParcelable.microsite.getPrefix() + micrositeParcelable.microsite.getNumber().toString());
        textViewMovement.setText(getDataSavingType(micrositeParcelable.microsite));
        textViewAverageData.setText("Moyenne : " + getGoodObservationRounded(micrositeParcelable.microsite) + " lux");
        textViewNote.setText("Note : " + micrositeParcelable.microsite.getDescription());
        textViewGood.setText("Données utilisable : " + getPercentageGoodObservationRounded(micrositeParcelable.microsite));

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //DatabaseController.getInstance(null).destroyMicrositeAndDataAndSiteIfNeeded(micrositeParcelable.microsite.getID());
                //adapter.notifyDataSetChanged();
            }
        });
    }

    private String getDataSavingType(Microsite microsite){
        String answer = "";
        if (microsite.getDataSavingType().getID() == 1l) {
            answer = "Mobile";
        } else if (microsite.getDataSavingType().getID() == 2l){
            answer = "Immobile";
        } else {
            answer = "Inconnu";
        }

        return answer;
    }

    private String getPercentageGoodObservationRounded(Microsite microsite){
        Double PercentageGood = DatabaseController.getInstance(null).getPercentageGoodObservation(microsite.getID());
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);

        return df.format(PercentageGood * 100) + "%";
    }

    private String getGoodObservationRounded(Microsite microsite){
        Double average = DatabaseController.getInstance(null).getAverageLightDataFromMicrosite(microsite.getID());
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);

        return df.format(average);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}