package com.mffpdrf.lightproject.View;

import android.content.Context;
import android.location.Location;
import android.media.MediaPlayer;
import android.widget.TextView;

import com.mffpdrf.lightproject.Controllers.GPSController;
import com.mffpdrf.lightproject.Controllers.Sensor.SensorController;
import com.mffpdrf.lightproject.Controllers.SettingsController;
import com.mffpdrf.lightproject.Events.OnGPSListener;
import com.mffpdrf.lightproject.Events.OnSensorDataListener;
import com.mffpdrf.lightproject.R;

import java.time.Instant;

public class SensorDataListenerDisplay implements OnSensorDataListener, OnGPSListener {
    private static SensorDataListenerDisplay instance;

    private Context currentContext;
    private TextView textViewLightValue;
    private TextView textViewLongitudeValue;
    private TextView textViewLatitudeValue;

    private TextView textViewAngleX;
    private TextView textViewAngleY;

    private MediaPlayer alarm;
    private Boolean canUseAlarm;

    private GPSController gpsController;

    private SensorController sensorController;

    private Boolean correctAngle;

    private SensorDataListenerDisplay(Context context, TextView textViewLightValue) {
        currentContext = context;
        this.textViewLightValue = textViewLightValue;
        createAlarm();
        setListenerForThis();
        canUseAlarm = false;
        correctAngle = false;
    }

    private SensorDataListenerDisplay(Context context) {
        currentContext = context;
        createAlarm();
        setListenerForThis();
        canUseAlarm = false;
        correctAngle = false;
    }

    public static SensorDataListenerDisplay getInstance(Context context){
        if(instance == null){
            instance = new SensorDataListenerDisplay(context);
        } else {
            instance.textViewLightValue = null;
        }

        return instance;
    }

    public static SensorDataListenerDisplay getInstance(Context context, TextView textViewLightValue){
        if(instance == null){
            instance = new SensorDataListenerDisplay(context, textViewLightValue);
        } else {
            instance.textViewLightValue = textViewLightValue;
        }
        return instance;
    }


    private void setListenerForThis(){
        sensorController = SensorController.getInstance(currentContext);
        sensorController.setOnSensorDataListener(this);

        gpsController = new GPSController(currentContext);
        gpsController.setOnGPSListener(this);
    }

    private void createAlarm(){
        alarm = MediaPlayer.create(currentContext, R.raw.angle_error_sound);
    }

    @Override
    public void onLightDataChanged(Float lightValue) {
        if (textViewLightValue != null){
            textViewLightValue.setText(lightValue.toString() + " lux");
            //textViewLightValue.setText(Long.toString(Instant.now().getEpochSecond()));
            gpsController.requestLocation();
        }
    }

    @Override
    public void onNewSensorAccuracyForLightChanged(Integer accuracy) { }

    @Override
    public void onPressureDataChanged(Float pressureValue) {   }

    @Override
    public void onNewSensorAccuracyForPressureChanged(Integer accuracy) {    }

    @Override
    public void onNewSensorAccuracyForAngleChanged(Integer accuracy) {    }

    @Override
    public void onSensorForAngleChanged(Float angleX, Float angleY) {
        verifyAngle(angleX, angleY);
        showAngle(angleX, angleY);
    }

    private void verifyAngle(Float angleX, Float angleY){
        if (alarm != null){
            if ((Math.abs(angleX) > SettingsController.getMaximumOrientation() || (Math.abs(angleY) > SettingsController.getMaximumOrientation()))){
                correctAngle = false;
                if (canUseAlarm){
                    alarm.start();
                }
            }else if (alarm.isPlaying()){
                alarm.pause();
                alarm.seekTo(0);
                correctAngle = true;
            }else {
                correctAngle = true;
            }
        }
    }

    private void showAngle(Float angleX, Float angleY){
        if(textViewAngleX != null && textViewAngleY != null){
            textViewAngleY.setText(angleY.toString());
            textViewAngleX.setText(angleX.toString());
        }
    }

    @Override
    public void onNewLocationDetected(Location locationResult) throws Exception {
        if (locationResult != null){
            if (textViewLongitudeValue != null && textViewLatitudeValue != null){
                textViewLongitudeValue.setText("Longitude : " + String.valueOf(locationResult.getLongitude()));
                textViewLatitudeValue.setText("Latitude : " + String.valueOf(locationResult.getLatitude()));
            }
        }else {
            throw new Exception("No location detected");
        }
    }

    public void setTextViewLightValue(TextView textViewLightValue) {
        this.textViewLightValue = textViewLightValue;
    }

    public void setTextViewLongitudeValue(TextView textViewLongitudeValue) {
        this.textViewLongitudeValue = textViewLongitudeValue;
    }

    public void setTextViewLatitudeValue(TextView textViewLatitudeValue) {
        this.textViewLatitudeValue = textViewLatitudeValue;
    }

    public void setTextViewAngleX(TextView textViewAngleX) {
        this.textViewAngleX = textViewAngleX;
    }

    public void setTextViewAngleY(TextView textViewAngleY) {
        this.textViewAngleY = textViewAngleY;
    }


    public void deactivateAlarm(){
        canUseAlarm = false;
    }

    public void activateAlarm(){
        canUseAlarm = true;
    }

    public Boolean isAngleOk(){
        return correctAngle;
    }
}