package com.mffpdrf.lightproject.View.RecyclerView.ExpandableRecyclerView;

import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mffpdrf.lightproject.Controllers.DatabaseController;
import com.mffpdrf.lightproject.R;
import com.mffpdrf.lightproject.View.RecyclerView.ExpandableRecyclerViewAdapterMicrosite;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

public class SiteGroupViewHolder extends GroupViewHolder {
    private TextView textViewName;
    private ImageView arrow;
    private Button buttonDelete;


    public SiteGroupViewHolder(View itemView) {
        super(itemView);
        arrow = (ImageView) itemView.findViewById(R.id.arrow);
        textViewName = itemView.findViewById(R.id.textViewSiteName);
        buttonDelete = itemView.findViewById(R.id.buttonDeleteSite);
    }

    public void bind(SiteExpandableGroup siteExpandableGroup, ExpandableRecyclerViewAdapterMicrosite adapter){
        textViewName.setText("Site : "+ siteExpandableGroup.getTitle());
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //DatabaseController.getInstance(null).destroySiteAndItsData(siteExpandableGroup.site.getID());
                //adapter.notifyDataSetChanged();
            }
        });
    }


    @Override
    public void expand() {
        animateExpand();
    }

    @Override
    public void collapse() {
        animateCollapse();
    }

    private void animateExpand() {
        RotateAnimation rotate =
                new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        arrow.setAnimation(rotate);
    }

    private void animateCollapse() {
        RotateAnimation rotate =
                new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        arrow.setAnimation(rotate);
    }
}
