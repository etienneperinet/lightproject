package com.mffpdrf.lightproject.Constants;

public class TagConstants {
    public static final String BLUETOOTH_PERMISSION = "Bluetooth permission";

    public static final String GENERIC_ERROR = "Generic error";
    public static final String FILE_ERROR = "File error";
    public static final String DATABASE_ERROR = "Database error";
    public static final String RECEIVER_UNREGISTER_ERROR = "Receiver unregister error";
    public static final String BLUETOOTH_ERROR = "Bluetooth error";
    public static final String JSON_ERROR = "JSON error";
    public static final String IO_ERROR = "I/O error";

    public static final String JSON_SENT = "JSON sent";
    public static final String JSON_RECEIVED = "JSON received";
    public static final String JSON_CONFIRMATION_SENT = "JSON confirmation sent";
    public static final String JSON_CONFIRMATION_RECEIVED = "JSON confirmation received";

    public static final String FILE_NOT_FOUND = "File not found";

    public static final String LAST_PING = "Last ping";
    public static final String AVERAGE_PING = "Average ping";
}