package com.mffpdrf.lightproject.Constants;

public class DatabaseConstants {

    public enum CellphoneMode{
        reference,
        main
    }

    public static final String DATABASE_NAME = "field_data.db";
    public static final int DATABASE_VERSION = 1;

    //Table names

    public static final String TABLE_PROJECT = "project";
    public static final String TABLE_CELLPHONE = "cellphone";
    public static final String TABLE_MAIN_SESSION = "main_session";
    public static final String TABLE_SITE = "site";
    public static final String TABLE_OBSERVER = "observer";
    public static final String TABLE_DATA_SAVING_TYPE = "data_saving_type";
    public static final String TABLE_REFERENCE_SESSION = "reference_session";
    public static final String TABLE_REFERENCE_POSITION = "reference_position_session";
    public static final String TABLE_REFERENCE_DATA = "reference_data";
    public static final String TABLE_REFERENCE_LIGHT_DATA = "reference_light_data";
    public static final String TABLE_MICROSITE = "microsite";
    public static final String TABLE_MAIN_DATA = "main_data";
    public static final String TABLE_MAIN_LIGHT_DATA = "main_light_data";
    public static final String TABLE_MAIN_PRESSURE_DATA = "main_pressure_data";
    public static final String TABLE_GRAPH_MULTIPLIER = "graph_multiplier";
    public static final String TABLE_GRAPH_POINT_MULTIPLIER = "graph_point_multiplier";
    public static final String TABLE_LAST_OBJECT_SENT = "last_object_sent";

    //Create table

    public static final String CREATE_TABLE_PROJECT = "CREATE TABLE \"" + TABLE_PROJECT + "\" (" +
            "\"number\" TEXT NOT NULL," +
            "\"name\" TEXT NOT NULL," +
            "PRIMARY KEY (\"number\")" +
            ");";

    public static final String CREATE_TABLE_CELLPHONE = "CREATE TABLE \"" + TABLE_CELLPHONE + "\" (" +
            "\"id\" TEXT UNIQUE," +
            "\"light_sensor_saturation\" INTEGER," +
            "PRIMARY KEY (\"id\")" +
            ");";

    public static final String CREATE_TABLE_MAIN_SESSION = "CREATE TABLE \"" + TABLE_MAIN_SESSION + "\" (" +
            "\"id\" INTEGER UNIQUE," +
            "\"project_number\" INTEGER," +
            "\"observer_id\" INTEGER," +
            "\"cellphone_id\" TEXT," +
            "\"reference_session_id\" INTEGER UNIQUE," +
            "PRIMARY KEY (\"id\" AUTOINCREMENT)," +
            "FOREIGN KEY (\"project_number\") REFERENCES \"project\"(\"id\")," +
            "FOREIGN KEY (\"observer_id\") REFERENCES \"observer\"(\"id\")," +
            "FOREIGN KEY (\"cellphone_id\") REFERENCES \"cellphone\"(\"id\")" +
            ");";


    public static final String CREATE_TABLE_SITE = "CREATE TABLE \"" + TABLE_SITE + "\" (" +
            "\"id\" INTEGER UNIQUE," +
            "\"name\" TEXT NOT NULL," +
            "\"main_session_id\" INTEGER," +
            "\"project_number\" INTEGER," +
            "PRIMARY KEY (\"id\" AUTOINCREMENT)," +
            "FOREIGN KEY (\"main_session_id\") REFERENCES \""+ TABLE_MAIN_SESSION +"\"(\"id\")" +
            "FOREIGN KEY (\"project_number\") REFERENCES \""+ TABLE_PROJECT +"\"(\"number\")" +
            ");";

    public static final String CREATE_TABLE_OBSERVER = "CREATE TABLE \"" + TABLE_OBSERVER + "\" (" +
            "\"id\" INTEGER UNIQUE," +
            "\"first_name\" TEXT NOT NULL," +
            "\"last_name\" TEXT NOT NULL," +
            "\"employee_code\" TEXT NOT NULL," +
            "PRIMARY KEY (\"id\" AUTOINCREMENT)" +
            ");";

    public static final String CREATE_TABLE_DATA_SAVING_TYPE = "CREATE TABLE \"" + TABLE_DATA_SAVING_TYPE + "\" (" +
            "\"id\" INTEGER UNIQUE," +
            "\"name\" TEXT NOT NULL," +
            "PRIMARY KEY (\"id\" AUTOINCREMENT)" +
            ");";

    public static final String CREATE_TABLE_REFERENCE_SESSION = "CREATE TABLE \"" + TABLE_REFERENCE_SESSION + "\" (" +
            "\"id\" INTEGER UNIQUE," +
            "\"cellphone_id\" TEXT," +
            "\"year\" INTEGER NOT NULL," +
            "\"month\" INTEGER NOT NULL," +
            "\"day\" INTEGER NOT NULL," +
            "PRIMARY KEY (\"id\" AUTOINCREMENT)," +
            "FOREIGN KEY (\"cellphone_id\") REFERENCES \"cellphone\"(\"id\")" +
            ");";

    public static final String CREATE_TABLE_REFERENCE_POSITION = "CREATE TABLE \"" + TABLE_REFERENCE_POSITION + "\" (" +
            "\"id\" INTEGER UNIQUE," +
            "\"gps_latitude\" REAL NULL," +
            "\"gps_longitude\" REAL NULL," +
            "\"reference_session_id\" INTEGER," +
            "PRIMARY KEY (\"id\" AUTOINCREMENT)," +
            "FOREIGN KEY (\"reference_session_id\") REFERENCES \""+ TABLE_REFERENCE_SESSION +"\"(\"id\")" +
            ");";

    public static final String CREATE_TABLE_REFERENCE_DATA = "CREATE TABLE \"" + TABLE_REFERENCE_DATA + "\" (" +
            "\"id\" INTEGER UNIQUE," +
            "\"time_passed\" INTEGER NOT NULL," +
            "\"reference_position_id\" INTEGER," +
            "PRIMARY KEY (\"id\" AUTOINCREMENT)," +
            "FOREIGN KEY (\"reference_position_id\") REFERENCES \""+TABLE_REFERENCE_POSITION+"\"(\"id\")" +
            ");";

    public static final String CREATE_TABLE_REFERENCE_LIGHT_DATA = "CREATE TABLE \"" + TABLE_REFERENCE_LIGHT_DATA + "\" (" +
            "\"id\" INTEGER UNIQUE," +
            "\"sensor_status\" INTEGER NOT NULL," +

            "\"all_values\" REAL NOT NULL," +
            "\"good_values\" REAL NOT NULL," +

            "\"all_standardized_values\" REAL," +
            "\"good_standardized_values\" REAL," +

            "\"number_good_observations\" INTEGER NOT NULL," +
            "\"number_observations\" INTEGER NOT NULL," +
            "\"standard_deviation_good\" REAL NOT NULL," +
            "\"standard_deviation_all\" REAL NOT NULL," +
            "\"rotation_cell_x\" REAL NOT NULL," +
            "\"rotation_cell_y\" REAL NOT NULL," +

            "PRIMARY KEY (\"id\" AUTOINCREMENT)," +
            "FOREIGN KEY (\"id\") REFERENCES \""+TABLE_REFERENCE_DATA+"\"(\"id\")" +
            ");";

    public static final String CREATE_TABLE_MICROSITE = "CREATE TABLE \"" + TABLE_MICROSITE + "\" (" +
            "\"id\" INTEGER UNIQUE," +
            "\"prefix\" TEXT," +
            "\"number\" INTEGER NOT NULL," +
            "\"description\" TEXT," +
            "\"gps_latitude\" REAL NULL," +
            "\"gps_longitude\" REAL NULL," +
            "\"site_id\" INTEGER," +
            "\"data_saving_type_id\" INTEGER," +
            "PRIMARY KEY (\"id\" AUTOINCREMENT)," +
            "FOREIGN KEY (\"site_id\") REFERENCES \"site\"(\"id\")," +
            "FOREIGN KEY (\"data_saving_type_id\") REFERENCES \"data_saving_type\"(\"id\")" +
            ");";

    public static final String CREATE_TABLE_MAIN_DATA = "CREATE TABLE \"" + TABLE_MAIN_DATA + "\" (" +
            "\"id\" INTEGER UNIQUE," +
            "\"time_passed\" INTEGER NOT NULL," +
            "\"gps_latitude\" REAL NULL," +
            "\"gps_longitude\" REAL NULL," +
            "\"microsite_id\" INTEGER," +
            "PRIMARY KEY (\"id\" AUTOINCREMENT)," +
            "FOREIGN KEY (\"microsite_id\") REFERENCES \"microsite\"(\"id\")" +
            ");";

    public static final String CREATE_TABLE_MAIN_LIGHT_DATA = "CREATE TABLE \"" + TABLE_MAIN_LIGHT_DATA + "\" (" +
            "\"id\" INTEGER UNIQUE," +
            "\"sensor_status\" INTEGER NOT NULL," +

            "\"all_values\" REAL NOT NULL," +
            "\"good_values\" REAL NOT NULL," +
            "\"all_standardized_values\" REAL," +
            "\"good_standardized_values\" REAL," +

            "\"number_good_observations\" INTEGER NOT NULL," +
            "\"number_observations\" INTEGER NOT NULL," +
            "\"standard_deviation_good\" REAL NOT NULL," +
            "\"standard_deviation_all\" REAL NOT NULL," +
            "\"rotation_cell_x\" REAL NOT NULL," +
            "\"rotation_cell_y\" REAL NOT NULL," +

            "PRIMARY KEY (\"id\")," +
            "FOREIGN KEY (\"id\") REFERENCES \""+TABLE_MAIN_DATA +"\"(\"id\")" +
            ");";



    public static final String CREATE_TABLE_MAIN_PRESSURE_DATA = "CREATE TABLE \"" + TABLE_MAIN_PRESSURE_DATA + "\" (" +
            "\"id\" INTEGER UNIQUE," +
            "\"sensor_status\" INTEGER NOT NULL," +

            "\"all_values\" REAL NOT NULL," +
            "\"good_values\" REAL NOT NULL," +
            "\"number_good_observations\" INTEGER NOT NULL," +
            "\"number_observations\" INTEGER NOT NULL," +
            "\"standard_deviation_good\" REAL NOT NULL," +
            "\"standard_deviation_all\" REAL NOT NULL," +

            "PRIMARY KEY (\"id\")," +
            "FOREIGN KEY (\"id\") REFERENCES \""+TABLE_MAIN_DATA +"\"(\"id\")" +
            ");";

    public static final String CREATE_TABLE_GRAPH_MULTIPLIER = "CREATE TABLE \"" + TABLE_GRAPH_MULTIPLIER + "\" (" +
            "\"id\" INTEGER UNIQUE," +

            "\"year\" INTEGER NOT NULL," +
            "\"month\" INTEGER NOT NULL," +
            "\"day\" INTEGER NOT NULL," +

            "\"cellphone_id\" TEXT," +
            "PRIMARY KEY (\"id\" AUTOINCREMENT)," +
            "FOREIGN KEY (\"cellphone_id\") REFERENCES \""+ TABLE_CELLPHONE +"\"(\"id\")" +
            ");";

    public static final String CREATE_TABLE_GRAPH_POINT_MULTIPLIER = "CREATE TABLE \"" + TABLE_GRAPH_POINT_MULTIPLIER + "\" (" +
            "\"id\" INTEGER UNIQUE," +

            "\"multiplier\" REAL NOT NULL," +
            "\"data_recorded\" REAL NOT NULL," +

            "\"graph_point_multiplier_id\" INTEGER," +
            "PRIMARY KEY (\"id\" AUTOINCREMENT)," +
            "FOREIGN KEY (\"graph_point_multiplier_id\") REFERENCES \""+ TABLE_GRAPH_MULTIPLIER +"\"(\"id\")" +
            ");";

    //Insert into

    public static final String INSERT_MOBILE_DATA_SAVING_TYPE = "INSERT INTO \"data_saving_type\"(\"name\")" +
            "VALUES ('Mobile')";

    public static final String INSERT_IMMOBILE_DATA_SAVING_TYPE = "INSERT INTO \"data_saving_type\"(\"name\")" +
            "VALUES ('Immobile')";

    //Select all

    public static final String SELECT_OBSERVERS = "SELECT * FROM " + TABLE_OBSERVER;
    public static final String SELECT_GRAPH_POINT_MULTIPLIERS = "SELECT * FROM " + TABLE_GRAPH_POINT_MULTIPLIER;
    public static final String SELECT_GRAPH_MULTIPLIERS = "SELECT * FROM " + TABLE_GRAPH_MULTIPLIER;
    public static final String SELECT_MAIN_SESSIONS = "SELECT * FROM " + TABLE_MAIN_SESSION;
    public static final String SELECT_REFERENCE_SESSIONS = "SELECT * FROM " + TABLE_REFERENCE_SESSION;
    public static final String SELECT_PROJECTS = "SELECT * FROM " + TABLE_PROJECT;
    public static final String SELECT_CELLPHONES = "SELECT * FROM " + TABLE_CELLPHONE;
    public static final String SELECT_MICROSITES_WITH_SITE = "SELECT * FROM " + TABLE_MICROSITE + " WHERE site_id = ?";
    public static final String SELECT_SITES_WITH_SESSION_ID = "SELECT * FROM " + TABLE_SITE + " WHERE main_session_id = ?";

    public static final String SELECT_DATA_ID_WITH_MICROSITE = "SELECT id FROM " + TABLE_MAIN_DATA + " WHERE microsite_id = ?";

    public static final String SELECT_MAIN_LIGHTS_FOR_UPDATE = "SELECT id, all_values, good_values  FROM " + TABLE_MAIN_LIGHT_DATA;

    public static final String SELECT_ALL_GOOD_AVERAGE_LIGHT_DATA = "SELECT good_values FROM " + TABLE_MAIN_LIGHT_DATA + " WHERE id = ?";

    public static final String SELECT_MICROSITES_FROM_SITE = "SELECT * FROM " + TABLE_MICROSITE + " WHERE id = ?";


    //Select one
    public static final String SELECT_DATA_SAVINGTYPE = "SELECT * FROM " + TABLE_MAIN_DATA + " WHERE id = ?";

    public static final String SELECT_MAIN_DATA = "SELECT * FROM " + TABLE_MAIN_DATA + " WHERE id = ?";
    public static final String SELECT_MAIN_LIGHT_DATA = "SELECT * FROM " + TABLE_MAIN_LIGHT_DATA + " WHERE id = ?";
    public static final String SELECT_MAIN_PRESSURE_DATA = "SELECT * FROM " + TABLE_MAIN_PRESSURE_DATA + " WHERE id = ?";
    public static final String SELECT_MAIN_SESSION = "SELECT * FROM " + TABLE_MAIN_SESSION + " WHERE id = ?";
    public static final String SELECT_MICROSITE = "SELECT * FROM " + TABLE_MICROSITE + " WHERE id = ?";
    public static final String SELECT_OBSERVER = "SELECT * FROM " + TABLE_OBSERVER + " WHERE id = ?";
    public static final String SELECT_PROJECT = "SELECT * FROM " + TABLE_PROJECT + " WHERE number = ?";
    public static final String SELECT_REFERENCE_DATA = "SELECT * FROM " + TABLE_REFERENCE_DATA + " WHERE id = ?";
    public static final String SELECT_REFERENCE_LIGHT_DATA = "SELECT * FROM " + TABLE_REFERENCE_LIGHT_DATA + " WHERE id = ?";
    public static final String SELECT_REFERENCE_POSITION = "SELECT * FROM " + TABLE_REFERENCE_POSITION + " WHERE id = ?";
    public static final String SELECT_REFERENCE_SESSION = "SELECT * FROM " + TABLE_REFERENCE_SESSION + " WHERE id = ?";
    public static final String SELECT_SITE = "SELECT * FROM " + TABLE_SITE + " WHERE id = ?";
    public static final String SELECT_SITE_WITH_NAME_AND_PROJECT_NUMBER = "SELECT * FROM " + TABLE_SITE + " WHERE name = ? AND project_number = ?";

    //Select next

    public static final String SELECT_NEXT_MAIN_DATA = "SELECT * FROM " + TABLE_MAIN_DATA + " WHERE id > ? AND microsite_id = ? ORDER BY id LIMIT 1" ;
    public static final String SELECT_NEXT_MAIN_LIGHT_DATA = "SELECT * FROM " + TABLE_MAIN_LIGHT_DATA + " WHERE id > ? ORDER BY id LIMIT 1" ;
    public static final String SELECT_NEXT_MAIN_PRESSURE_DATA = "SELECT * FROM " + TABLE_MAIN_PRESSURE_DATA + " WHERE id > ? ORDER BY id LIMIT 1" ;
    public static final String SELECT_NEXT_MAIN_SESSION = "SELECT * FROM " + TABLE_MAIN_SESSION + " WHERE id > ? ORDER BY id LIMIT 1" ;
    public static final String SELECT_NEXT_MICROSITE = "SELECT * FROM " + TABLE_MICROSITE + " WHERE id > ? AND site_id = ? ORDER BY id LIMIT 1" ;
    public static final String SELECT_NEXT_OBSERVER = "SELECT * FROM " + TABLE_OBSERVER + " WHERE id > ? ORDER BY id LIMIT 1" ;
    public static final String SELECT_NEXT_REFERENCE_DATA = "SELECT * FROM " + TABLE_REFERENCE_DATA + " WHERE id > ? ORDER BY id LIMIT 1" ;
    public static final String SELECT_NEXT_REFERENCE_LIGHT_DATA = "SELECT * FROM " + TABLE_REFERENCE_LIGHT_DATA + " WHERE id > ? ORDER BY id LIMIT 1" ;
    public static final String SELECT_NEXT_REFERENCE_POSITION = "SELECT * FROM " + TABLE_REFERENCE_POSITION + " WHERE id > ? ORDER BY id LIMIT 1" ;
    public static final String SELECT_NEXT_REFERENCE_SESSION = "SELECT * FROM " + TABLE_REFERENCE_SESSION + " WHERE id > ? ORDER BY id LIMIT 1" ;
    public static final String SELECT_NEXT_SITE = "SELECT * FROM " + TABLE_SITE + " WHERE id > ? AND main_session_id = ? ORDER BY id LIMIT 1";

    public static final String SELECT_NEXT_GRAPH_MULTIPLIER = "SELECT * FROM " + TABLE_GRAPH_MULTIPLIER + " WHERE id > ? ORDER BY id LIMIT 1";
    public static final String SELECT_NEXT_GRAPH_POINT_MULTIPLIER = "SELECT * FROM " + TABLE_GRAPH_POINT_MULTIPLIER + " WHERE id > ?  AND graph_point_multiplier_id = ? ORDER BY id LIMIT 1";

    //Tests

    public static final String TEST_DATA = "test_data";
    public static final String TEST_GOOD_DATA = "test_good_data";

    public static final String CREATE_TABLE_TEST_DATA = "CREATE TABLE \"" + TEST_DATA + "\" (" +
            "\"id\" INTEGER UNIQUE," +

            "\"time\" INTEGER NOT NULL," +
            "\"value\" REAL NOT NULL," +

            "\"id_real_data\" INTEGER," +
            "PRIMARY KEY (\"id\" AUTOINCREMENT)," +
            "FOREIGN KEY (\"id_real_data\") REFERENCES \""+TABLE_MAIN_DATA +"\"(\"id\")" +
            ");";

    public static final String CREATE_TABLE_TEST_GOOD_DATA = "CREATE TABLE \"" + TEST_GOOD_DATA + "\" (" +
            "\"id\" INTEGER UNIQUE," +

            "\"time\" INTEGER NOT NULL," +
            "\"value\" REAL NOT NULL," +

            "\"id_real_data\" INTEGER," +
            "PRIMARY KEY (\"id\" AUTOINCREMENT)," +
            "FOREIGN KEY (\"id_real_data\") REFERENCES \""+TABLE_MAIN_DATA +"\"(\"id\")" +
            ");";
}