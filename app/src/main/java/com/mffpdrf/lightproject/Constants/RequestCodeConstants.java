package com.mffpdrf.lightproject.Constants;

import com.mffpdrf.lightproject.View.Activity.InitialActivity;

public class RequestCodeConstants {
    public static final Integer LOCATION_PERMISSIONS = 1001;
    public static final Integer BLUETOOTH_PERMISSIONS = 1002;
    public static final Integer EXTERNAL_STORAGE_PERMISSIONS = 1003;
}
