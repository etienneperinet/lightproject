package com.mffpdrf.lightproject.Constants;

public class FileConstants {

    public static final String SETTINGS = "settings.json";

    public static final String LAST_OBJECT_MERGED = "lastObjectMerged.json";

    public static final String CELLPHONE_MODE = "cellphoneMode.json";

}