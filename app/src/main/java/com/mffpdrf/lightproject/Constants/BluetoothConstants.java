package com.mffpdrf.lightproject.Constants;

import java.util.UUID;

public class BluetoothConstants {
    public enum ConnectionMode {
        sessionCreation,
        dataMerging
    }

    public static final String APP_NAME = "LightProject";

    public static final UUID SECURE_UUID = UUID.fromString("63c86d9b-295f-4856-99bc-7e603536e597");

    public static final String INTENT_CONNECTION_MODE = "connectionMode";

    public static final String INTENT_DATA_MERGING = "merging";

    public static final String INTENT_SESSION_CREATION = "sessionCreation";

    public static final String INTENT_CELLPHONE_MODE = "cellphoneMode";

    public static final String INTENT_REFERENCE = "reference";

    public static final String INTENT_MAIN = "main";

    public static final String MERGE_FINISHED = "mergeFinished";

    public static final String SESSION_CREATED = "sessionCreated";

    public static final String PING = "ping";

    public static final String PONG = "pong";

    public static final Integer TIME_BEFORE_END = 5000;

    public static final Long ACTIVITY_SWITCHING_WAITING_TIME = 8l;
}