package com.mffpdrf.lightproject.Constants;

public class ErrorMessagesConstants {
    public static final String BLUETOOTH_CONNECTION_INTERRUPTED = "La connexion Bluetooth a été interrompu";
    public static final String DATABASE_EXPORT_FAILED = "L'exportation de la base de données à échoué";
}