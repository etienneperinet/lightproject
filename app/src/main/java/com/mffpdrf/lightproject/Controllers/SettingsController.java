package com.mffpdrf.lightproject.Controllers;

import android.content.Context;
import android.util.Log;

import com.mffpdrf.lightproject.Constants.FileConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.View.Activity.InitialActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;

public class SettingsController {
    Context context;

    public enum EndOfRecordingEnum{
        automatic,
        manual
    }

    private static Integer millisecondsBetweenEachRecording = 3000;
    private static Integer numberOfRecords = 10;
    private static EndOfRecordingEnum endOfRecordingModeChosen = EndOfRecordingEnum.automatic;
    private static Integer maximumOrientation = 15;

    private static final String NAME_RECORDING_FREQUENCY = "frequency";
    private static final String NAME_NUMBER_OF_RECORDS = "number_of_recording";
    private static final String NAME_MODE_CHOSEN = "mode_chosen";
    private static final String NAME_MAXIMUM_ORIENTATION = "maximum_orientation";

    public SettingsController(Context context) {
        this.context = context;
        readFile();
    }

    public static Integer getMillisecondsBetweenEachRecording() {
        return millisecondsBetweenEachRecording;
    }

    public static void setMillisecondsBetweenEachRecording(Integer millisecondsBetweenEachRecording) {
        SettingsController.millisecondsBetweenEachRecording = millisecondsBetweenEachRecording;
    }

    public static Integer getNumberOfRecords() {
        return numberOfRecords;
    }

    public static void setNumberOfRecords(Integer numberOfRecords) {
        SettingsController.numberOfRecords = numberOfRecords;
    }

    public static EndOfRecordingEnum getEndOfRecordingModeChosen() {
        return endOfRecordingModeChosen;
    }

    public static void setEndOfRecordingModeChosen(EndOfRecordingEnum endOfRecordingModeChosen) {
        SettingsController.endOfRecordingModeChosen = endOfRecordingModeChosen;
    }

    public Integer getTimeWithNumberOfRecord(){
        return SettingsController.getNumberOfRecords() * SettingsController.getMillisecondsBetweenEachRecording();
    }

    public Integer getTimeWithNumberOfRecord(Integer numberOfRecords){
        SettingsController.numberOfRecords = numberOfRecords;
        return SettingsController.getNumberOfRecords()* SettingsController.getMillisecondsBetweenEachRecording();
    }

    public static Integer getMaximumOrientation() {
        return maximumOrientation;
    }

    public static void setMaximumOrientation(Integer maximumOrientation) {
        SettingsController.maximumOrientation = maximumOrientation;
    }

    public Integer changeNumberOfRecordWithTime(Integer time){
        SettingsController.numberOfRecords = time / SettingsController.getMillisecondsBetweenEachRecording();
        return SettingsController.getNumberOfRecords();
    }


    public String convertEndOfRecordingEnumToString(EndOfRecordingEnum endOfRecordingEnumToConvert){
        String valueToSend = "";
        switch (endOfRecordingEnumToConvert) {
            case automatic:
                valueToSend = "automatic";
                break;
            case manual:
                valueToSend = "manual";
                break;
        }
        return valueToSend;
    }

    public EndOfRecordingEnum convertStringToEndOfRecordingEnum(String stringToConvert){
        EndOfRecordingEnum valueToSend = null;
        switch (stringToConvert) {
            case "automatic":
                valueToSend = EndOfRecordingEnum.automatic;
                break;
            case "manual":
                valueToSend = EndOfRecordingEnum.manual;
                break;
        }
        return valueToSend;
    }


    public JSONObject makeJSONObject(){
        JSONObject obj = new JSONObject() ;
        try {
            obj.put(NAME_RECORDING_FREQUENCY, SettingsController.getMillisecondsBetweenEachRecording());
            obj.put(NAME_NUMBER_OF_RECORDS, SettingsController.getNumberOfRecords());
            obj.put(NAME_MODE_CHOSEN, convertEndOfRecordingEnumToString(getEndOfRecordingModeChosen()));
            obj.put(NAME_MAXIMUM_ORIENTATION, getMaximumOrientation());
        } catch (JSONException e) {
            Log.e(TagConstants.JSON_ERROR, "makeJSONObject: " + e.getMessage());
            e.printStackTrace();
        }

        return obj;
    }

    private void readFile(){
        try {
            File myObj = new File(context.getFilesDir() , FileConstants.SETTINGS);
            Scanner myReader = new Scanner(myObj);
            String data = "";
            while (myReader.hasNextLine()) {
                data = myReader.nextLine();
            }
            useDataFromFile(data);
            myReader.close();
        } catch (FileNotFoundException e) {
            createFile();
        }
    }

    private void useDataFromFile(String dataFromFile){
        try {
            JSONObject jsonObject = new JSONObject(dataFromFile);
            millisecondsBetweenEachRecording = jsonObject.getInt(NAME_RECORDING_FREQUENCY);
            numberOfRecords = jsonObject.getInt(NAME_NUMBER_OF_RECORDS);
            endOfRecordingModeChosen = convertStringToEndOfRecordingEnum(jsonObject.getString(NAME_MODE_CHOSEN));
            maximumOrientation = jsonObject.getInt(NAME_MAXIMUM_ORIENTATION);
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "useDataFromFile: " + exception.getMessage());
            exception.printStackTrace();
        }
    }

    public void createFile(){
        JSONObject jsonObject = makeJSONObject();
        try {
            File filepath = new File(context.getFilesDir() , FileConstants.SETTINGS);
            FileWriter fileWriter = new FileWriter(filepath);

            fileWriter.append(jsonObject.toString());
            fileWriter.flush();
            fileWriter.close();
        } catch (Exception e) {
            Log.e("File Creation Error", e.getMessage());
        }
    }
}
