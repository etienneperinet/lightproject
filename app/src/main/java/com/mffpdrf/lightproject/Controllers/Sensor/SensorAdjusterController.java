package com.mffpdrf.lightproject.Controllers.Sensor;

import android.content.Context;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.mffpdrf.lightproject.Controllers.DatabaseController;
import com.mffpdrf.lightproject.Models.DatabaseModels.GraphMultiplierPoint;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SensorAdjusterController {
    static private SensorAdjusterController instance;
    List<GraphMultiplierPoint> pointsOnGraph;
    Integer lastIndex;

    public SensorAdjusterController(Context context) {
        this.pointsOnGraph = DatabaseController.getInstance(context).getLastGraphPointMultiplierPoints();
        orderPointsOnGraph();
        lastIndex = 0;
    }

    public static SensorAdjusterController getInstance(Context context){
        if (instance == null){
            instance = new SensorAdjusterController(context);
        } else {
            instance.pointsOnGraph = DatabaseController.getInstance(context).getLastGraphPointMultiplierPoints();
        }
        return instance;
    }

    public static void askForCSV(Context context){
        if (getInstance(context).isListEmpty()){
            Toast.makeText(context, "Veuillez charger le CSV dans les paramètres pour calibrer les données", Toast.LENGTH_LONG).show();
        }
    }


    public Boolean isListEmpty(){
        Boolean listEmpty = true;
        if (instance.pointsOnGraph.size() > 0){
            listEmpty = false;
        }
        return listEmpty;
    }

    private void orderPointsOnGraph(){
        Collections.sort(pointsOnGraph, new Comparator<GraphMultiplierPoint>() {
            @Override
            public int compare(GraphMultiplierPoint o1, GraphMultiplierPoint o2) {
                return Float.compare(o1.getDataSeen(), o2.getDataSeen());
            }
        });
    }


    private Float getMultiplier(Float dataSeen){
        Integer indexOfComparableData = getDataIndexInListComparableToDataSeen(dataSeen);
        // realMultiplier = ((DataJustSeen – DataSeen(0)) * ((Multiplier(1) - Multiplier(0)) / (DataSeen(1) - DataSeen (0)))) + Multiplier(0)
        // 0 = index -1
        // 1 = index
        if (lastIndex >= pointsOnGraph.size()){
            lastIndex --;
        } else if (lastIndex <= 0){
            lastIndex++;
        }
        return ((dataSeen - pointsOnGraph.get(indexOfComparableData - 1).getDataSeen())
                *((pointsOnGraph.get(indexOfComparableData).getDataMultiplier() - pointsOnGraph.get(indexOfComparableData - 1).getDataMultiplier())
                /(pointsOnGraph.get(indexOfComparableData).getDataSeen() - pointsOnGraph.get(indexOfComparableData - 1).getDataSeen())))
                + pointsOnGraph.get(indexOfComparableData - 1).getDataMultiplier();
    }

    private Integer getDataIndexInListComparableToDataSeen(Float dataSeen){
        if (pointsOnGraph.get(lastIndex).getDataSeen() > dataSeen){
            goThroughListBackward(dataSeen);
            lastIndex++;
        }
        else {
            goThroughListForward(dataSeen);
        }
        return lastIndex;
    }

    private void goThroughListForward(Float dataSeen){

        while (pointsOnGraph.get(lastIndex).getDataSeen() < dataSeen && lastIndex < pointsOnGraph.size() - 1) {
            lastIndex ++;
        }
    }

    private void goThroughListBackward(Float dataSeen){
        while (pointsOnGraph.get(lastIndex).getDataSeen() > dataSeen && lastIndex > 1) {
            lastIndex --;
        }
    }

    public Float adjustData(Float data){
        return data * getMultiplier(data);
    }
}
