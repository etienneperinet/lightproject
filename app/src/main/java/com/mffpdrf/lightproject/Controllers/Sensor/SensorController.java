package com.mffpdrf.lightproject.Controllers.Sensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import com.mffpdrf.lightproject.Events.OnSensorDataListener;

import java.util.ArrayList;
import java.util.List;

public class SensorController {
    private Boolean sensorForAngleAccurate;

    private float gravity[];
    private float geoMagnetic[];
    float orientation[] = new float[3];
    float R[] = new float[9];
    float I[] = new float[9];
    float pitch = 0f;
    float roll = 0f;

    private static SensorController instance;
    private SensorManager sensorManager;
    Context currentContext;
    private SensorEventListener listenerSensor;
    private List<OnSensorDataListener> listenerData = new ArrayList<>();

    public SensorController(Context context) {
        this.sensorManager = sensorManager = (SensorManager) context.getSystemService(context.SENSOR_SERVICE) ;
        sensorForAngleAccurate = false;
        listenerSensor = new SensorEventListener() {
            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
                if (sensor.getType() == Sensor.TYPE_ROTATION_VECTOR){

                }
            }

            @Override
            public void onSensorChanged(SensorEvent event) {

                switch (event.sensor.getType()){
                    case Sensor.TYPE_LIGHT:
                        sendLightData((Float) event.values[0]);
                        break;
                    case Sensor.TYPE_PRESSURE:
                        sendPressureData((Float) event.values[0]);
                        break;
                    /*case Sensor.TYPE_ROTATION_VECTOR:
                        //sendAngleOfCellPhone((Float) event.values[0], (Float) event.values[1]);
                        break;*/
                    case Sensor.TYPE_ACCELEROMETER:
                        gravity = event.values;
                        break;
                    case Sensor.TYPE_MAGNETIC_FIELD:
                        geoMagnetic = event.values;
                        break;
                    default:
                        break;
                }

                if (gravity != null && geoMagnetic != null) {
                    boolean success = SensorManager.getRotationMatrix(R, I, gravity, geoMagnetic);
                    if (success) {
                        SensorManager.getOrientation(R, orientation);
                        pitch = orientation[1];
                        roll = orientation[2];
                        sendAngleOfCellPhone(pitch * -1, roll * -1);
                    }
                }
            }
        };

        sensorManager.registerListener(listenerSensor, sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT), SensorManager.SENSOR_DELAY_GAME);
        sensorManager.registerListener(listenerSensor, sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE), SensorManager.SENSOR_DELAY_GAME);
        //sensorManager.registerListener(listenerSensor, sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR), SensorManager.SENSOR_DELAY_GAME);

        sensorManager.registerListener(listenerSensor, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_GAME);
        sensorManager.registerListener(listenerSensor, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_GAME);
    }

    public static SensorController getInstance(Context context){
        if (instance == null){
            instance = new SensorController(context);
        }
        return instance;
    }

    private void sendLightData(Float lightData){
        for(OnSensorDataListener listener : listenerData){
            listener.onLightDataChanged(lightData);
        }
    }

    private void sendPressureData(Float pressureData){
        for(OnSensorDataListener listener : listenerData){
            listener.onPressureDataChanged(pressureData);
        }
    }

    private void sendAngleOfCellPhone(Float angleX, Float angleY){
        for(OnSensorDataListener listener : listenerData){
            listener.onSensorForAngleChanged((float)Math.toDegrees(angleX), (float)Math.toDegrees(angleY));
        }
    }

    public void setOnSensorDataListener(OnSensorDataListener setOnDataListener){
        listenerData.add(setOnDataListener);
    }

}
