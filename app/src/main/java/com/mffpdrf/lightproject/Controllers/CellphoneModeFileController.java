package com.mffpdrf.lightproject.Controllers;

import android.content.Context;
import android.util.Log;

import com.mffpdrf.lightproject.Constants.BluetoothConstants;
import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Constants.FileConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;

public class CellphoneModeFileController {
    private Context context;

    public CellphoneModeFileController(Context context) {
        this.context = context;
    }

    public void writeFile(DatabaseConstants.CellphoneMode cellphoneMode) {

        JSONObject cellphoneJSON = new JSONObject();
        try {
            if (cellphoneMode == null) {
                cellphoneJSON.put("cellphoneMode", "null");
            } else if (cellphoneMode == DatabaseConstants.CellphoneMode.main) {
                cellphoneJSON.put("cellphoneMode", DatabaseConstants.CellphoneMode.main.toString());
            } else if (cellphoneMode == DatabaseConstants.CellphoneMode.reference) {
                cellphoneJSON.put("cellphoneMode", DatabaseConstants.CellphoneMode.reference.toString());
            }

            try {
                if (cellphoneJSON != null) {
                    File filepath = new File(context.getFilesDir() , FileConstants.CELLPHONE_MODE);
                    FileWriter fileWriter = new FileWriter(filepath);

                    fileWriter.append(cellphoneJSON.toString());
                    fileWriter.flush();
                    fileWriter.close();
                }
            } catch (Exception exception) {
                Log.e(TagConstants.FILE_ERROR, "writeFile", exception);
            }
        } catch (JSONException jsonException) {
            Log.e(TagConstants.JSON_ERROR, "writeFile: ", jsonException);
        }
    }

    public DatabaseConstants.CellphoneMode readFile() {
        String data = null;
        DatabaseConstants.CellphoneMode cellphoneMode = null;
        try {
            File file = new File(context.getFilesDir(), FileConstants.CELLPHONE_MODE);
            if (file.exists()) {
                Scanner scanner = new Scanner(file);
                while (scanner.hasNext()) {
                    data = scanner.nextLine();
                }
                scanner.close();
            }
        } catch (FileNotFoundException exception) {
            Log.e(TagConstants.FILE_NOT_FOUND, "readFile: ", exception);
        }
        JSONObject jsonFile = null;
        try {
            if (data != null) {
                jsonFile = new JSONObject(data);
                try {
                    String mode = jsonFile.getString("cellphoneMode");
                    if (mode.equals(DatabaseConstants.CellphoneMode.main.toString())) {
                        cellphoneMode = DatabaseConstants.CellphoneMode.main;
                    } else if (mode.equals(DatabaseConstants.CellphoneMode.reference.toString())) {
                        cellphoneMode = DatabaseConstants.CellphoneMode.reference;
                    }
                } catch (JSONException exception) {
                    Log.e(TagConstants.JSON_ERROR, "Cellphone: ", exception);
                }
            }
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "readFile: ", exception);
        }
        return cellphoneMode;
    }
}