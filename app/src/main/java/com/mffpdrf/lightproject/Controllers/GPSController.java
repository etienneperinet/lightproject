package com.mffpdrf.lightproject.Controllers;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.mffpdrf.lightproject.Events.OnGPSListener;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

public class GPSController extends Thread {
    private final Integer DEFAULT_UPDATE_INTERVAL_MILLISECONDS = 10000;
    private final Integer FASTEST_UPDATE_INTERVAL_MILLISECONDS = 1000;

    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private FusedLocationProviderClient fusedLocationProviderClient;

    private Context context;
    private OnGPSListener onGPSListener;

    public GPSController(Context context) {
        locationRequest = LocationRequest.create();
        this.context = context;

        locationRequest.setInterval(DEFAULT_UPDATE_INTERVAL_MILLISECONDS);
        locationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_MILLISECONDS);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context);
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(@NonNull LocationResult locationResult) {
                super.onLocationResult(locationResult);
            }
        };
    }

    public void requestLocation() {
        requestUpdateLocation();
        showLocation();
    }

    private void requestUpdateLocation() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
        }
    }

    private void showLocation(){
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fusedLocationProviderClient.getLastLocation().addOnSuccessListener((Activity) context, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    try {
                        sendGPSCoordinates(location);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void sendGPSCoordinates(Location locationResult) throws Exception {
        onGPSListener.onNewLocationDetected(locationResult);
    }

    public void setOnGPSListener(OnGPSListener setOnDataListener){
        onGPSListener = setOnDataListener;
    }


    public interface afterLocationUpdate{
        void onLocationUpdate(Location locationResult);
    }

    public void callAfterLocationUpdate(afterLocationUpdate locationUpdateCallBack){
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fusedLocationProviderClient.getLastLocation().addOnSuccessListener((Activity) context, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    try {
                        locationUpdateCallBack.onLocationUpdate(location);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}