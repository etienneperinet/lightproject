package com.mffpdrf.lightproject.Controllers.Merger;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.mffpdrf.lightproject.Constants.BluetoothConstants;
import com.mffpdrf.lightproject.Constants.ErrorMessagesConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Controllers.DatabaseController;
import com.mffpdrf.lightproject.Events.OnMergeEventListener;
import com.mffpdrf.lightproject.Models.DatabaseModels.GraphMultiplier;
import com.mffpdrf.lightproject.Models.DatabaseModels.GraphMultiplierPoint;
import com.mffpdrf.lightproject.Models.DatabaseModels.Cellphone;
import com.mffpdrf.lightproject.Models.ReceptionConfirmation;
import com.mffpdrf.lightproject.Models.DatabaseModels.MainData;
import com.mffpdrf.lightproject.Models.DatabaseModels.MainLightData;
import com.mffpdrf.lightproject.Models.DatabaseModels.MainPressureData;
import com.mffpdrf.lightproject.Models.DatabaseModels.MainSession;
import com.mffpdrf.lightproject.Models.DatabaseModels.Microsite;
import com.mffpdrf.lightproject.Models.DatabaseModels.Observer;
import com.mffpdrf.lightproject.Models.DatabaseModels.Project;
import com.mffpdrf.lightproject.Models.DatabaseModels.Site;
import com.mffpdrf.lightproject.Services.BluetoothConnectionService;

import org.json.JSONException;
import org.json.JSONObject;


import java.util.List;

public class MergerSenderController extends MergerController implements BluetoothConnectionService.OnMergeObjectConfirmationReceiveListener {
    private static final Integer TIME_BEFORE_SENDING_FIRST_OBJECT = 3333;

    private Handler handler;
    private Runnable runnable;

    private DatabaseController databaseController;
    private BluetoothConnectionService bluetoothConnectionService;

    private List<Cellphone> cellphones;
    private List<Project> projects;
    private Integer currentProjectIndex = 0;
    private Integer currentCellphoneIndex = 0;

    //region LastObjectsSent
    private MainSession lastMainSessionSent;
    private Site lastSiteSent;
    private Microsite lastMicrositeSent;
    private MainData lastMainDataSent;

    private GraphMultiplierPoint lastGraphPoint;
    private GraphMultiplier lastGraph;

    private ReceptionConfirmation lastObjectSent;

    private Boolean mergeCompleted = false;
    //endregion

    private OnMergeEventListener listenerMerger;

    public MergerSenderController(BluetoothConnectionService bluetoothConnectionService, Context context, OnMergeEventListener setOnListener) {
        super(context);
        setOnMergeEventListener(setOnListener);
        this.bluetoothConnectionService = bluetoothConnectionService;
        databaseController = DatabaseController.getInstance(null);
        handler = new Handler(Looper.getMainLooper());
        runnable = new Runnable() {
            @Override
            public void run() {

                listenerMerger.onMergeFailed(ErrorMessagesConstants.BLUETOOTH_CONNECTION_INTERRUPTED);
            }
        };
        lastObjectSent = new ReceptionConfirmation();
        cellphones = databaseController.getCellphones();
        projects = databaseController.getProjects();

        setLastObjectsSent();
        findFirstObjectToSend();
        sendFirstObject();

        bluetoothConnectionService.setOnMergeObjectConfirmationReceivedListener(this);
    }

    public void sendFirstObject(){
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sendObject();
            }
        }, TIME_BEFORE_SENDING_FIRST_OBJECT);
    }

    public void setOnMergeEventListener(OnMergeEventListener setOnListener){
        listenerMerger = setOnListener;
    }

    private void setLastObjectsSent(){
        lastMainSessionSent = new MainSession(0l);
        lastSiteSent = new Site(0l);
        lastMicrositeSent = new Microsite(0l);
        lastMainDataSent = new MainData(0l);

        lastGraphPoint = new GraphMultiplierPoint(0l);
        lastGraph = new GraphMultiplier(0l);
    }

    private void findFirstObjectToSend(){
        //If there is no json file, send the first cellphone
        if (databaseController.didMergeFailed()){
            MergerFileController mergerFileController = new MergerFileController(context);
            lastJSONObject = mergerFileController.readFile();
        } else {
            lastJSONObject = getCellphoneNext();
        }
    }

    private void findNextObjectToSend(){
        try {
            switch (lastJSONObject.getString("JSONType")){
                case DatabaseConstants.TABLE_CELLPHONE:
                    lastJSONObject = getCellphoneNext();
                    break;
                case DatabaseConstants.TABLE_PROJECT:
                    lastJSONObject = getProjectNextObject();
                    break;
                case DatabaseConstants.TABLE_MAIN_SESSION:
                    lastJSONObject = getNextObserver();
                    break;
                case DatabaseConstants.TABLE_OBSERVER:
                    lastJSONObject = getSiteNextObject();
                    break;
                case DatabaseConstants.TABLE_SITE:
                    lastJSONObject = getMicrositeNextObject();
                    break;
                case DatabaseConstants.TABLE_MICROSITE:
                    lastJSONObject = getMainDataNextObject();
                    break;
                case DatabaseConstants.TABLE_MAIN_DATA:
                    lastJSONObject = getNextMainLightData();
                    break;
                case DatabaseConstants.TABLE_MAIN_LIGHT_DATA:
                    lastJSONObject = getNextMainPressureData();
                    break;
                case DatabaseConstants.TABLE_MAIN_PRESSURE_DATA:
                    lastJSONObject = getMainDataNextObject();
                    break;
                case DatabaseConstants.TABLE_GRAPH_MULTIPLIER:
                    lastJSONObject = getGraphMultiplierPointNextObject();
                    break;
                case DatabaseConstants.TABLE_GRAPH_POINT_MULTIPLIER:
                    lastJSONObject = getGraphMultiplierPointNextObject();
                    break;
                default:
            }

        } catch (JSONException exception){
            Log.e(TagConstants.JSON_ERROR, "findNextObjectToSend: ", exception);
        }
    }

    private void setLastObjectSent(JSONObject jsonToSend){
        try {
            lastObjectSent.setObjectID(jsonToSend.getString("id"));
            lastObjectSent.setObjectName(jsonToSend.getString("JSONType"));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "setLastObjectSent: ", exception);
        }
    }

    private void setLastObjectSentProject(JSONObject jsonToSend){
        try {
            lastObjectSent.setObjectID(jsonToSend.getString("number"));
            lastObjectSent.setObjectName(jsonToSend.getString("JSONType"));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "setLastObjectSent: ", exception);
        }
    }

    private JSONObject getCellphoneNext(){
        JSONObject jsonToSend;
        if (currentCellphoneIndex >= cellphones.size()){
            jsonToSend = getProjectNextObject();
        } else {
            jsonToSend = cellphones.get(currentCellphoneIndex).toJSON();
            currentCellphoneIndex++;
            setLastObjectSent(jsonToSend);
        }

        return jsonToSend;
    }

    private JSONObject getProjectNextObject(){
        JSONObject jsonToSend;
        if (currentProjectIndex >= projects.size()){
            jsonToSend = getMainSessionNextObject();
        } else {
            jsonToSend = projects.get(currentProjectIndex).toJSON();
            setLastObjectSentProject(jsonToSend);
            currentProjectIndex++;
        }

        return jsonToSend;
    }

    private JSONObject getMainSessionNextObject(){
        JSONObject jsonToSend = null;

        MainSession nextMainSession = databaseController.getNextMainSession(lastMainSessionSent.getID());
        if (nextMainSession == null){
            jsonToSend = getGraphMultiplierNextObject();
        } else {
            lastMainSessionSent = nextMainSession;
            jsonToSend = lastMainSessionSent.toJSON();
            setLastObjectSent(jsonToSend);
        }

        return jsonToSend;
    }

    private JSONObject getNextObserver(){
        JSONObject jsonToSend = null;

        Observer objectToSend = databaseController.getObserver(lastMainSessionSent.getObserver().getID());

        jsonToSend = objectToSend.toJSON();
        setLastObjectSent(jsonToSend);

        return jsonToSend;
    }

    private JSONObject getSiteNextObject(){
        JSONObject jsonToSend;

        Site nextSiteToSend = databaseController.getNextSite(lastSiteSent.getID(), lastMainSessionSent.getID());
        if (nextSiteToSend == null){
            jsonToSend = getMainSessionNextObject();
        } else {
            lastSiteSent = nextSiteToSend;
            jsonToSend = lastSiteSent.toJSON();
            setLastObjectSent(jsonToSend);
        }

        return jsonToSend;
    }

    private JSONObject getMicrositeNextObject(){
        JSONObject jsonToSend;

        Microsite nextMicrositeToSend = databaseController.getNextMicrosite(lastMicrositeSent.getID(), lastSiteSent.getID());

        if (nextMicrositeToSend == null){
            jsonToSend = getSiteNextObject();
        }else {
            lastMicrositeSent = nextMicrositeToSend;
            jsonToSend = lastMicrositeSent.toJSON();
            setLastObjectSent(jsonToSend);
        }
        return jsonToSend;
    }

    private JSONObject getMainDataNextObject(){
        JSONObject jsonToSend;

        MainData nextMainDataToSend = databaseController.getNextMainData(lastMainDataSent.getID(), lastMicrositeSent.getID());

        if (nextMainDataToSend == null){
            jsonToSend = getMicrositeNextObject();
        }else {
            lastMainDataSent = nextMainDataToSend;
            jsonToSend = lastMainDataSent.toJSON();
            setLastObjectSent(jsonToSend);
        }
        return jsonToSend;
    }

    private JSONObject getNextMainLightData(){
        JSONObject jsonToSend;

        MainLightData lightData = databaseController.getMainLightData(lastMainDataSent.getID());

        if (lightData == null){
            jsonToSend = getNextMainPressureData();
        }else {
            jsonToSend = lightData.toJSON();
            setLastObjectSent(jsonToSend);
        }
        return jsonToSend;
    }

    private JSONObject getNextMainPressureData(){
        JSONObject jsonToSend;

        MainPressureData pressureData = databaseController.getMainPressureData(lastMainDataSent.getID());
        if (pressureData != null){
            jsonToSend = pressureData.toJSON();
            setLastObjectSent(jsonToSend);
        } else {
            jsonToSend = getMainDataNextObject();
        }


        return jsonToSend;
    }

    private JSONObject getGraphMultiplierNextObject(){
        JSONObject jsonToSend = null;

        GraphMultiplier nextGraph = databaseController.getNextGraphMultiplier(lastGraph.getID());

        if (nextGraph == null){
            lastJSONObject = getMergeFinishedNotification();
            sendObject();
        }else {
            lastGraph = nextGraph;
            jsonToSend = lastGraph.toJSON();
            setLastObjectSent(jsonToSend);
        }
        return jsonToSend;
    }

    private JSONObject getGraphMultiplierPointNextObject(){
        JSONObject jsonToSend = null;

        GraphMultiplierPoint nextGraphPoint = databaseController.getNextGraphMultiplierPoint(lastGraphPoint.getID(), lastMainDataSent.getID());

        if (nextGraphPoint == null){
            jsonToSend = getGraphMultiplierNextObject();
        }else {
            lastGraphPoint = nextGraphPoint;
            jsonToSend = lastGraph.toJSON();
            setLastObjectSent(jsonToSend);
        }
        return jsonToSend;
    }

    public void sendObject(){
        if (lastJSONObject != null) {
            bluetoothConnectionService.write(lastJSONObject.toString().getBytes());
            Log.d(TagConstants.JSON_SENT, lastJSONObject.toString());
            handler.postDelayed(runnable, BluetoothConstants.TIME_BEFORE_END);
        }
    }

    public JSONObject getMergeFinishedNotification() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("JSONType", BluetoothConstants.MERGE_FINISHED);
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "getMergeFinishedNotification: ", exception);
        }

        return jsonObject;
    }

    private void endMerge(){
        handler.removeCallbacks(runnable);
        mergeCompleted = true;
        listenerMerger.onMergeDone();
    }

    @Override
    public void onConfirmation(JSONObject confirmationObject) {
        handler.removeCallbacks(runnable);
        Log.d(TagConstants.JSON_CONFIRMATION_RECEIVED, "onConfirmation: " + confirmationObject.toString());
        if (confirmationObject.toString().contains(BluetoothConstants.MERGE_FINISHED)){
            endMerge();
        } else {
            findNextObjectToSend();
            MergerFileController mergerFileController = new MergerFileController(context);
            mergerFileController.writeFile(lastJSONObject);
            if (!mergeCompleted) {
                sendObject();
            }
        }


    }
}