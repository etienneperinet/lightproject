package com.mffpdrf.lightproject.Controllers.Sensor;


import android.content.Context;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Controllers.DatabaseController;
import com.mffpdrf.lightproject.Controllers.GPSController;
import com.mffpdrf.lightproject.Controllers.SettingsController;
import com.mffpdrf.lightproject.Events.OnSensorDataListener;
import com.mffpdrf.lightproject.Models.DataProcessingModels.DataRecordedByTheSensors;
import com.mffpdrf.lightproject.R;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static com.mffpdrf.lightproject.Controllers.SettingsController.getMillisecondsBetweenEachRecording;

public class DataProcessingController implements OnSensorDataListener {
    private Context context;

    public final Integer MINIMUM_ACCURACY_TO_RECORD = 2;
    public Boolean wasRecordCancel;
    static private DataProcessingController instance;

    private Long IDToUseOnRecord;

    private Boolean recording;
    private Boolean rightAngle;
    private Boolean calculating;

    private Integer lightAccuracy;
    private Integer pressureAccuracy;
    private Integer angleAccuracy;

    private Double latitude = null;
    private Double longitude = null;

    private Float worstYAngle = 0f;
    private Float worstXAngle = 0f;

    private List<DataRecordedByTheSensors> allLastLightValues;
    private List<DataRecordedByTheSensors> allLastPressureValues;

    private DataRecordedByTheSensors lastLightRecorded = new DataRecordedByTheSensors(0f, 0l, false);
    private DataRecordedByTheSensors lastPressureRecorded = new DataRecordedByTheSensors(0f,0l, false);

    private Long firstTimeForCurrentRecord;
    private Long firstTimingSinceBeginning;

    private DatabaseController databaseController;
    private SensorController sensorController;
    private SensorAdjusterController sensorAdjusterController;

    private TextView textViewNumberOfRecordingSinceBeginning;

    private static Long timePassed = 0l;
    private Integer numberOfDataRecorded = 0;
    private Handler handlerRecording;
    private Runnable runnableRecording;

    private Boolean wasTheSoundMade = false;

    private MediaPlayer alarmForTheEndOf;

    private DatabaseConstants.CellphoneMode cellphoneMode;

    private DataProcessingController(Context context, DatabaseConstants.CellphoneMode cellphoneMode) {
        this.context = context;
        recording = false;// For Testing ???
        rightAngle = false;// For Testing ???
        lightAccuracy = 3;
        pressureAccuracy = 3;
        allLastLightValues = new ArrayList<DataRecordedByTheSensors>();
        allLastPressureValues = new ArrayList<DataRecordedByTheSensors>();

        databaseController = DatabaseController.getInstance(context);
        sensorAdjusterController = SensorAdjusterController.getInstance(context);
        setListenerForThis(context);
        createAlarm();
        startRecording(cellphoneMode);
        firstTimingSinceBeginning = Instant.now().toEpochMilli();
    }

    private void createAlarm(){
        alarmForTheEndOf = MediaPlayer.create(context, R.raw.end_of_recording);
    }

    private void startRecording(DatabaseConstants.CellphoneMode cellphoneMode){
        this.cellphoneMode = cellphoneMode;
        handlerRecording = new Handler(Looper.getMainLooper());
        runnableRecording = createCorrectRunnable(cellphoneMode);
        handlerRecording.postDelayed(runnableRecording, SettingsController.getMillisecondsBetweenEachRecording());
    }

    public void cancelRecording(){
        wasRecordCancel = false;
        databaseController.destroyMicrositeAndDataAndSiteIfNeeded(IDToUseOnRecord);
    }

    private Runnable createCorrectRunnable(DatabaseConstants.CellphoneMode cellphoneMode){
        switch (cellphoneMode) {
            case reference:
                return new Runnable() {
                    @Override
                    public void run() {
                        runEachTimingReferenceData();
                        handlerRecording.postDelayed(this, getMillisecondsBetweenEachRecording());
                    }};
            case main:
                return new Runnable() {
                    @Override
                    public void run() {
                        runEachTimingMainData();
                        handlerRecording.postDelayed(this, getMillisecondsBetweenEachRecording());
                    }};
        }
        return null;
    }

    static public DataProcessingController getInstance(Context context, TextView textViewNumberOfRecordingSinceBeginning,
                                                       @Nullable DatabaseConstants.CellphoneMode cellphoneMode) {
        if (instance == null){
            instance = new DataProcessingController(context, cellphoneMode);
        }
        instance.textViewNumberOfRecordingSinceBeginning = textViewNumberOfRecordingSinceBeginning;
        instance.databaseController = DatabaseController.getInstance(context);
        instance.setListenerForThis(context);
        
        return instance;
    }

    static public DataProcessingController getInstance(Context context, @Nullable DatabaseConstants.CellphoneMode cellphoneMode){
        if (instance == null){
            instance = new DataProcessingController(context, cellphoneMode);
        }
        instance.textViewNumberOfRecordingSinceBeginning = null;
        instance.recording = false;
        instance.databaseController = DatabaseController.getInstance(context);
        instance.setListenerForThis(context);
        return instance;
    }

    static public void destroyInstance(){
        instance.handlerRecording.removeCallbacks(instance.runnableRecording);
        instance = null;
    }

    private void setListenerForThis(Context context){
        sensorController = SensorController.getInstance(context);
        sensorController.setOnSensorDataListener(this);
    }

    public Boolean getRecording() {
        return recording;
    }

    public void turnOnRecording(){
        wasTheSoundMade = false;
        recording = true;
    }

    public void turnOffRecording(){
        recording = false;

    }

    private void resetValuesToStartNewRecordingMain(){
        allLastPressureValues.clear();
        allLastLightValues.clear();

        lastLightRecorded.setTimeRecorded(firstTimeForCurrentRecord);
        lastPressureRecorded.setTimeRecorded(firstTimeForCurrentRecord);

        allLastLightValues.add(lastLightRecorded);
        allLastPressureValues.add(lastPressureRecorded);
    }

    private void resetValuesToStartNewRecordingReference(){
        allLastLightValues.clear();
        lastLightRecorded.setTimeRecorded(firstTimeForCurrentRecord);
        allLastLightValues.add(lastLightRecorded);
    }

    private void resetWorstAngles(){
        worstYAngle = 0f;
        worstXAngle = 0f;
    }

    private void loadDataToShow(){
        numberOfDataRecorded = 0;
        if ( textViewNumberOfRecordingSinceBeginning != null){
            textViewNumberOfRecordingSinceBeginning.setText("0");
        }
    }

    private void askForLocation(){
        GPSController gpsController = new GPSController(context);
        gpsController.callAfterLocationUpdate(new GPSController.afterLocationUpdate() {
            @Override
            public void onLocationUpdate(Location locationResult) {
                longitude = locationResult.getLongitude();
                latitude = locationResult.getLatitude();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void loadDataForFirstTimingMain(){
        loadDataToShow();
        //giveTestValue(); //Give data to lastData ???
        firstTimeForCurrentRecord = Instant.now().toEpochMilli();
        lastLightRecorded.setTimeRecorded(firstTimeForCurrentRecord);
        lastPressureRecorded.setTimeRecorded(firstTimeForCurrentRecord);


        resetValuesToStartNewRecordingMain();
        resetWorstAngles();
        turnOnRecording();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void loadDataForFirstTimingReference(){
        loadDataToShow();
        //giveTestValue(); //Give data to lastData ???
        firstTimeForCurrentRecord = Instant.now().toEpochMilli();
        lastLightRecorded.setTimeRecorded(firstTimeForCurrentRecord);

        resetValuesToStartNewRecordingReference();
        resetWorstAngles();
        turnOnRecording();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void reloadDataForNextTimingMain(){
        firstTimeForCurrentRecord = Instant.now().toEpochMilli();
        lastLightRecorded = allLastLightValues.get(allLastLightValues.size() - 1);
        lastPressureRecorded = allLastPressureValues.get(allLastPressureValues.size() - 1);
        resetValuesToStartNewRecordingMain();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void reloadDataForNextTimingReference(){
        firstTimeForCurrentRecord = Instant.now().toEpochMilli();
        lastLightRecorded = allLastLightValues.get(allLastLightValues.size() - 1);
        resetValuesToStartNewRecordingReference();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void giveTestValue(){
        lastLightRecorded = new DataRecordedByTheSensors(42f, false);
        lastPressureRecorded= new DataRecordedByTheSensors(42f, false);
    }

    private void updateUI(){
        if (textViewNumberOfRecordingSinceBeginning!=null){
            textViewNumberOfRecordingSinceBeginning.setText(numberOfDataRecorded.toString());
        }
        numberOfDataRecorded++;
    }

    private Boolean canContinueRecording(){
        Boolean canContinueRecording = true;
        if (SettingsController.getEndOfRecordingModeChosen() == SettingsController.EndOfRecordingEnum.automatic){
            if (numberOfDataRecorded > SettingsController.getNumberOfRecords()){
                canContinueRecording = false;
            }
        }

        return canContinueRecording;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private Boolean runEachTimingMainData() {
        Boolean canContinueRecording = canContinueRecording();
        if (canContinueRecording){
            timePassed+= getMillisecondsBetweenEachRecording();
            if (recording){
                askForLocation();
                recordMainData();
            }
        } else if (!wasTheSoundMade){
            alarmForTheEndOf.start();
            wasTheSoundMade = true;
        }
        return canContinueRecording;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void runEachTimingReferenceData() {
        timePassed+= getMillisecondsBetweenEachRecording();
        if (recording){
            recordReferenceData();
        }
    }

    private List<DataRecordedByTheSensors> setLastTimeRecordedLight(List<DataRecordedByTheSensors> allLastLightValuesToCalculate){
        Integer listSize = allLastLightValuesToCalculate.size();
        Long timeFromLastItem = allLastLightValuesToCalculate.get(listSize - 1).getTimeRecorded();

        allLastLightValuesToCalculate.get(listSize - 1).setTimeRecorded(Instant.now().toEpochMilli() - timeFromLastItem);
        return allLastLightValuesToCalculate;
    }

    private List<DataRecordedByTheSensors> setLastTimeRecordedPressure(List<DataRecordedByTheSensors> allLastPressureValuesToCalculate ){
        Integer listSize = allLastPressureValuesToCalculate.size();
        Long timeFromLastItem = allLastPressureValuesToCalculate.get(listSize - 1).getTimeRecorded();

        allLastPressureValuesToCalculate.get(listSize - 1).setTimeRecorded(Instant.now().toEpochMilli() - timeFromLastItem);
        return allLastPressureValuesToCalculate;

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void recordMainData(){
        updateUI();

        runCalculationsForMain();

        resetWorstAngles();
        reloadDataForNextTimingMain();
    }
    private void compressList(){

    }

    private void runCalculationsForMain(){
        List<DataRecordedByTheSensors> allLastLightValuesToCalculate = new ArrayList<>();
        List<DataRecordedByTheSensors> allLastPressureValuesToCalculate = new ArrayList<>();

        allLastLightValuesToCalculate.addAll(allLastLightValues) ;
        allLastPressureValuesToCalculate.addAll(allLastPressureValues);
        allLastPressureValuesToCalculate = setLastTimeRecordedPressure(allLastPressureValuesToCalculate);
        allLastLightValuesToCalculate = setLastTimeRecordedLight(allLastLightValuesToCalculate);

        Long mainData = databaseController.addMainData(IDToUseOnRecord, getTimePassedSinceBeginningOfRecording(), latitude, longitude);
        recordMainLightData(mainData, allLastLightValuesToCalculate);
        recordMainPressureData(mainData, allLastPressureValuesToCalculate);


        /*Handler calculatorHandler = new Handler(Looper.getMainLooper());
        calculatorHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
            }
        }, 0);*/
    }

    private Long getTimePassedSinceBeginningOfRecording(){
        Long now = Instant.now().toEpochMilli();
        Long valueToReturn = Instant.now().toEpochMilli() - firstTimingSinceBeginning;
        return valueToReturn;
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void recordReferenceData(){
        updateUI();
        List<DataRecordedByTheSensors> allLastLightValuesToCalculate = new ArrayList<>();

        allLastLightValuesToCalculate.addAll(allLastLightValues);
        allLastLightValuesToCalculate = setLastTimeRecordedLight(allLastLightValuesToCalculate);


        Long referenceData = databaseController.addReferenceData(IDToUseOnRecord, getTimePassedSinceBeginningOfRecording());
        recordReferenceLightData(referenceData, allLastLightValuesToCalculate);
        resetWorstAngles();
        reloadDataForNextTimingReference();
    }

    private List<DataRecordedByTheSensors> getListOfGoodData(List<DataRecordedByTheSensors> allLastValues){
        List<DataRecordedByTheSensors> goodLastValues = new ArrayList<>();
        for (DataRecordedByTheSensors data: allLastValues) {
            if (data.getGoodData()){
                goodLastValues.add(data);
            }
        }
        return goodLastValues;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void recordMainLightData(Long mainData, List<DataRecordedByTheSensors> allLastLightValuesToCalculate){
        Float averageLightData = 0f;
        Float averageGoodLightData = 0f;
        Float standardDeviationAll = 0f;
        Float standardDeviationGood = 0f;
        List<DataRecordedByTheSensors> goodLastLightValuesToCalculate = getListOfGoodData(allLastLightValuesToCalculate);

        if (isThereData(allLastLightValuesToCalculate)){
            averageLightData = calculateRelativeAverage(allLastLightValuesToCalculate);
            standardDeviationAll = calculateStandardDeviation(allLastLightValuesToCalculate, averageLightData);
        }
        if (isThereData(goodLastLightValuesToCalculate)) {
            averageGoodLightData = calculateRelativeAverage(goodLastLightValuesToCalculate);
            standardDeviationGood = calculateStandardDeviation(goodLastLightValuesToCalculate, averageGoodLightData);
        }

        Float goodLightStandardized = 0f;
        Float allLightStandardized = 0f;
        if (!sensorAdjusterController.isListEmpty()){
            goodLightStandardized = sensorAdjusterController.adjustData(averageGoodLightData);
            allLightStandardized = sensorAdjusterController.adjustData(averageLightData);
        }
        databaseController.addMainLightData(mainData, lightAccuracy, averageLightData, averageGoodLightData,
                allLightStandardized, goodLightStandardized,
                goodLastLightValuesToCalculate.size(), allLastLightValuesToCalculate.size(),standardDeviationAll, standardDeviationGood,
                worstXAngle, worstYAngle);
        //AddAllDataForTest(mainData, allLastLightValuesToCalculate);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void recordMainPressureData(Long mainData, List<DataRecordedByTheSensors> allLastPressureValuesToCalculate){
        Float averagePressureData = 0f;
        Float averageGoodPressureData = 0f;
        Float standardDeviationAll = 0f;
        Float standardDeviationGood = 0f;
        List<DataRecordedByTheSensors> goodLastPressureValuesToCalculate = getListOfGoodData(allLastPressureValuesToCalculate);
        if (isThereData(allLastPressureValuesToCalculate)){
            averagePressureData = calculateRelativeAverage(allLastPressureValuesToCalculate);
            standardDeviationAll = calculateStandardDeviation(allLastPressureValuesToCalculate, averagePressureData);
        }
        if (isThereData(goodLastPressureValuesToCalculate)){
            averageGoodPressureData = calculateRelativeAverage(goodLastPressureValuesToCalculate);
            standardDeviationGood = calculateStandardDeviation(goodLastPressureValuesToCalculate, averageGoodPressureData);
        }
        databaseController.addMainPressureData(mainData, pressureAccuracy, averagePressureData, averageGoodPressureData,
                goodLastPressureValuesToCalculate.size(), allLastPressureValuesToCalculate.size(),standardDeviationAll, standardDeviationGood);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void recordReferenceLightData(Long referenceData, List<DataRecordedByTheSensors> allLastLightValuesToCalculate){
        Float averageLightData = 0f;
        Float averageGoodLightData = 0f;
        Float standardDeviationAll = 0f;
        Float standardDeviationGood = 0f;
        List<DataRecordedByTheSensors> goodLastLightValuesToCalculate = getListOfGoodData(allLastLightValuesToCalculate);

        if (isThereData(allLastLightValuesToCalculate)){
            averageLightData = calculateRelativeAverage(allLastLightValuesToCalculate);
            standardDeviationAll = calculateStandardDeviation(allLastLightValuesToCalculate, averageLightData);
        }
        if (isThereData(goodLastLightValuesToCalculate)){
            averageGoodLightData = calculateRelativeAverage(goodLastLightValuesToCalculate);
            standardDeviationGood = calculateStandardDeviation(goodLastLightValuesToCalculate, averageGoodLightData);
        }

        Float goodLightStandardized = 0f;
        Float allLightStandardized = 0f;

        if (!sensorAdjusterController.isListEmpty()){
            goodLightStandardized = sensorAdjusterController.adjustData(averageGoodLightData);
            allLightStandardized = sensorAdjusterController.adjustData(averageLightData);
        }
        databaseController.addReferenceLightData(referenceData, lightAccuracy, averageLightData, averageGoodLightData,
                allLightStandardized, goodLightStandardized, goodLastLightValuesToCalculate.size(), allLastLightValuesToCalculate.size(),standardDeviationAll,
                standardDeviationGood, worstXAngle, worstYAngle);
        //Log.e("Register new angle", "worstAngleX : " + worstXAngle + "  worstAngleY : " + worstYAngle);
        //AddAllDataForTest(referenceData, allLastLightValuesToCalculate);
    }


    private void AddAllDataForTest(Long idData, List<DataRecordedByTheSensors> allLastLightValuesToCalculate){
        int i = 0;
        do {
            databaseController.addTestData(allLastLightValuesToCalculate.get(i).getData(), allLastLightValuesToCalculate.get(i).getTimeRecorded(), idData);
            if (allLastLightValuesToCalculate.get(i).getGoodData()){
                databaseController.addTestGoodData(allLastLightValuesToCalculate.get(i).getData(), allLastLightValuesToCalculate.get(i).getTimeRecorded(), idData);
            }
            i++;
        }while (i < allLastLightValuesToCalculate.size());
    }



    private Boolean isThereData(List<DataRecordedByTheSensors> values){
        Boolean answer = true;
        if (values == null) {
            answer = false;
        }else if (values.size() <= 1){

            if (values.size() == 0){
                answer = false;
            } else if (values.get(0).getData() == null){
                answer = false;
            }

        }else if (values.get(0).getData() == null){
                values.remove(0);
        }

        return answer;
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private Float calculateRelativeAverage(List<DataRecordedByTheSensors> listOfData) {
        Float totalWeightedData = 0f;
        Long totalRecordTime = 0l;

        for (Integer index = 0; index < listOfData.size(); index++) {
            Float data = listOfData.get(index).getData();
            Long timeRecorded = listOfData.get(index).getTimeRecorded();

            totalRecordTime += timeRecorded;
            Float weightedData = data * timeRecorded;
            totalWeightedData += weightedData;
        }
        Float relativeAverage = 0f;
        if (totalRecordTime != 0){
            relativeAverage = totalWeightedData / totalRecordTime;
        }

        return relativeAverage;
    }

    private Float calculateStandardDeviation(List<DataRecordedByTheSensors> listOfData, Float relativeAverage){
        /*Float average = calculateArithmeticAverageDataRecordedByTheSensors(listOfData);
        List<Float> differenceBetweenAverageAndData = new ArrayList<Float>();
        int i = 0;
        do {
            differenceBetweenAverageAndData.add(listOfData.get(i).getData() - average);
            i++;
        }while (i < listOfData.size()-1);

        Float arithmeticAverage = calculateArithmeticAverage(differenceBetweenAverageAndData);
        return arithmeticAverage;*/

        Float totalAbsoluteDifference = 0f;
        Long totalRecordTime = 0l;

        for (Integer index = 0; index < listOfData.size(); index++) {
            Float data = listOfData.get(index).getData();
            Long timeRecorded = listOfData.get(index).getTimeRecorded();
            Float absoluteDifference = Math.abs(data - relativeAverage) * timeRecorded;
            totalRecordTime += timeRecorded;
            totalAbsoluteDifference += absoluteDifference;
        }

        Float standardDeviation = 0f;
        if (totalRecordTime != 0){
            standardDeviation = totalAbsoluteDifference / totalRecordTime;
        }
        return standardDeviation;
    }

    /*private Long getTotalRecordTime(List<DataRecordedByTheSensors> dataRecordedByTheSensors) {
        Long totalRecordTime = 0l;

        for (Integer index = 0; index < dataRecordedByTheSensors.size(); index++) {
            totalRecordTime += dataRecordedByTheSensors.get(index).getTimeRecorded();
        }

        return totalRecordTime;
    }*/

    /*@RequiresApi(api = Build.VERSION_CODES.O)
    private Float calculateRelativeAverageForMoreThanOneData(List<DataRecordedByTheSensors> listOfData) {
        int i = 1;
        Float weightOnTheMeasure;
        Float averageData = 0f;
        Long timeSinceStartOfFunction = Instant.now().toEpochMilli();

        weightOnTheMeasure = Float.valueOf(listOfData.get(0).getTimeRecorded() - firstTimeDataWasRecorded) / Float.valueOf(timeSinceStartOfFunction - firstTimeDataWasRecorded);
        averageData = weightOnTheMeasure * listOfData.get(0).getData();

        while (i < listOfData.size()-2) {
            weightOnTheMeasure = Float.valueOf(listOfData.get(i+1).getTimeRecorded() - listOfData.get(i).getTimeRecorded()) / Float.valueOf(timeSinceStartOfFunction - firstTimeDataWasRecorded);
            averageData += weightOnTheMeasure * listOfData.get(i).getData();
            i++;
        }

        weightOnTheMeasure = Float.valueOf(timeSinceStartOfFunction - listOfData.get(i).getTimeRecorded()) / Float.valueOf(timeSinceStartOfFunction - firstTimeDataWasRecorded);
        averageData += weightOnTheMeasure * listOfData.get(i).getData();

        return averageData;
    }*/

    /*private Float calculateArithmeticAverageDataRecordedByTheSensors(List<DataRecordedByTheSensors> listOfData){
        int i = 0;
        Float total = 0f;
        do {
            total += listOfData.get(i).getData();
            i++;
        }while (i < listOfData.size()-1);

        return (total / listOfData.size());
    }*/

    /*private Float calculateArithmeticAverage(List<Float> listOfData){
        int i = 0;
        Float total = 0f;
        do {
            total += Math.abs(listOfData.get(i));
            i++;
        }while (i < listOfData.size()-1);

        return (total / listOfData.size());
    }*/

    private void addNewLightValueToList(Float lightValue){
        if (rightAngle && lightAccuracy >= MINIMUM_ACCURACY_TO_RECORD){
            allLastLightValues.add(new DataRecordedByTheSensors(lightValue, true));
        } else {
            allLastLightValues.add(new DataRecordedByTheSensors(lightValue, false));
        }
        Integer listSize = allLastLightValues.size();
        Long timeFromLastItem = allLastLightValues.get(listSize - 1).getTimeRecorded();
        Long timeFromPreviousItem = allLastLightValues.get(listSize - 2).getTimeRecorded();

        allLastLightValues.get(listSize - 2).setTimeRecorded(timeFromLastItem - timeFromPreviousItem);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onLightDataChanged(Float lightValue) {
        if (recording){
            addNewLightValueToList(lightValue);
        } else if (rightAngle && lightAccuracy >= MINIMUM_ACCURACY_TO_RECORD){
            lastLightRecorded = null;
            lastLightRecorded = new DataRecordedByTheSensors(lightValue, true);
        } else {
            lastLightRecorded = null;
            lastLightRecorded = new DataRecordedByTheSensors(lightValue, false);
        }
    }

    @Override
    public void onNewSensorAccuracyForLightChanged(Integer accuracy) {
        lightAccuracy = accuracy;
    }

    private void addNewPressureValueToList(Float pressureValue){
        if (rightAngle && lightAccuracy >= MINIMUM_ACCURACY_TO_RECORD){
            allLastPressureValues.add(new DataRecordedByTheSensors(pressureValue, true));
        } else {
            allLastPressureValues.add(new DataRecordedByTheSensors(pressureValue, false));
        }
        Integer listSize = allLastPressureValues.size();
        Long timeFromLastItem = allLastPressureValues.get(listSize - 1).getTimeRecorded();
        Long timeFromPreviousItem = allLastPressureValues.get(listSize - 2).getTimeRecorded();

        allLastPressureValues.get(listSize - 2).setTimeRecorded(timeFromLastItem - timeFromPreviousItem);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onPressureDataChanged(Float pressureValue) {
        if (cellphoneMode == DatabaseConstants.CellphoneMode.main){
            if (recording){
                addNewPressureValueToList(pressureValue);
            }else if(pressureAccuracy >= MINIMUM_ACCURACY_TO_RECORD){
                lastPressureRecorded = null;
                lastPressureRecorded = new DataRecordedByTheSensors(pressureValue, true);

            }else{
                lastPressureRecorded = null;
                lastPressureRecorded = new DataRecordedByTheSensors(pressureValue,false);
            }
        }
    }

    @Override
    public void onNewSensorAccuracyForPressureChanged(Integer accuracy) {
        pressureAccuracy = accuracy;
    }

    @Override
    public void onSensorForAngleChanged(Float angleX, Float angleY) {
        if ((Math.abs(angleX) > SettingsController.getMaximumOrientation() || (Math.abs(angleY) > SettingsController.getMaximumOrientation()))){
            rightAngle = false;
        }else {
            rightAngle = true;
        }
        checkNewWorstAngle(Math.abs(angleX), Math.abs(angleY));
    }

    private void checkNewWorstAngle(Float angleX, Float angleY){
        if (angleX > worstXAngle){
            worstXAngle = angleX;
        }
        if (angleY > worstYAngle){
            worstYAngle = angleY;
        }
    }

    @Override
    public void onNewSensorAccuracyForAngleChanged(Integer accuracy) {
        angleAccuracy = accuracy;
    }

    public void setIDToUseOnRecord(Long IDToUseOnRecord) {
        this.IDToUseOnRecord = IDToUseOnRecord;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }
}