package com.mffpdrf.lightproject.Controllers;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.mffpdrf.lightproject.Constants.TagConstants;

import java.util.ArrayList;
import java.util.List;

public class BluetoothController {
    Context context;

    private Boolean discoveringPhones = false;

    List<OnBluetoothChangeListener>  listener = new ArrayList<>();

    BluetoothAdapter bluetoothAdapter;
    BluetoothDevice bluetoothDevice;

    public List<BluetoothDevice> phonesDiscovered = new ArrayList<>();

    public BluetoothController(Context context) {
        this.context = context;
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        context.registerReceiver(bondStateChangeBroadcastReceiver, filter);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    private BroadcastReceiver unpairedDevicesBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (action.equals(BluetoothDevice.ACTION_FOUND)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                addDiscoveredPhone(device);
            }
        }
    };

    private BroadcastReceiver bondStateChangeBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (action.equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (device.getBondState() == BluetoothDevice.BOND_BONDED) {
                    bluetoothDevice = device;
                }
            }
        }
    };

    private void unregisterReceivers() {
        try {
            context.unregisterReceiver(bondStateChangeBroadcastReceiver);
            if (discoveringPhones) {
                context.unregisterReceiver(unpairedDevicesBroadcastReceiver);
            }
        } catch (IllegalArgumentException exception) {
            Log.e(TagConstants.RECEIVER_UNREGISTER_ERROR, "unregisterReceivers: ", exception);
        }
    }

    public void discoverPhones() {
        if (bluetoothAdapter != null) {
            if (bluetoothAdapter.isDiscovering()) {
                bluetoothAdapter.cancelDiscovery();
            }
            bluetoothAdapter.startDiscovery();
            discoveringPhones = true;
        }
        IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        context.registerReceiver(unpairedDevicesBroadcastReceiver, discoverDevicesIntent);
    }

    private void cancelPhoneDiscovery() {
        if (discoveringPhones) {
            bluetoothAdapter.cancelDiscovery();
        }
    }

    public void stop() {
        cancelPhoneDiscovery();
        unregisterReceivers();
    }

    public void enableBluetooth() {
        if (bluetoothAdapter != null){
            if (!bluetoothAdapter.isEnabled()) {
                Intent enableBluetoothIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                context.startActivity(enableBluetoothIntent);
            }
        }
    }

    public void enablePhoneDiscoverability() {
        int scanMode = bluetoothAdapter.getScanMode();
        if (scanMode != BluetoothAdapter.SCAN_MODE_CONNECTABLE || scanMode != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 30);
            context.startActivity(discoverableIntent);
        }
    }

    private void addDiscoveredPhone(BluetoothDevice device){
        BluetoothClass deviceClass = device.getBluetoothClass();
        if (deviceClass != null && deviceClass.getDeviceClass() == BluetoothClass.Device.PHONE_SMART && !phonesDiscovered.contains(device)) {
            phonesDiscovered.add(device);
            for(OnBluetoothChangeListener currentListener : listener) {
                currentListener.onPhoneDiscovered(device);
            }
        }
    }

    public void setOnBluetoothChange(OnBluetoothChangeListener listener) {
        this.listener.add(listener);
    }

    public interface OnBluetoothChangeListener {
        void onPhoneDiscovered(BluetoothDevice device);
    }
}