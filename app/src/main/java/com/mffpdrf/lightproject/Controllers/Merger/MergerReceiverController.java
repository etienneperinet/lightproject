package com.mffpdrf.lightproject.Controllers.Merger;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.mffpdrf.lightproject.Constants.BluetoothConstants;
import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Constants.ErrorMessagesConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.Controllers.DatabaseController;
import com.mffpdrf.lightproject.Events.OnMergeEventListener;
import com.mffpdrf.lightproject.Models.DatabaseModels.Cellphone;
import com.mffpdrf.lightproject.Models.DatabaseModels.GraphMultiplier;
import com.mffpdrf.lightproject.Models.DatabaseModels.GraphMultiplierPoint;
import com.mffpdrf.lightproject.Models.ReceptionConfirmation;
import com.mffpdrf.lightproject.Models.DatabaseModels.MainData;
import com.mffpdrf.lightproject.Models.DatabaseModels.MainLightData;
import com.mffpdrf.lightproject.Models.DatabaseModels.MainPressureData;
import com.mffpdrf.lightproject.Models.DatabaseModels.MainSession;
import com.mffpdrf.lightproject.Models.DatabaseModels.Microsite;
import com.mffpdrf.lightproject.Models.DatabaseModels.Observer;
import com.mffpdrf.lightproject.Models.DatabaseModels.Project;
import com.mffpdrf.lightproject.Models.DatabaseModels.Site;
import com.mffpdrf.lightproject.Services.BluetoothConnectionService;

import org.json.JSONException;
import org.json.JSONObject;

public class MergerReceiverController extends MergerController implements BluetoothConnectionService.OnMergeObjectReceiveObjectListener {
    static MergerReceiverController instance;
    private Handler handler;
    private Runnable runnable;

    BluetoothConnectionService bluetoothConnectionService;
    private Boolean startingMerge = true;
    DatabaseController databaseController = DatabaseController.getInstance(null);
    private OnMergeEventListener listenerMerger;
    MergerFileController mergerFileController = new MergerFileController(context);

    public MergerReceiverController(BluetoothConnectionService bluetoothConnectionService, Context context, OnMergeEventListener setOnListener) {
        super(context);
        setOnMergeEventListener(setOnListener);
        this.bluetoothConnectionService = bluetoothConnectionService;
        bluetoothConnectionService.setOnMergeObjectReceiveObjectListener(this);

        handler = new Handler(Looper.getMainLooper());
        runnable = new Runnable() {
            @Override
            public void run() {
                listenerMerger.onMergeFailed(ErrorMessagesConstants.BLUETOOTH_CONNECTION_INTERRUPTED);
                instance = null;
            }
        };
    }

    public static MergerReceiverController getInstance(BluetoothConnectionService bluetoothConnectionService, Context context, OnMergeEventListener setOnListener){
        if (instance == null){
            instance = new MergerReceiverController(bluetoothConnectionService, context, setOnListener);
        }
        return instance;
    }

    public void setOnMergeEventListener(OnMergeEventListener setOnListener){
        listenerMerger = setOnListener;
    }

    public void addToDatabaseIfNewOrSendAStraightConfirmation(JSONObject jsonObject){
        if (databaseController.requiresMerge()){
            try {
                lastJSONObject = mergerFileController.readFile();
                //lastJSONObject = databaseController.readFile();
                if (lastJSONObject != null && lastJSONObject.toString().equals(jsonObject.toString())){
                    sendConfirmation(new ReceptionConfirmation(jsonObject.getString("JSONType"), jsonObject.getString("JSONType")));
                } else {
                    createConfirmationAndAddObjectToDatabase(jsonObject);
                    mergerFileController.writeFile(jsonObject);
                }
            } catch (JSONException exception) {
                Log.e(TagConstants.JSON_ERROR, "verifyLastObjectAddedToDB " + exception.getMessage());
                exception.printStackTrace();
            }
        }
        startingMerge = false;
    }

    private void createConfirmationAndAddObjectToDatabase(JSONObject jsonObject){
        try {
            switch (jsonObject.getString("JSONType")){
                case DatabaseConstants.TABLE_CELLPHONE:
                    addCellphoneToDatabase(jsonObject);
                    break;
                case DatabaseConstants.TABLE_PROJECT:
                    addProjectToDatabase(jsonObject);
                    break;
                case DatabaseConstants.TABLE_MAIN_SESSION:
                    addMainSessionToDatabase(jsonObject);
                    break;
                case DatabaseConstants.TABLE_OBSERVER:
                    addObserverToDatabase(jsonObject);
                    break;
                case DatabaseConstants.TABLE_SITE:
                    addSiteToDatabase(jsonObject);
                    break;
                case DatabaseConstants.TABLE_MICROSITE:
                    addMicrositeToDatabase(jsonObject);
                    break;
                case DatabaseConstants.TABLE_MAIN_DATA:
                    addMainDataToDatabase(jsonObject);
                    break;
                case DatabaseConstants.TABLE_MAIN_LIGHT_DATA:
                    addMainLightDataToDatabase(jsonObject);
                    break;
                case DatabaseConstants.TABLE_MAIN_PRESSURE_DATA:
                    addMainPressureDataToDatabase(jsonObject);
                    break;
                case DatabaseConstants.TABLE_GRAPH_MULTIPLIER:
                    addGraphMultiplierToDatabase(jsonObject);
                    break;
                case DatabaseConstants.TABLE_GRAPH_POINT_MULTIPLIER:
                    addGraphMultiplierPointToDatabase(jsonObject);
                    break;
                case BluetoothConstants.MERGE_FINISHED:
                    if (databaseController.export()) {
                        //sendConfirmation
                        handler.removeCallbacks(runnable);
                        listenerMerger.onMergeDone();
                        handler.removeCallbacks(runnable);
                        sendConfirmation(new ReceptionConfirmation(BluetoothConstants.MERGE_FINISHED, "null"));
                    } else {
                        //Toast.makeText(context, "Il y a eu un problème lors de l'exportation de la base de données", Toast.LENGTH_LONG).show();
                        listenerMerger.onMergeFailed(ErrorMessagesConstants.DATABASE_EXPORT_FAILED);
                    }
                    break;
                default:
            }
        } catch (JSONException exception){
            Log.e(TagConstants.JSON_ERROR, "createConfirmation : " + exception.getMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private void sendConfirmation(ReceptionConfirmation confirmation){
        bluetoothConnectionService.write(confirmation.toJSON().toString().getBytes());
        Log.d(TagConstants.JSON_CONFIRMATION_SENT, "createConfirmation: " + confirmation.toJSON().toString());
        handler.removeCallbacks(runnable);
    }

    private void addCellphoneToDatabase(JSONObject jsonObject){
        ReceptionConfirmation confirmation = null;
        try {
            confirmation = new ReceptionConfirmation(DatabaseConstants.TABLE_CELLPHONE, jsonObject.getString("id"));
            databaseController.addCellphone(new Cellphone(jsonObject));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "createConfirmation (Cellphone) : " + exception.getMessage());
        }
        sendConfirmation(confirmation);
    }

    private void addProjectToDatabase(JSONObject jsonObject){
        ReceptionConfirmation confirmation = null;
        try {
            confirmation = new ReceptionConfirmation(DatabaseConstants.TABLE_PROJECT, jsonObject.getString("number"));
            databaseController.addProject(new Project(jsonObject));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "createConfirmation (Project): " + exception.getMessage());
        }
        sendConfirmation(confirmation);
    }

    private void addMainSessionToDatabase(JSONObject jsonObject){
        ReceptionConfirmation confirmation = null;
        try {
            confirmation = new ReceptionConfirmation(DatabaseConstants.TABLE_MAIN_SESSION, jsonObject.getString("id"));
            databaseController.addMainSession(new MainSession(jsonObject));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "createConfirmation (MainSession) : " + exception.getMessage());
        }
        sendConfirmation(confirmation);
    }

    private void addObserverToDatabase(JSONObject jsonObject){
        ReceptionConfirmation confirmation = null;
        try {
            confirmation = new ReceptionConfirmation(DatabaseConstants.TABLE_OBSERVER, jsonObject.getString("id"));
            databaseController.addObserver(new Observer(jsonObject));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "createConfirmation (Observer) : " + exception.getMessage());
        }
        sendConfirmation(confirmation);
    }

    private void addSiteToDatabase(JSONObject jsonObject){
        ReceptionConfirmation confirmation = null;
        try {
            confirmation = new ReceptionConfirmation(DatabaseConstants.TABLE_SITE, jsonObject.getString("id"));
            databaseController.addSite(new Site(jsonObject));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "createConfirmation (Site) : " + exception.getMessage());
        }
        sendConfirmation(confirmation);
    }

    private void addMicrositeToDatabase(JSONObject jsonObject){
        ReceptionConfirmation confirmation = null;
        try {
            confirmation = new ReceptionConfirmation(DatabaseConstants.TABLE_MICROSITE, jsonObject.getString("id"));
            databaseController.addMicrosite(new Microsite(jsonObject));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "createConfirmation (Microsite) : " + exception.getMessage());
        }
        sendConfirmation(confirmation);
    }

    private void addMainDataToDatabase(JSONObject jsonObject){
        ReceptionConfirmation confirmation = null;
        try {
            confirmation = new ReceptionConfirmation(DatabaseConstants.TABLE_MAIN_DATA, jsonObject.getString("id"));
            databaseController.addMainData(new MainData(jsonObject));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "createConfirmation (MainData) : " + exception.getMessage());
        }
        sendConfirmation(confirmation);
    }

    private void addMainLightDataToDatabase(JSONObject jsonObject){
        ReceptionConfirmation confirmation = null;
        try {
            confirmation = new ReceptionConfirmation(DatabaseConstants.TABLE_MAIN_LIGHT_DATA, jsonObject.getString("id"));
            databaseController.addMainLightData(new MainLightData(jsonObject));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "createConfirmation (MainLightData) : " + exception.getMessage());
        }
        sendConfirmation(confirmation);
    }

    private void addMainPressureDataToDatabase(JSONObject jsonObject){
        ReceptionConfirmation confirmation = null;
        try {
            confirmation = new ReceptionConfirmation(DatabaseConstants.TABLE_MAIN_PRESSURE_DATA, jsonObject.getString("id"));
            databaseController.addMainPressureData(new MainPressureData(jsonObject));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "createConfirmation (MainPressureData) : " + exception.getMessage());
        }
        sendConfirmation(confirmation);
    }

    private void addGraphMultiplierToDatabase(JSONObject jsonObject){
        ReceptionConfirmation confirmation = null;
        try {
            confirmation = new ReceptionConfirmation(DatabaseConstants.TABLE_GRAPH_MULTIPLIER, jsonObject.getString("id"));
            databaseController.addGraphMultiplier(new GraphMultiplier(jsonObject));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "createConfirmation (GraphMultiplier) : " + exception.getMessage());
        }
        sendConfirmation(confirmation);
    }

    private void addGraphMultiplierPointToDatabase(JSONObject jsonObject){
        databaseController.addGraphPointMultiplier(new GraphMultiplierPoint(jsonObject));
        ReceptionConfirmation confirmation = null;
        try {
            confirmation = new ReceptionConfirmation(DatabaseConstants.TABLE_GRAPH_POINT_MULTIPLIER, jsonObject.getString("id"));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "createConfirmation (GraphMultiplierPoint) : " + exception.getMessage());
        }
        sendConfirmation(confirmation);
    }

    @Override
    public void onReception(JSONObject jsonObject) {
        handler.removeCallbacks(runnable);
        Log.d(TagConstants.JSON_RECEIVED, jsonObject.toString());
        if (startingMerge){
            addToDatabaseIfNewOrSendAStraightConfirmation(jsonObject);
        } else {
            if (lastJSONObject != null) {
                if(!lastJSONObject.toString().equals(jsonObject.toString())){
                    createConfirmationAndAddObjectToDatabase(jsonObject);
                    try {
                        if (!jsonObject.getString("JSONType").equals(BluetoothConstants.MERGE_FINISHED)) {
                            mergerFileController.writeFile(lastJSONObject);
                        }
                    } catch (JSONException exception) {
                        Log.e(TagConstants.JSON_ERROR, "onReception: ", exception);
                    }
                    lastJSONObject = jsonObject;
                }
            } else {
                createConfirmationAndAddObjectToDatabase(jsonObject);
                lastJSONObject = jsonObject;
            }
        }
    }
}