package com.mffpdrf.lightproject.Controllers;

import com.mffpdrf.lightproject.Constants.BluetoothConstants;
import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;

import com.mffpdrf.lightproject.Controllers.Merger.MergerController;
import com.mffpdrf.lightproject.Controllers.Merger.MergerFileController;

import com.mffpdrf.lightproject.Models.DatabaseModels.GraphMultiplier;
import com.mffpdrf.lightproject.Models.DatabaseModels.GraphMultiplierPoint;

import com.mffpdrf.lightproject.Models.DatabaseModels.Cellphone;
import com.mffpdrf.lightproject.Models.DatabaseModels.DataSavingType;
import com.mffpdrf.lightproject.Models.DatabaseModels.MainData;
import com.mffpdrf.lightproject.Models.DatabaseModels.MainLightData;
import com.mffpdrf.lightproject.Models.DatabaseModels.MainPressureData;
import com.mffpdrf.lightproject.Models.DatabaseModels.MainSession;
import com.mffpdrf.lightproject.Models.DatabaseModels.Microsite;

import com.mffpdrf.lightproject.Models.DatabaseModels.Observer;
import com.mffpdrf.lightproject.Models.DatabaseModels.Project;
import com.mffpdrf.lightproject.Models.DatabaseModels.ReferenceSession;
import com.mffpdrf.lightproject.Models.DatabaseModels.Site;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DatabaseController extends SQLiteOpenHelper {
    private static DatabaseController instance = null;

    private Context context;

    private SQLiteDatabase database;

    private Long lastSessionID;
    private String lastProjectNumber;

    private Observer selectedObserver;
    private Project selectedProject;
    private MainSession selectedMainSession;

    private DatabaseController(@Nullable Context context) {
        super(context, DatabaseConstants.DATABASE_NAME, null, DatabaseConstants.DATABASE_VERSION);
        this.context = context;
    }

    public static DatabaseController getInstance(@Nullable Context context){
        if (instance == null){
            instance = new DatabaseController(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createStandardizationTables(db);
        createCentralTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public static void copy(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte[] buf = new byte[1024];
        int len;
        while ((len = inputStream.read(buf)) > 0) {
            outputStream.write(buf, 0, len);
        }
    }

    public void delete() {
        try {
            context.deleteDatabase(DatabaseConstants.DATABASE_NAME);
        } catch (Exception exception) {
            Log.e(TagConstants.DATABASE_ERROR, "delete: ", exception);
        }
    }

    public void deleteData() {
        deleteSite();
        deleteMicrosite();
        deleteReferenceLightData();
        deleteReferencePosition();
        deleteReferenceData();
        deleteReferenceSession();
        deleteMainLightData();
        deleteMainPressureData();
        deleteMainData();
        deleteMainSession();
    }

    public Boolean export() {
        //Request write file
        Boolean exportSucceeded = false;

        try {
            String directoryPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).getPath() + File.separator + "capture_de_lumiere";
            File dir = new File(directoryPath);
            if (!dir.exists()) {
                if (dir.mkdirs()) {
                    Log.d("TAG", "Directory created!");
                } else {
                    Log.e("TAG", "Unable to create directory");
                }
            }

            InputStream inputStream = null;
            OutputStream outputStream = null;

            String date;
            ZonedDateTime zonedDateTime = ZonedDateTime.now();
            date = zonedDateTime.getDayOfMonth() + "-" + zonedDateTime.getMonthValue() + "-" + zonedDateTime.getYear() + "_" + zonedDateTime.getHour() + "h" + zonedDateTime.getMinute() + "m" + zonedDateTime.getSecond() + "s";

            String fileName = DatabaseConstants.DATABASE_NAME;
            File inFile = new File (Environment.getDataDirectory(), "//data//"+context.getPackageName()+"//databases//" + fileName);

            try {
                inputStream = new FileInputStream(inFile);
                File outFile = new File(directoryPath, date + ".db");
                outputStream = new FileOutputStream(outFile);
                copy(inputStream, outputStream);
                exportSucceeded = true;
            } catch (IOException exception) {
                Log.e(TagConstants.FILE_ERROR, "exportDatabase: ", exception);
            }

        } catch (Exception exception) {
            Log.e(TagConstants.GENERIC_ERROR, "exportDatabase: ", exception);
        }

        return exportSucceeded;
    }

    public Boolean requiresMerge() {
        Boolean requiresMerge = false;

        MergerFileController mergerFileController = new MergerFileController(context);
        JSONObject file = mergerFileController.readFile();

        if (file != null) {
            try {
                String jsonType = file.getString("JSONType");
                if (jsonType.equals(BluetoothConstants.MERGE_FINISHED)) {
                    if (getTotalMainSessions() > 0 || getTotalReferenceSessions() > 0) {
                        requiresMerge = true;
                    }
                } else {
                    requiresMerge = true;
                }
            } catch (JSONException exception) {
                Log.e(TagConstants.JSON_ERROR, "merged: ", exception);
            }
        } else {
            if (getTotalMainSessions() > 0 || getTotalReferenceSessions() > 0) {
                requiresMerge = true;
            }
        }

        return requiresMerge;
    }

    public Boolean didMergeFailed() {
        Boolean mergeFailed = true;

        MergerFileController mergerFileController = new MergerFileController(context);
        JSONObject file = mergerFileController.readFile();
        //JSONObject file = readFile();
        try {
            if (file != null){
                if (file.getString("JSONType").equals(BluetoothConstants.MERGE_FINISHED)){
                    mergeFailed = false;
                }
            } else {
                mergeFailed = false;
            }
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "merged: ", exception);
        }
        return mergeFailed;
    }

    public Observer getSelectedObserver() {
        return selectedObserver;
    }

    public Project getSelectedProject() {
        return selectedProject;
    }

    public void setSelectedObserver(Observer selectedObserver) {
        this.selectedObserver = selectedObserver;
    }

    public void setSelectedProject(Project selectedProject) {
        this.selectedProject = selectedProject;
    }

    public void setSelectedMainSession(MainSession selectedMainSession) {
        this.selectedMainSession = selectedMainSession;
    }
    private void createStandardizationTables(SQLiteDatabase db){
        db.execSQL(DatabaseConstants.CREATE_TABLE_GRAPH_MULTIPLIER);
        db.execSQL(DatabaseConstants.CREATE_TABLE_GRAPH_POINT_MULTIPLIER);
    }

    private void createCentralTable(SQLiteDatabase db){
        db.execSQL(DatabaseConstants.CREATE_TABLE_PROJECT);
        db.execSQL(DatabaseConstants.CREATE_TABLE_CELLPHONE);
        db.execSQL(DatabaseConstants.CREATE_TABLE_MAIN_SESSION);
        db.execSQL(DatabaseConstants.CREATE_TABLE_SITE);
        db.execSQL(DatabaseConstants.CREATE_TABLE_OBSERVER);
        db.execSQL(DatabaseConstants.CREATE_TABLE_DATA_SAVING_TYPE);
        db.execSQL(DatabaseConstants.CREATE_TABLE_REFERENCE_SESSION);
        db.execSQL(DatabaseConstants.CREATE_TABLE_REFERENCE_POSITION);
        db.execSQL(DatabaseConstants.CREATE_TABLE_REFERENCE_DATA);
        db.execSQL(DatabaseConstants.CREATE_TABLE_MICROSITE);
        db.execSQL(DatabaseConstants.CREATE_TABLE_MAIN_DATA);
        db.execSQL(DatabaseConstants.CREATE_TABLE_MAIN_LIGHT_DATA);
        db.execSQL(DatabaseConstants.CREATE_TABLE_REFERENCE_LIGHT_DATA);
        db.execSQL(DatabaseConstants.CREATE_TABLE_MAIN_PRESSURE_DATA);

        db.execSQL(DatabaseConstants.INSERT_MOBILE_DATA_SAVING_TYPE);
        db.execSQL(DatabaseConstants.INSERT_IMMOBILE_DATA_SAVING_TYPE);

        db.execSQL(DatabaseConstants.CREATE_TABLE_TEST_DATA);
        db.execSQL(DatabaseConstants.CREATE_TABLE_TEST_GOOD_DATA);
    }

    //Add (recording)

    public Long addTestData(Float value, Long time, Long idRealData) {
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("time", time);
        contentValues.put("value", value);
        contentValues.put("id_real_data", idRealData);
        return database.insert(DatabaseConstants.TEST_DATA, null, contentValues);
    }

    public Long addTestGoodData(Float value, Long time, Long idRealData) {
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("time", time);
        contentValues.put("value", value);
        contentValues.put("id_real_data", idRealData);
        return database.insert(DatabaseConstants.TEST_GOOD_DATA, null, contentValues);
    }

    public Long addSite(String name) {
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        if (lastProjectNumber != null)//Pour debug
        {
            contentValues.put("project_number", lastProjectNumber);
        }

        contentValues.put("main_session_id", lastSessionID);

        return database.insert(DatabaseConstants.TABLE_SITE, null, contentValues);
    }

    public Long addMicrosite(String prefix, Integer number, String description, Integer dataSavingTypeID, Long siteID, Double latitude, Double longitude){
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("prefix", prefix);
        contentValues.put("number", number);
        contentValues.put("description", description);
        if (latitude != null && latitude != 0) {
            contentValues.put("gps_latitude", latitude);
        }
        if (longitude != null && longitude != 0) {
            contentValues.put("gps_longitude", longitude);
        }
        contentValues.put("site_id", siteID);
        contentValues.put("data_saving_type_id", dataSavingTypeID);
        return database.insert(DatabaseConstants.TABLE_MICROSITE, null, contentValues);
    }

    public Long addMainData(Long micrositeID, Long timePassed, Double gpsLatitude, Double gpsLongitude){
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("microsite_id", micrositeID);
        contentValues.put("time_passed", timePassed);
        if (gpsLatitude != null && gpsLatitude != 0) {
            contentValues.put("gps_latitude", gpsLatitude);
        }
        if (gpsLongitude != null && gpsLongitude != 0) {
            contentValues.put("gps_longitude", gpsLongitude);
        }

        return database.insert(DatabaseConstants.TABLE_MAIN_DATA, null, contentValues);
    }

    public Long addMainLightData(Long id, Integer sensorStatus, Float allLightValues,  Float goodLightValues,
                                 Float allStandardizedValues,  Float goodStandardizedValues,
                                 Integer numberOfGoodObservation, Integer numberOfObservation,
                                 Float standardDeviationAll, Float standardDeviationGood,
                                 Float rotationCellX, Float rotationCellY){
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("sensor_status", sensorStatus);

        contentValues.put("all_values", allLightValues);
        contentValues.put("good_values", goodLightValues);

        if (allStandardizedValues != null && goodStandardizedValues != null){
            contentValues.put("all_standardized_values", allStandardizedValues);
            contentValues.put("good_standardized_values", goodStandardizedValues);
        }

        contentValues.put("number_good_observations", numberOfGoodObservation);
        contentValues.put("number_observations", numberOfObservation);

        contentValues.put("standard_deviation_good", standardDeviationGood);
        contentValues.put("standard_deviation_all", standardDeviationAll);

        contentValues.put("rotation_cell_x", rotationCellX);
        contentValues.put("rotation_cell_y", rotationCellY);

        return database.insert(DatabaseConstants.TABLE_MAIN_LIGHT_DATA, null, contentValues);
    }

    public Long addMainPressureData(Long id, Integer sensorReport, Float allLightValues, Float goodLightValues,
                                    Integer numberOfGoodObservation, Integer numberOfObservation,
                                    Float standardDeviationAll, Float standardDeviationGood) {
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("sensor_status", sensorReport);

        contentValues.put("all_values", allLightValues);
        contentValues.put("good_values", goodLightValues);

        contentValues.put("number_good_observations", numberOfGoodObservation);
        contentValues.put("number_observations", numberOfObservation);

        contentValues.put("standard_deviation_good", standardDeviationGood);
        contentValues.put("standard_deviation_all", standardDeviationAll);


        return database.insert(DatabaseConstants.TABLE_MAIN_PRESSURE_DATA, null, contentValues);
    }

    public void addReferenceLightData(Long id, Integer sensorStatus, Float allLightValues,  Float goodLightValues,
                                      Float allStandardizedValues,  Float goodStandardizedValues,
                                      Integer numberOfGoodObservation, Integer numberOfObservation,
                                      Float standardDeviationAll, Float standardDeviationGood,
                                      Float rotationCellX, Float rotationCellY){
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("sensor_status", sensorStatus);

        contentValues.put("all_values", allLightValues);
        contentValues.put("good_values", goodLightValues);

        if (allStandardizedValues != null && goodStandardizedValues != null){
            contentValues.put("all_standardized_values", allStandardizedValues);
            contentValues.put("good_standardized_values", goodStandardizedValues);
        }

        contentValues.put("number_good_observations", numberOfGoodObservation);
        contentValues.put("number_observations", numberOfObservation);

        contentValues.put("standard_deviation_good", standardDeviationGood);
        contentValues.put("standard_deviation_all", standardDeviationAll);

        contentValues.put("rotation_cell_x", rotationCellX);
        contentValues.put("rotation_cell_y", rotationCellY);

        database.insert(DatabaseConstants.TABLE_REFERENCE_LIGHT_DATA, null, contentValues);
    }

    public void addObserver(String firstName, String lastName, String employeeCode){
        if (!observerExists(employeeCode)) {
            database = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("first_name", firstName);
            contentValues.put("last_name", lastName);
            contentValues.put("employee_code", employeeCode);
            database.insert(DatabaseConstants.TABLE_OBSERVER, null, contentValues);
        }
    }

    public void addProject(String number, String name){
        if (!projectExists(number)) {
            database = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("number", number);
            contentValues.put("name", name);
            database.insert(DatabaseConstants.TABLE_PROJECT, null, contentValues);
        }
    }

    public void addCellphone(String id){
        if (!cellphoneExists(id)) {
            database = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("id", id);
            database.insert(DatabaseConstants.TABLE_CELLPHONE, null, contentValues);
        }
    }

    public Long addReferenceSession(String cellphoneID){
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("cellphone_id", cellphoneID);

        lastSessionID = database.insert(DatabaseConstants.TABLE_REFERENCE_SESSION, null, contentValues);
        return lastSessionID;
    }

    public Long addReferencePosition(Double gpsLatitude, Double gpsLongitude){
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        if (gpsLatitude != null && gpsLatitude != 0) {
            contentValues.put("gps_latitude", gpsLatitude);
        }
        if (gpsLongitude != null && gpsLongitude != 0) {
            contentValues.put("gps_longitude", gpsLongitude);
        }
        contentValues.put("reference_session_id", lastSessionID);
        return database.insert(DatabaseConstants.TABLE_REFERENCE_POSITION, null, contentValues);
    }

    public Long addReferenceData(Long referenceDataSessionID, Long timePassed){
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("time_passed", timePassed);
        contentValues.put("reference_position_id", referenceDataSessionID);
        return database.insert(DatabaseConstants.TABLE_REFERENCE_DATA, null, contentValues);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public Long addGraphMultiplier(LocalDateTime date, String cellphoneID){
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("year", date.getYear());
        contentValues.put("month", date.getMonthValue());
        contentValues.put("day", date.getDayOfMonth());

        contentValues.put("cellphone_id", cellphoneID);
        return database.insert(DatabaseConstants.TABLE_GRAPH_MULTIPLIER, null, contentValues);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public Long addGraphMultiplier(GraphMultiplier graphMultiplier){
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("year", graphMultiplier.getDate().getYear());
        contentValues.put("month",  graphMultiplier.getDate().getMonthValue());
        contentValues.put("day",  graphMultiplier.getDate().getDayOfMonth());
        contentValues.put("cellphone_id",  graphMultiplier.getCellphone().getID());
        return database.insert(DatabaseConstants.TABLE_GRAPH_MULTIPLIER, null, contentValues);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public Long addGraphPointMultiplier(Float multiplier, Float dataRecorded, Long graphID){
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("multiplier", multiplier);
        contentValues.put("data_recorded", dataRecorded);
        contentValues.put("graph_point_multiplier_id", graphID);
        return database.insert(DatabaseConstants.TABLE_GRAPH_POINT_MULTIPLIER, null, contentValues);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public Long addGraphPointMultiplier(GraphMultiplierPoint graphMultiplierPoint){
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("multiplier", graphMultiplierPoint.getDataMultiplier());
        contentValues.put("data_recorded", graphMultiplierPoint.getDataSeen());
        contentValues.put("graph_id", graphMultiplierPoint.getGraphMultiplier().getID());
        return database.insert(DatabaseConstants.TABLE_GRAPH_POINT_MULTIPLIER, null, contentValues);
    }

    //Get (all)

    public ArrayList<Observer> getObservers(){
        ArrayList<Observer> observers = new ArrayList<>();
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_OBSERVERS, null);
        if (cursor.moveToFirst()){
            do {
                Observer observer = new Observer();
                observer.setID(cursor.getLong(0));
                observer.setFirstName(cursor.getString(1));
                observer.setLastName(cursor.getString(2));
                observer.setEmployeeCode(cursor.getString(3));
                observers.add(observer);
            } while (cursor.moveToNext());
        }
        return observers;
    }

    public Integer getTotalMainSessions(){
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_MAIN_SESSIONS, null);
        Integer mainSessions = 0;
        if (cursor.moveToFirst()){
            do {
                mainSessions++;
            } while (cursor.moveToNext());
        }
        return mainSessions;
    }

    public Integer getTotalReferenceSessions(){
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_REFERENCE_SESSIONS, null);
        Integer referenceSessions = 0;
        if (cursor.moveToFirst()){
            do {
                referenceSessions++;
            } while (cursor.moveToNext());
        }
        return referenceSessions;
    }

    public ArrayList<Project> getProjects(){
        ArrayList<Project> projects = new ArrayList<>();
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_PROJECTS, null);
        if (cursor.moveToFirst()){
            do {
                Project project = new Project();
                project.setNumber(cursor.getString(0));
                project.setName(cursor.getString(1));
                projects.add(project);
            } while (cursor.moveToNext());
        }
        return projects;
    }

    public ArrayList<Cellphone> getCellphones(){
        ArrayList<Cellphone> cellphones = new ArrayList<>();
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_CELLPHONES, null);
        if (cursor.moveToFirst()){
            do {
                Cellphone cellphone = new Cellphone();
                cellphone.setID(cursor.getString(0));
                cellphone.setLightSensorSaturation(cursor.getFloat(1));
                cellphones.add(cellphone);
            } while (cursor.moveToNext());
        }
        return cellphones;
    }

    public List<Microsite> getMicrositesFromSite(Long idSite){
        List<Microsite> arrayListToReturn = new ArrayList<>();
        Microsite microsite = null;
        Site site = null;
        DataSavingType dataSavingType = null;
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_MICROSITES_WITH_SITE, new String[]{ idSite.toString()});
        if (cursor.moveToFirst()){
            do {
                microsite = new Microsite();
                site = new Site();
                dataSavingType = new DataSavingType();
                microsite.setID(cursor.getLong(0));
                microsite.setPrefix(cursor.getString(1));
                microsite.setNumber(cursor.getInt(2));
                microsite.setDescription(cursor.getString(3));
                microsite.setGpsLatitude(cursor.getFloat(4));
                microsite.setGpsLongitude(cursor.getFloat(5));
                site.setID(cursor.getLong(6));
                microsite.setSite(site);
                dataSavingType.setID(cursor.getLong(7));
                microsite.setDataSavingType(dataSavingType);
                arrayListToReturn.add(microsite);
            } while (cursor.moveToNext());
        }
        return arrayListToReturn;
    }


    public List<Site> getSites(){
        Site site = null;
        List<Site> arrayToReturn = new ArrayList<>();
        MainSession mainSession = null;
        Project project = null;
        database = this.getReadableDatabase();

        Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_SITES_WITH_SESSION_ID, new String[]{lastSessionID.toString()});
        if (cursor.moveToFirst()){
            do {
                site = new Site();
                mainSession = new MainSession();
                project = new Project();
                site.setID(cursor.getLong(0));
                site.setName(cursor.getString(1));
                mainSession.setID(cursor.getLong(2));
                site.setMainSession(mainSession);
                project.setNumber(cursor.getString(3));
                site.setProject(project);
                arrayToReturn.add(site);
            } while (cursor.moveToNext());
        }
        return arrayToReturn;
    }

    public Site getSites(Long sessionID){
        Site site = null;
        MainSession mainSession = null;
        Project project = null;
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_SITES_WITH_SESSION_ID, new String[]{sessionID.toString()});
        if (cursor.moveToFirst()){
            site = new Site();
            mainSession = new MainSession();
            project = new Project();
            do {
                site.setID(cursor.getLong(0));
                site.setName(cursor.getString(1));
                mainSession.setID(cursor.getLong(2));
                site.setMainSession(mainSession);
                project.setNumber(cursor.getString(3));
                site.setProject(project);
            } while (cursor.moveToNext());
        }
        return site;
    }

    //get (specific)
    public Microsite getMicrosite(Long id){
        Microsite microsite = null;
        Site site = null;
        DataSavingType dataSavingType = null;
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_MICROSITE, new String[]{ id.toString()});
        if (cursor.moveToFirst()){
            microsite = new Microsite();
            site = new Site();
            dataSavingType = new DataSavingType();
            do {
                microsite.setID(cursor.getLong(0));
                microsite.setPrefix(cursor.getString(1));
                microsite.setNumber(cursor.getInt(2));
                microsite.setDescription(cursor.getString(3));
                microsite.setGpsLatitude(cursor.getFloat(4));
                microsite.setGpsLongitude(cursor.getFloat(5));
                site.setID(cursor.getLong(6));
                microsite.setSite(site);
                dataSavingType.setID(cursor.getLong(7));
                microsite.setDataSavingType(dataSavingType);
            } while (cursor.moveToNext());
        }
        return microsite;
    }

    public MainLightData getMainLightData(Long id){
        MainLightData mainLightData = null;
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_MAIN_LIGHT_DATA, new String[]{ id.toString() });
        if (cursor.moveToFirst()){
            mainLightData = new MainLightData();
            do {
                mainLightData.setID(cursor.getLong(0));
                mainLightData.setSensorStatus(cursor.getInt(1));
                mainLightData.setAllValues(cursor.getFloat(2));
                mainLightData.setGoodValues(cursor.getFloat(3));
                mainLightData.setAllStandardizedValues(cursor.getFloat(4));
                mainLightData.setGoodStandardizedValues(cursor.getFloat(5));
                mainLightData.setNumberGoodObservations(cursor.getInt(6));
                mainLightData.setNumberObservations(cursor.getInt(7));
                mainLightData.setStandardDeviationGood(cursor.getFloat(8));
                mainLightData.setStandardDeviationAll(cursor.getFloat(9));
                mainLightData.setRotationCellX(cursor.getFloat(10));
                mainLightData.setRotationCellY(cursor.getFloat(11));
            } while (cursor.moveToNext());
        }
        return mainLightData;
    }

    public MainPressureData getMainPressureData(Long id){
        MainPressureData mainPressureData = null;
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_MAIN_PRESSURE_DATA, new String[]{ id.toString() });
        if (cursor.moveToFirst()){
            mainPressureData = new MainPressureData();
            do {
                mainPressureData.setID(cursor.getLong(0));
                mainPressureData.setSensorStatus(cursor.getInt(1));
                mainPressureData.setAllValues(cursor.getFloat(2));
                mainPressureData.setGoodValues(cursor.getFloat(3));
                mainPressureData.setNumberGoodObservations(cursor.getInt(4));
                mainPressureData.setNumberObservations(cursor.getInt(5));
                mainPressureData.setStandardDeviationGood(cursor.getFloat(6));
                mainPressureData.setStandardDeviationAll(cursor.getFloat(7));
            } while (cursor.moveToNext());
        }
        return mainPressureData;
    }

    public Observer getObserver(Long id){
        Observer observer = null;
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_OBSERVER, new String[]{ id.toString() });
        if (cursor.moveToFirst()){
            observer = new Observer();
            do {
                observer.setID(cursor.getLong(0));
                observer.setFirstName(cursor.getString(1));
                observer.setLastName(cursor.getString(2));
                observer.setEmployeeCode(cursor.getString(3));
            } while (cursor.moveToNext());
        }
        return observer;
    }

    //Get (last)

    private Long getLastGraphMultiplierID(){
        Long id = 0l;
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_GRAPH_MULTIPLIERS, null);
        if (cursor.moveToFirst()){
            do {
                id = cursor.getLong(0);
            } while (cursor.moveToNext());
        }
        return id;
    }

    public List<GraphMultiplierPoint> getLastGraphPointMultiplierPoints(){
        Long graphID = getLastGraphMultiplierID();
        database = this.getReadableDatabase();
        List<GraphMultiplierPoint> listOfPoints = new ArrayList<>();
        Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_GRAPH_POINT_MULTIPLIERS + " WHERE graph_point_multiplier_id = " + graphID, null);
        if (cursor.moveToFirst()){
            do {
                GraphMultiplierPoint newPoint = new GraphMultiplierPoint(
                        cursor.getFloat(2),
                        cursor.getFloat(1)
                );
                listOfPoints.add(newPoint);
            } while (cursor.moveToNext());
        }
        return listOfPoints;
    }

    //Get next

    public MainData getNextMainData(Long id, Long micrositeID){
        MainData mainData = null;
        Microsite microsite = null;
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_NEXT_MAIN_DATA, new String[]{ id.toString(), micrositeID.toString() });
        if (cursor.moveToFirst()){
            mainData = new MainData();
            microsite = new Microsite();
            do {
                mainData.setID(cursor.getLong(0));
                mainData.setTimePassed(cursor.getLong(1));
                mainData.setGpsLatitude(cursor.getFloat(2));
                mainData.setGpsLongitude(cursor.getFloat(3));
                microsite.setID(cursor.getLong(4));
                mainData.setMicrosite(microsite);
            } while (cursor.moveToNext());
        }
        return mainData;
    }

    public MainSession getNextMainSession(Long id){
        MainSession mainSession = null;
        Project project = null;
        Observer observer = null;
        Cellphone cellphone = null;
        ReferenceSession referenceSession = null;
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_NEXT_MAIN_SESSION, new String[]{ id.toString() });
        if (cursor.moveToFirst()){
            mainSession = new MainSession();
            project = new Project();
            observer = new Observer();
            cellphone = new Cellphone();
            referenceSession = new ReferenceSession();
            do {
                mainSession.setID(cursor.getLong(0));
                project.setNumber(cursor.getString(1));
                mainSession.setProject(project);
                observer.setID(cursor.getLong(2));
                mainSession.setObserver(observer);
                cellphone.setID(cursor.getString(3));
                mainSession.setCellphone(cellphone);
                referenceSession.setID(cursor.getLong(4));
                mainSession.setReferenceSession(referenceSession);
            } while (cursor.moveToNext());
        }
        return mainSession;
    }

    public Microsite getNextMicrosite(Long id, Long siteID){
        Microsite microsite = null;
        Site site = null;
        DataSavingType dataSavingType = null;
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_NEXT_MICROSITE, new String[]{ id.toString(), siteID.toString() });
        if (cursor.moveToFirst()){
            microsite = new Microsite();
            site = new Site();
            dataSavingType = new DataSavingType();
            do {
                microsite.setID(cursor.getLong(0));
                microsite.setPrefix(cursor.getString(1));
                microsite.setNumber(cursor.getInt(2));
                microsite.setDescription(cursor.getString(3));
                microsite.setGpsLatitude(cursor.getFloat(4));
                microsite.setGpsLongitude(cursor.getFloat(5));
                site.setID(cursor.getLong(6));
                microsite.setSite(site);
                dataSavingType.setID(cursor.getLong(7));
                microsite.setDataSavingType(dataSavingType);
            } while (cursor.moveToNext());
        }
        return microsite;
    }

    public Site getNextSite(Long id, Long idMainSession){
        Site site = null;
        MainSession mainSession = null;
        Project project = null;
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_NEXT_SITE, new String[]{ id.toString(), idMainSession.toString()});
        if (cursor.moveToFirst()){
            site = new Site();
            mainSession = new MainSession();
            project = new Project();
            do {
                site.setID(cursor.getLong(0));
                site.setName(cursor.getString(1));
                mainSession.setID(cursor.getLong(2));
                site.setMainSession(mainSession);
                project.setNumber(cursor.getString(3));
                site.setProject(project);
            } while (cursor.moveToNext());
        }
        return site;
    }

    public Site getSite(Long id){
        Site site = null;
        MainSession mainSession = null;
        Project project = null;
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_SITE, new String[]{ id.toString()});
        if (cursor.moveToFirst()){
            site = new Site();
            mainSession = new MainSession();
            project = new Project();
            do {
                site.setID(cursor.getLong(0));
                site.setName(cursor.getString(1));
                mainSession.setID(cursor.getLong(2));
                site.setMainSession(mainSession);
                project.setNumber(cursor.getString(3));
                site.setProject(project);
            } while (cursor.moveToNext());
        }
        return site;
    }

    public Site getSite(String name){
        Site site = null;
        MainSession mainSession = null;
        Project project = null;
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_SITE_WITH_NAME_AND_PROJECT_NUMBER, new String[]{ name, lastProjectNumber});
        if (cursor.moveToFirst()){
            site = new Site();
            mainSession = new MainSession();
            project = new Project();
            do {
                site.setID(cursor.getLong(0));
                site.setName(cursor.getString(1));
                mainSession.setID(cursor.getLong(2));
                site.setMainSession(mainSession);
                project.setNumber(cursor.getString(3));
                site.setProject(project);
            } while (cursor.moveToNext());
        }
        return site;
    }

    public GraphMultiplier getNextGraphMultiplier(Long id){
        GraphMultiplier graphMultiplier = null;
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_NEXT_GRAPH_MULTIPLIER, new String[]{ id.toString()});
        if (cursor.moveToFirst()){
            graphMultiplier = new GraphMultiplier();

            do {
                graphMultiplier.setId(cursor.getLong(0));
                Integer year = cursor.getInt(1);
                Integer month = cursor.getInt(2);
                Integer day = cursor.getInt(3);
                graphMultiplier.setCellphone(new Cellphone( cursor.getString(4)));
                graphMultiplier.setDate(LocalDate.of(year, month, day));
            } while (cursor.moveToNext());
        }
        return graphMultiplier;
    }

    public GraphMultiplierPoint getNextGraphMultiplierPoint(Long id, Long idGraph){
        GraphMultiplierPoint graphMultiplierPoint = null;
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_NEXT_GRAPH_POINT_MULTIPLIER, new String[]{ id.toString(), idGraph.toString()});
        if (cursor.moveToFirst()){
            graphMultiplierPoint = new GraphMultiplierPoint();

            do {
                graphMultiplierPoint.setId(cursor.getLong(0));
                graphMultiplierPoint.setDataMultiplier(cursor.getFloat(1));
                graphMultiplierPoint.setDataSeen(cursor.getFloat(2));
                graphMultiplierPoint.setGraphMultiplier(new GraphMultiplier(cursor.getLong(3)));
            } while (cursor.moveToNext());
        }
        return graphMultiplierPoint;
    }

    //Exists

    public Boolean cellphoneExists(String id) {
        Boolean cellphoneExists = false;
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT id FROM " + DatabaseConstants.TABLE_CELLPHONE + " WHERE id='" + id + "'", null);
        if (cursor.moveToFirst()) {
            cellphoneExists = true;
        }
        return cellphoneExists;
    }

    public Boolean projectExists(String number) {
        Boolean projectExists = false;
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT number FROM " + DatabaseConstants.TABLE_PROJECT + " WHERE number='" + number + "'", null);
        if (cursor.moveToFirst()) {
            projectExists = true;
        }
        return projectExists;
    }

    public Boolean observerExists(String employeeCode) {
        Boolean observerExists = false;
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT employee_code FROM " + DatabaseConstants.TABLE_OBSERVER + " WHERE employee_code='" + employeeCode + "'", null);
        if (cursor.moveToFirst()) {
            observerExists = true;
        }
        return observerExists;
    }

    //Add (merge)

    public Long addSite(Site site) {
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", site.getID());
        contentValues.put("name", site.getName());
        contentValues.put("project_number", lastProjectNumber);
        contentValues.put("main_session_id", lastSessionID);
        return database.insert(DatabaseConstants.TABLE_SITE, null, contentValues);
    }

    public Long addMicrosite(Microsite microsite){
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", microsite.getID());
        contentValues.put("prefix", microsite.getPrefix());
        contentValues.put("number", microsite.getNumber());
        contentValues.put("description", microsite.getDescription());
        if (microsite.getGpsLatitude() != null && microsite.getGpsLatitude() != 0) {
            contentValues.put("gps_latitude", microsite.getGpsLatitude());
        }
        if (microsite.getGpsLongitude() != null && microsite.getGpsLongitude() != 0) {
            contentValues.put("gps_longitude", microsite.getGpsLongitude());
        }
        contentValues.put("site_id", microsite.getSite().getID());
        contentValues.put("data_saving_type_id", microsite.getDataSavingType().getID());
        return database.insert(DatabaseConstants.TABLE_MICROSITE, null, contentValues);
    }

    public Long addMainData(MainData mainData){
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", mainData.getID());
        contentValues.put("microsite_id", mainData.getMicrosite().getID());
        if (mainData.getGpsLatitude() != null && mainData.getGpsLatitude() != 0) {
            contentValues.put("gps_latitude", mainData.getGpsLatitude());
        }
        if (mainData.getGpsLongitude() != null && mainData.getGpsLongitude() != 0) {
            contentValues.put("gps_longitude", mainData.getGpsLongitude());
        }
        contentValues.put("time_passed", mainData.getTimePassed());

        return database.insert(DatabaseConstants.TABLE_MAIN_DATA, null, contentValues);
    }

    public Long addMainLightData(MainLightData mainLightData){
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", mainLightData.getID());
        contentValues.put("sensor_status", mainLightData.getSensorStatus());

        contentValues.put("all_values", mainLightData.getAllValues());
        contentValues.put("good_values", mainLightData.getGoodValues());

        contentValues.put("all_standardized_values", mainLightData.getAllStandardizedValues());
        contentValues.put("good_standardized_values", mainLightData.getGoodStandardizedValues());

        contentValues.put("number_good_observations", mainLightData.getNumberGoodObservations());
        contentValues.put("number_observations", mainLightData.getNumberObservations());

        contentValues.put("standard_deviation_good", mainLightData.getStandardDeviationGood());
        contentValues.put("standard_deviation_all", mainLightData.getStandardDeviationAll());

        contentValues.put("rotation_cell_x", mainLightData.getRotationCellX());
        contentValues.put("rotation_cell_y", mainLightData.getRotationCellY());

        return database.insert(DatabaseConstants.TABLE_MAIN_LIGHT_DATA, null, contentValues);
    }

    public Long addMainPressureData(MainPressureData mainPressureData) {
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", mainPressureData.getID());
        contentValues.put("sensor_status", mainPressureData.getSensorStatus());

        contentValues.put("all_values", mainPressureData.getAllValues());
        contentValues.put("good_values", mainPressureData.getGoodValues());

        contentValues.put("number_good_observations", mainPressureData.getNumberGoodObservations());
        contentValues.put("number_observations", mainPressureData.getNumberObservations());

        contentValues.put("standard_deviation_good", mainPressureData.getStandardDeviationGood());
        contentValues.put("standard_deviation_all", mainPressureData.getStandardDeviationAll());

        return database.insert(DatabaseConstants.TABLE_MAIN_PRESSURE_DATA, null, contentValues);
    }

    public void addObserver(Observer observer){
        if (!observerExists(observer.getEmployeeCode())) {
            database = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("id", observer.getID());
            contentValues.put("first_name", observer.getFirstName());
            contentValues.put("last_name", observer.getLastName());
            contentValues.put("employee_code", observer.getEmployeeCode());
            database.insert(DatabaseConstants.TABLE_OBSERVER, null, contentValues);
        }
    }

    public void addProject(Project project){
        if (!projectExists(project.getNumber())) {
            database = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("number", project.getNumber());
            contentValues.put("name", project.getName());
            database.insert(DatabaseConstants.TABLE_PROJECT, null, contentValues);
        }
    }

    public void addCellphone(Cellphone cellphone){
        if (!cellphoneExists(cellphone.getID())) {
            database = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("id", cellphone.getID());
            database.insert(DatabaseConstants.TABLE_CELLPHONE, null, contentValues);
        }
    }

    public Long addMainSession(MainSession mainSession){
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        if (mainSession.getID() != null) {
            if (mainSession.getID() != 0) {
                contentValues.put("id", mainSession.getID());
            }
        }
        contentValues.put("project_number", mainSession.getProject().getNumber());
        contentValues.put("observer_id", mainSession.getObserver().getID());
        contentValues.put("cellphone_id", mainSession.getCellphone().getID());
        contentValues.put("reference_session_id", mainSession.getReferenceSession().getID());
        lastProjectNumber = mainSession.getProject().getNumber();
        lastSessionID = database.insert(DatabaseConstants.TABLE_MAIN_SESSION, null, contentValues);
        return lastSessionID;
    }

    public Long addReferenceSession(ReferenceSession referenceSession){
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        if (referenceSession.getID() != null) {
            contentValues.put("id", referenceSession.getID());
        }
        contentValues.put("cellphone_id", referenceSession.getCellphone().getID());
        contentValues.put("year", referenceSession.getDate().getYear());
        contentValues.put("month", referenceSession.getDate().getMonth().getValue());
        contentValues.put("day", referenceSession.getDate().getDayOfMonth());

        lastSessionID = database.insert(DatabaseConstants.TABLE_REFERENCE_SESSION, null, contentValues);
        return lastSessionID;
    }

    //Update

    public ArrayList<Long> getMainDataIDWithMicrositeID(Long idMicrosite){
        ArrayList<Long> listOfMainDataID = new ArrayList<>();
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_DATA_ID_WITH_MICROSITE,  new String[]{ idMicrosite.toString() });
        if (cursor.moveToFirst()){
            do {
                listOfMainDataID.add(cursor.getLong(0));
            } while (cursor.moveToNext());
        }
        return listOfMainDataID;
    }

    private Boolean isSiteUsedMoreThanOnce(Long idSite){
        Boolean isSiteUsedMoreThanOnce = false;
        Site siteSearchingIn = getSite(idSite);
        database = this.getReadableDatabase();
        Integer counter = 0;
        Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_MICROSITES_WITH_SITE, new String[]{ siteSearchingIn.getID().toString()});
        if (cursor.moveToFirst()){
            do {
                counter++;
                if (counter>1){
                    isSiteUsedMoreThanOnce = true;
                }
            } while (cursor.moveToNext() && !isSiteUsedMoreThanOnce);
        }
        return isSiteUsedMoreThanOnce;
    }

    private Boolean destroySite(Long idSite){
        return database.delete(DatabaseConstants.TABLE_SITE, "id = ?", new String[]{String.valueOf(idSite)}) > 0;
    }

    private Boolean destroyMicrosite(Long micrositeID){
        return database.delete(DatabaseConstants.TABLE_MICROSITE, "id = ?", new String[]{String.valueOf(micrositeID)}) > 0;
    }

    private Integer destroyAllData(Long micrositeID){
        ArrayList<Long> allID = getMainDataIDWithMicrositeID(micrositeID);
        for ( Long idData : allID) {
            database.delete(DatabaseConstants.TABLE_MAIN_LIGHT_DATA, "id = ?", new String[]{String.valueOf(idData)});
            database.delete(DatabaseConstants.TABLE_MAIN_PRESSURE_DATA, "id = ?", new String[]{String.valueOf(idData)});
        }
        return database.delete(DatabaseConstants.TABLE_MAIN_DATA, "microsite_id = ?", new String[]{String.valueOf(micrositeID)});
    }



    public Boolean destroySiteIfNeeded(Long siteID){
        Boolean wasSiteDestroyed = false;
        if (!isSiteUsedMoreThanOnce(siteID)){
            wasSiteDestroyed = destroySite(siteID);
        }
        return wasSiteDestroyed;
    }

    public void destroyMicrositeAndDataAndSiteIfNeeded(Long micrositeID){
        Microsite microsite = getMicrosite(micrositeID);

        destroyAllData(micrositeID);
        destroySiteIfNeeded(microsite.getSite().getID());
        destroyMicrosite(micrositeID);
    }

    public void destroySiteAndItsData(Long siteID){
        List<Microsite> allMicrositeLinkedToSite = getMicrositesFromSite(siteID);
        for (Microsite microsite: allMicrositeLinkedToSite) {
            destroyAllData(microsite.getID());
            destroyMicrosite(microsite.getID());
        }
        destroySite(siteID);
    }

    public Double getAverageLightDataFromMicrosite(Long micrositeID){
        ArrayList<Long> allID = getMainDataIDWithMicrositeID(micrositeID);
        Double mainLightTotal = 0d;
        for (Long idData : allID) {
            MainLightData mainLightData = getMainLightData(idData);
            if (mainLightData.getGoodStandardizedValues() == null || mainLightData.getGoodStandardizedValues() <= 0){
                mainLightTotal += mainLightData.getGoodValues();
            } else {
                mainLightTotal += mainLightData.getGoodStandardizedValues();
            }
        }
        return mainLightTotal / allID.size();
    }

    public Double getPercentageGoodObservation(Long micrositeID){
        ArrayList<Long> allID = getMainDataIDWithMicrositeID(micrositeID);
        Double numberOfGoodObservations = 0d;
        Double numberOfObservations = 0d;
        for (Long idData : allID) {
            MainLightData mainLightData = getMainLightData(idData);
            numberOfGoodObservations += mainLightData.getNumberGoodObservations();
            numberOfObservations += mainLightData.getNumberObservations();
        }
        return numberOfGoodObservations / numberOfObservations;
    }


    //Delete

    private void deleteSite() {
        database.delete(DatabaseConstants.TABLE_SITE, null, null);
    }

    private void deleteMicrosite() {
        database.delete(DatabaseConstants.TABLE_MICROSITE, null, null);
    }

    private void deleteMainLightData() {
        database.delete(DatabaseConstants.TABLE_MAIN_LIGHT_DATA, null, null);
    }

    private void deleteMainPressureData() {
        database.delete(DatabaseConstants.TABLE_MAIN_PRESSURE_DATA, null, null);
    }

    private void deleteMainData() {
        database.delete(DatabaseConstants.TABLE_MAIN_DATA, null, null);
    }

    private void deleteMainSession() {
        database.delete(DatabaseConstants.TABLE_MAIN_SESSION, null, null);
    }

    private void deleteReferenceLightData() {
        database.delete(DatabaseConstants.TABLE_REFERENCE_LIGHT_DATA, null, null);
    }

    private void deleteReferenceData() {
        database.delete(DatabaseConstants.TABLE_REFERENCE_DATA, null, null);
    }

    private void deleteReferencePosition() {
        database.delete(DatabaseConstants.TABLE_REFERENCE_POSITION, null, null);
    }

    private void deleteReferenceSession() {
        database.delete(DatabaseConstants.TABLE_REFERENCE_SESSION, null, null);
    }
}