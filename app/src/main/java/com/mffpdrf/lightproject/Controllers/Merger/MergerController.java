package com.mffpdrf.lightproject.Controllers.Merger;

import android.content.Context;
import android.util.Log;

import com.mffpdrf.lightproject.Constants.BluetoothConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.Controllers.DatabaseController;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;

public abstract class MergerController {
    protected Context context;
    protected JSONObject lastJSONObject;

    public MergerController(Context context) {
        this.context = context;
    }

    /*public void generateFile(JSONObject jsonObject) {
        try {
            if (jsonObject != null) {
                File filepath = new File(context.getFilesDir() , BluetoothConstants.LAST_OBJECT_MERGED_JSON_FILE_NAME);
                FileWriter fileWriter = new FileWriter(filepath);

                fileWriter.append(jsonObject.toString());
                fileWriter.flush();
                fileWriter.close();
            }
        } catch (Exception e) {
            Log.e("File Creation Error", e.getMessage());
        }
    }

    public JSONObject readFile() {
        String data = null;
        try {
            File file = new File(context.getFilesDir(), BluetoothConstants.LAST_OBJECT_MERGED_JSON_FILE_NAME);
            if (file.exists()) {
                Scanner scanner = new Scanner(file);
                while (scanner.hasNext()) {
                    data = scanner.nextLine();
                }
                scanner.close();
            }
        } catch (FileNotFoundException exception) {
            Log.e(TagConstants.FILE_NOT_FOUND, "readFile: ", exception);
        }
        JSONObject jsonFile = null;
        try {
            if (data != null) {
                jsonFile = new JSONObject(data);
            }
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "readFile: ", exception);
        }
        return jsonFile;
    }

    public void destroyFile() {
        try {
            File file = new File(context.getFilesDir(), BluetoothConstants.LAST_OBJECT_MERGED_JSON_FILE_NAME);
            file.delete();
        } catch (Exception exception) {

        }
    }*/
}