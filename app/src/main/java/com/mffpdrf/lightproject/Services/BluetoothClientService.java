package com.mffpdrf.lightproject.Services;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.mffpdrf.lightproject.Constants.BluetoothConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.Controllers.Merger.MergerSenderController;
import com.mffpdrf.lightproject.Events.OnMergeEventListener;
import com.mffpdrf.lightproject.Models.DatabaseModels.ReferenceSession;

import java.io.IOException;
import java.util.UUID;

public class BluetoothClientService implements BluetoothConnectionService.OnMainEventListener {
    static private BluetoothClientService instance;
    Context context;
    public ConnectThread connectThread;

    BluetoothAdapter bluetoothAdapter;
    BluetoothDevice bluetoothDevice;
    private UUID deviceUUID;

    BluetoothConnectionService bluetoothConnectionService = null;
    MergerSenderController mergerSenderController;
    private OnClientDeviceConnectedListener listener;

    private BluetoothClientService(Context context, BluetoothDevice device) {
        this.context = context;
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        connectThread = new ConnectThread(device);
        try {
            connectThread.start();
        } catch (Exception e) {
            Log.e("TAG", "BluetoothClientService: ", e);
        }
    }

    static public BluetoothClientService getInstance(Context context, @Nullable BluetoothDevice device){
        if (instance == null){
            try {
                instance = new BluetoothClientService(context, device);
            } catch (Exception exception) {
                Log.e("TAG", "getInstance: ", exception);
            }
        }
        return instance;
    }

    public void destroyInstance(){
        if (bluetoothConnectionService != null) {
            bluetoothConnectionService.closeSocket();
            bluetoothConnectionService = null;
        }
        connectThread.cancel();
        instance = null;
    }

    public void setOnClientConnectedListener(OnClientDeviceConnectedListener setOnListener){
        listener = setOnListener;
    }

    public void createMergerSenderController(OnMergeEventListener setOnListener){
        mergerSenderController = new MergerSenderController(bluetoothConnectionService, context, setOnListener);
    }

    public void write(byte[] bytes) {
        bluetoothConnectionService.write(bytes);
    }

    @Override
    public void onSessionCreation(ReferenceSession referenceSession, Long timeToWait) {
        listener.onSessionCreation(referenceSession, timeToWait);
    }

    public class ConnectThread extends Thread {
        private BluetoothSocket bluetoothSocket;

        public ConnectThread(BluetoothDevice device) {
            bluetoothDevice = device;
            deviceUUID = BluetoothConstants.SECURE_UUID;

            BluetoothSocket temporarySocket = null;

            try {
                temporarySocket = bluetoothDevice.createRfcommSocketToServiceRecord(deviceUUID);
            } catch (IOException exception) {
                Toast.makeText(context, exception.getMessage(), Toast.LENGTH_LONG);
                Log.e(TagConstants.BLUETOOTH_ERROR, "ConnectThread: ", exception);
            }

            bluetoothSocket = temporarySocket;
        }

        public void run() {
            bluetoothAdapter.cancelDiscovery();

            try {
                bluetoothSocket.connect();
            } catch (IOException exception) {
                try {
                    bluetoothSocket.close();
                } catch (IOException closeException) {
                    Toast.makeText(context, exception.getMessage(), Toast.LENGTH_LONG);
                    Log.e(TagConstants.BLUETOOTH_ERROR, "run: ", exception);
                }
            }

            bluetoothConnectionService = new BluetoothConnectionService(context, bluetoothSocket);
            bluetoothConnectionService.setOnMainDeviceConnectedListener(BluetoothClientService.this);
            listener.onClientSideConnection();
        }

        public void cancel() {
            try {
                bluetoothSocket.close();
            } catch (IOException exception) {
                Toast.makeText(context, exception.getMessage(), Toast.LENGTH_LONG);
                Log.e(TagConstants.BLUETOOTH_ERROR, "cancel: ", exception);
            }
        }
    }

    public interface OnClientDeviceConnectedListener {
        void onSessionCreation(ReferenceSession referenceSession, Long timeToWait);
        void onClientSideConnection();
    }
}