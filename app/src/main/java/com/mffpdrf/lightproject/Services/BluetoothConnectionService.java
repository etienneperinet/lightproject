package com.mffpdrf.lightproject.Services;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import com.mffpdrf.lightproject.Constants.BluetoothConstants;
import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.Models.DatabaseModels.ReferenceSession;

import org.json.JSONException;
import org.json.JSONObject;


public class BluetoothConnectionService {
    private static final String TAG = "BluetoothConnectionService";
    private static final Integer NUMBER_OF_PINGS = 15;

    private Context context;

    public ConnectedThread connectedThread;
    private BluetoothSocket bluetoothSocket = null;

    private OnReferenceSessionCreatedListener referenceListener;
    private OnMainEventListener mainListener;
    private OnMergeObjectReceiveObjectListener mergeReceiverListener;
    private OnMergeObjectConfirmationReceiveListener mergeSenderListener;

    private List<Long> pingResponseTime = new ArrayList<>();
    private Long lastPingTime;

    public BluetoothConnectionService(Context context, BluetoothSocket bluetoothSocket) {
        this.context = context;
        this.bluetoothSocket = bluetoothSocket;
        connectedThread = new ConnectedThread(this.bluetoothSocket);
        connectedThread.start();
    }

    public void write(byte[] output) {
        connectedThread.write(output);
    }

    public void closeSocket() {
        if (connectedThread != null) {
            connectedThread.cancel();
        }
        if (bluetoothSocket != null) {
            try {
                bluetoothSocket.close();
            } catch (IOException exception) {
                Log.e(TagConstants.IO_ERROR, "closeSocket: ", exception);
            }
        }
    }

    public void setOnSessionCreatedListener(OnReferenceSessionCreatedListener setOnListener){
        referenceListener = setOnListener;
    }

    public void setOnMainDeviceConnectedListener(OnMainEventListener setOnListener){
        mainListener = setOnListener;
    }

    public void setOnMergeObjectReceiveObjectListener(OnMergeObjectReceiveObjectListener setOnListener){
        mergeReceiverListener = setOnListener;
    }

    public void setOnMergeObjectConfirmationReceivedListener(OnMergeObjectConfirmationReceiveListener setOnListener){
        mergeSenderListener = setOnListener;
    }


    private class ConnectedThread extends Thread {
        private ReferenceSession lastReferenceSessionCreated;
        private final BluetoothSocket bluetoothSocket;
        private final InputStream inputStream;
        private final OutputStream outputStream;
        private byte[] buffer;

        public ConnectedThread(BluetoothSocket socket) {
            bluetoothSocket = socket;

            InputStream temporaryInput = null;
            OutputStream temporaryOutput = null;

            try {
                temporaryInput = socket.getInputStream();
            } catch (IOException exception) {
                Log.e(TagConstants.BLUETOOTH_ERROR, "ConnectedThread: " + exception.getMessage());
            }

            try {
                temporaryOutput = socket.getOutputStream();
            } catch (IOException exception) {
                Log.e(TagConstants.BLUETOOTH_ERROR, "ConnectedThread: " + exception.getMessage());
            }

            inputStream = temporaryInput;
            outputStream = temporaryOutput;
        }

        @SuppressLint("LongLogTag")
        public void run() {
            buffer = new byte[1024];

            while(true) {
                read(buffer);
            }
        }

        public void read(byte[] buffer) {
            if (bluetoothSocket.isConnected()) {
                try {
                    int incomingBytes = inputStream.read(buffer);
                    String incomingString = new String(buffer, 0, incomingBytes);
                    try {
                        JSONObject incomingJSON = new JSONObject(incomingString);
                        checkJSONType(incomingJSON);
                    } catch (JSONException exception) {
                        Log.e(TagConstants.JSON_ERROR, "read: " + exception.getMessage());
                    }
                } catch (IOException exception) {
                    Log.e(TagConstants.BLUETOOTH_ERROR, "read: " + exception.getMessage());
                }
            }
        }

        public void checkJSONType (JSONObject jsonObject) {
            try {
                //if (jsonObject != null) {
                    switch (jsonObject.getString("JSONType")){
                        case DatabaseConstants.TABLE_REFERENCE_SESSION:
                            findCurrentPingAndCreateSession(jsonObject);
                            break;
                        case DatabaseConstants.TABLE_LAST_OBJECT_SENT:
                            mergeSenderListener.onConfirmation(jsonObject);
                            break;
                        case BluetoothConstants.SESSION_CREATED:
                            referenceListener.onSessionCreated(jsonObject.getLong("timeToWait"));
                            break;
                        case BluetoothConstants.PING:
                            receivePing();
                            break;
                        case BluetoothConstants.PONG:
                            receivePong();
                            break;
                        default:
                            mergeReceiverListener.onReception(jsonObject);
                    }
                //}
            } catch (JSONException exception) {
                Log.e(TagConstants.JSON_ERROR, "read: " + exception.getMessage());
            }
        }

        @SuppressLint("LongLogTag")
        public void write(byte[] bytes) {
            try {
                outputStream.write(bytes);
                String outgoingMessage = new String(bytes, Charset.defaultCharset());
                Log.d(TAG, "sent: " + outgoingMessage);
            } catch (IOException exception) {
                Log.e(TagConstants.BLUETOOTH_ERROR, "write: " + exception.getMessage());
            }
        }

        public void cancel() {
            try {
                bluetoothSocket.close();
            } catch (IOException exception) {
                Log.e(TagConstants.BLUETOOTH_ERROR, "cancel: " + exception.getMessage());
            }
        }


        /*
        Ancienne façon de faire la création de session
        private void callSessionCreation(JSONObject jsonObject){
            ReferenceSession referenceSession = new ReferenceSession(jsonObject);
            connectionListener.onSessionCreation(referenceSession);
        }
        */

        public JSONObject getSessionCreatedJSON(Long timeToWait) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("JSONType", BluetoothConstants.SESSION_CREATED);
                jsonObject.put("timeToWait", timeToWait);
            } catch (JSONException exception) {
                Log.e(TagConstants.JSON_ERROR, "getSessionCreatedJSON: ", exception);
            }

            return jsonObject;
        }

        private void findCurrentPingAndCreateSession(JSONObject referenceSessionJSONObject){
            lastReferenceSessionCreated = new ReferenceSession(referenceSessionJSONObject);
            sendPing();
            //La session sera initialisé après que le ping sera calculé dans calculatePing()
        }

        private void sendPing(){
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("JSONType", BluetoothConstants.PING);
            } catch (JSONException exception) {
                Log.e(TagConstants.JSON_ERROR, "sendPing : ", exception);
            }
            lastPingTime = Instant.now().toEpochMilli();
            write(jsonObject.toString().getBytes());
        }

        private void receivePing(){
            sendPong();
        }

        private void sendPong(){
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("JSONType", BluetoothConstants.PONG);
            } catch (JSONException exception) {
                Log.e(TagConstants.JSON_ERROR, "sendPing: ", exception);
            }
            write(jsonObject.toString().getBytes());
        }

        private void receivePong(){
            if (pingResponseTime.size() < NUMBER_OF_PINGS){
                Long pingCalculated = Instant.now().toEpochMilli() - lastPingTime;
                pingResponseTime.add(pingCalculated);
                Log.d(TagConstants.LAST_PING, "receivePong: " + "Your last ping is " + pingCalculated);
                sendPing();
            } else {
                calculatePing();
            }
        }

        private void calculatePing(){
            Long totalPing = 0l;
            for (Long currentPing : pingResponseTime){
                totalPing += currentPing;
            }
            Long average = totalPing / NUMBER_OF_PINGS;
            Log.d(TagConstants.AVERAGE_PING, "calculatePing: " + "Your average ping is " + average);
            //Call event
            mainListener.onSessionCreation(lastReferenceSessionCreated, average);
            //write change page
            write(getSessionCreatedJSON(0l).toString().getBytes());
        }
    }

    public interface OnMainEventListener {
        void onSessionCreation(ReferenceSession referenceSession, Long timeToWait);
    }

    public interface OnReferenceSessionCreatedListener {
        void onSessionCreated(Long timeToWait);
    }

    public interface OnMergeObjectConfirmationReceiveListener {
        void onConfirmation(JSONObject jsonObject);
    }

    public interface OnMergeObjectReceiveObjectListener {
        void onReception(JSONObject jsonObject);
    }
}