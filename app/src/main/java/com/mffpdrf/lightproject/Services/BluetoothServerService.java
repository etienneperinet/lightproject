package com.mffpdrf.lightproject.Services;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.mffpdrf.lightproject.Constants.BluetoothConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.Controllers.Merger.MergerReceiverController;
import com.mffpdrf.lightproject.Events.OnMergeEventListener;

import java.io.IOException;

import javax.security.auth.login.LoginException;

public class BluetoothServerService {
    private static BluetoothServerService instance;

    private static final String TAG = "BluetoothServerService";
    public AcceptThread acceptThread;
    Context context;
    BluetoothAdapter bluetoothAdapter;
    private BluetoothServerSocket serverSocket = null;
    BluetoothConnectionService bluetoothConnectionService = null;
    MergerReceiverController mergerReceiverController = null;

    private OnReferenceDeviceConnectedListener listenerConnection;

    private BluetoothServerService(Context context) {
        this.context = context;
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        acceptThread = new AcceptThread();
        acceptThread.start();
    }

    static public BluetoothServerService getInstance(Context context){
        if (instance == null){
            instance = new BluetoothServerService(context);
        }
        return instance;
    }

    public void destroyInstance(){
        if (bluetoothConnectionService != null) {
            bluetoothConnectionService.closeSocket();
            bluetoothConnectionService = null;
        }

        closeServerSocket();

        acceptThread.cancel();
        instance = null;
    }

    private void closeServerSocket() {
        try {
            if (serverSocket != null) {
                serverSocket.close();
            }
        } catch (IOException exception) {
            Log.e(TagConstants.IO_ERROR, "destroyInstance: ", exception);
        }
    }

    public void write(byte[] bytes) {
        bluetoothConnectionService.write(bytes);
    }

    public void setSessionCreatedListener(BluetoothConnectionService.OnReferenceSessionCreatedListener sessionCreatedListener) {
        bluetoothConnectionService.setOnSessionCreatedListener(sessionCreatedListener);
    }

    public void createMergerReceiver(OnMergeEventListener setOnListener){
        mergerReceiverController = MergerReceiverController.getInstance(bluetoothConnectionService, context, setOnListener);
    }

    public void setOnServerConnectedListener(OnReferenceDeviceConnectedListener setOnListener){
        listenerConnection = setOnListener;
    }

    private class AcceptThread extends Thread {

        public AcceptThread() {
            BluetoothServerSocket temporarySocket = null;

            try {
                temporarySocket = bluetoothAdapter.listenUsingRfcommWithServiceRecord(BluetoothConstants.APP_NAME, BluetoothConstants.SECURE_UUID);
            } catch (IOException exception) {
                Toast.makeText(context, exception.getMessage(), Toast.LENGTH_LONG);
            }

            serverSocket = temporarySocket;
        }

        public void run() {
            BluetoothSocket bluetoothSocket = null;

            while (true) {
                try {
                    if (serverSocket != null) {
                        bluetoothSocket = serverSocket.accept();
                        Log.d(TAG, "run: Server accept connection");
                    }
                } catch (IOException exception) {
                    Log.e(TagConstants.BLUETOOTH_ERROR, "run: ", exception);
                    break;
                }

                if (bluetoothSocket != null) {
                    bluetoothConnectionService = new BluetoothConnectionService(context, bluetoothSocket);
                    listenerConnection.onConnectionReference();

                    try {
                        serverSocket.close();
                    } catch (IOException exception) {
                        Toast.makeText(context, "Une erreur est survenue lors de la fermeture de la connection Bluetooth", Toast.LENGTH_LONG).show();
                        Log.e(TagConstants.BLUETOOTH_ERROR, "run: ", exception);
                    }
                    break;
                }
            }
        }

        public void cancel() {
            try {
                serverSocket.close();
            } catch (IOException exception) {
                Toast.makeText(context, exception.getMessage(), Toast.LENGTH_LONG);
                Log.e(TagConstants.BLUETOOTH_ERROR, "cancel: ", exception);
            }
        }
    }

    public interface OnReferenceDeviceConnectedListener{
        void onConnectionReference();
    }
}