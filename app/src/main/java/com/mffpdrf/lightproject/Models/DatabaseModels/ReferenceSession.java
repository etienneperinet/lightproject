package com.mffpdrf.lightproject.Models.DatabaseModels;

import android.util.Log;

import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.Models.DataFormatter;

import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalDate;

public class ReferenceSession implements DataFormatter {
    private Long id;
    private LocalDate date;
    private Cellphone cellphone;

    public ReferenceSession(Long id, LocalDate date, Cellphone cellphone) {
        this.id = id;
        this.date = date;
        this.cellphone = cellphone;
    }

    public ReferenceSession() {
        this.id = null;
        this.date = null;
        this.cellphone = null;
    }

    public ReferenceSession(JSONObject referenceSessionJSON) {
        try {
            this.id = referenceSessionJSON.getLong("id");
            this.date = LocalDate.of(referenceSessionJSON.getInt("year"), referenceSessionJSON.getInt("month"), referenceSessionJSON.getInt("day"));
            this.cellphone = new Cellphone(referenceSessionJSON.getString("cellphoneID"));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "ReferenceSession: ", exception);
        }
    }

    public Long getID() {
        return id;
    }

    public void setID(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Cellphone getCellphone() {
        return cellphone;
    }

    public void setCellphone(Cellphone cellphone) {
        this.cellphone = cellphone;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject referenceSessionJSON = new JSONObject();

        try {
            referenceSessionJSON.put("JSONType", DatabaseConstants.TABLE_REFERENCE_SESSION);
            referenceSessionJSON.put("id", id);
            referenceSessionJSON.put("year", date.getYear());
            referenceSessionJSON.put("month", date.getMonthValue());
            referenceSessionJSON.put("day", date.getDayOfMonth());
            referenceSessionJSON.put("cellphoneID", cellphone.getID());
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "ReferenceSession: ", exception);
        }

        return referenceSessionJSON;
    }
}