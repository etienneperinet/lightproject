package com.mffpdrf.lightproject.Models.DatabaseModels;

import android.util.Log;

import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.Models.DataFormatter;

import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalDate;

public class GraphMultiplier implements DataFormatter {
    private Long id;
    private LocalDate date;
    private Cellphone cellphone;

    public GraphMultiplier(Long id, LocalDate date) {
        this.id = id;
        this.date = date;
    }

    public GraphMultiplier(JSONObject graphMultiplierJSON) {
        try {
            this.id = graphMultiplierJSON.getLong("id");
            this.date = LocalDate.of(graphMultiplierJSON.getInt("year"), graphMultiplierJSON.getInt("month"), graphMultiplierJSON.getInt("day"));
            this.cellphone = new Cellphone(graphMultiplierJSON.getString("cellphoneID"));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "GraphMultiplier: ", exception);
        }
    }

    public GraphMultiplier(Long id) {
        this.id = id;
        this.date = LocalDate.now();
    }

    public GraphMultiplier() {

    }

    public Cellphone getCellphone() {
        return cellphone;
    }

    public void setCellphone(Cellphone cellphone) {
        this.cellphone = cellphone;
    }

    public Long getID() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject referenceSessionJSON = new JSONObject();

        try {
            referenceSessionJSON.put("JSONType", DatabaseConstants.TABLE_GRAPH_MULTIPLIER);
            referenceSessionJSON.put("id", id);
            referenceSessionJSON.put("year", date.getYear());
            referenceSessionJSON.put("month", date.getMonthValue());
            referenceSessionJSON.put("day", date.getDayOfMonth());
            referenceSessionJSON.put("cellphoneID", cellphone.getID());
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "GraphMultiplier: ", exception);
        }

        return referenceSessionJSON;
    }
}