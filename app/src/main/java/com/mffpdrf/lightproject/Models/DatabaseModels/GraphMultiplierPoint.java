package com.mffpdrf.lightproject.Models.DatabaseModels;

import android.util.Log;

import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.Models.DataFormatter;

import org.json.JSONException;
import org.json.JSONObject;

public class GraphMultiplierPoint implements DataFormatter {
    private Long id;
    private Float dataSeen;
    private Float dataMultiplier;
    private GraphMultiplier graphMultiplier;

    public GraphMultiplierPoint(Float dataSeen, Float dataMultiplier) {
        this.dataSeen = dataSeen;
        this.dataMultiplier = dataMultiplier;
    }

    public GraphMultiplierPoint(Long id) {
        this.id = id;
    }
    public GraphMultiplierPoint() {
        this.id = id;
    }

    public GraphMultiplierPoint(JSONObject graphMultiplierPointsJSON) {
        try {
            this.id = graphMultiplierPointsJSON.getLong("id");
            this.dataSeen = Float.parseFloat(graphMultiplierPointsJSON.getString("dataSeen"));
            this.dataMultiplier = Float.parseFloat(graphMultiplierPointsJSON.getString("dataMultiplier"));
            this.graphMultiplier = new GraphMultiplier(graphMultiplierPointsJSON.getLong("graphMultiplier"));
            } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "GraphPointMultiplier: ", exception);
        }
    }

    public Long getID() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GraphMultiplier getGraphMultiplier() {
        return graphMultiplier;
    }

    public void setGraphMultiplier(GraphMultiplier graphMultiplier) {
        this.graphMultiplier = graphMultiplier;
    }

    public void setDataSeen(Float dataSeen) {
        this.dataSeen = dataSeen;
    }

    public void setDataMultiplier(Float dataMultiplier) {
        this.dataMultiplier = dataMultiplier;
    }

    public Float getDataSeen() {
        return dataSeen;
    }

    public Float getDataMultiplier() {
        return dataMultiplier;
    }

    @Override
    public String toString() {
        return "PointOnGraphForMultiplier{" +
                "dataSeen=" + dataSeen +
                ", dataMultiplier=" + dataMultiplier +
                '}';
    }

    @Override
    public JSONObject toJSON() {
        JSONObject referenceSessionJSON = new JSONObject();

        try {
            referenceSessionJSON.put("JSONType", DatabaseConstants.TABLE_GRAPH_POINT_MULTIPLIER);
            referenceSessionJSON.put("id", id);
            referenceSessionJSON.put("dataSeen", dataSeen);
            referenceSessionJSON.put("dataMultiplier", dataMultiplier);
            referenceSessionJSON.put("graphMultiplierID", graphMultiplier.getID());
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "GraphPointMultiplier: ", exception);
        }

        return referenceSessionJSON;
    }
}