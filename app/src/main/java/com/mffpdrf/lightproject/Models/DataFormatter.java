package com.mffpdrf.lightproject.Models;

import org.json.JSONObject;

public interface DataFormatter {
    public JSONObject toJSON();
}