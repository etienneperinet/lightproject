package com.mffpdrf.lightproject.Models.DatabaseModels;

import android.util.Log;

import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.Models.DataFormatter;

import org.json.JSONException;
import org.json.JSONObject;

public class Site implements DataFormatter {
    private Long id;
    private String name;
    private Project project;
    private MainSession mainSession;

    public Site(Long id, String name, Project project, MainSession mainSession) {
        this.id = id;
        this.name = name;
        this.project = project;
        this.mainSession = mainSession;
    }

    public Site(String name, Project project, MainSession mainSession) {
        this.name = name;
        this.project = project;
        this.mainSession = mainSession;
    }

    public Site(String name) {
        this.name = name;
        this.project = null;
        this.mainSession = null;
    }

    public Site() {
        this.name = "";
        this.project = null;
        this.mainSession = null;
    }

    public Site(Long id) {
        this.id = id;
    }

    public Site(JSONObject siteJSON) {
        try {
            this.id = siteJSON.getLong("id");
            this.project = new Project();
            this.project.setNumber(siteJSON.getString("projectNumber"));
            this.name = siteJSON.getString("name");
            this.mainSession = new MainSession();
            this.mainSession.setID(siteJSON.getLong("mainSessionID"));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "Site: ", exception);
        }
    }

    public Long getID() {
        return id;
    }

    public String getName() {
        return name;
    }

    public MainSession getMainSession() {
        return mainSession;
    }

    public void setID(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMainSession(MainSession mainSession) {
        this.mainSession = mainSession;
    }

    public Project getProject() { return project; }

    public void setProject(Project project) { this.project = project; }

    @Override
    public JSONObject toJSON() {
        JSONObject siteJSON = new JSONObject();

        try {
            siteJSON.put("JSONType", DatabaseConstants.TABLE_SITE);
            siteJSON.put("id", id);
            siteJSON.put("name", name);
            siteJSON.put("projectNumber", project.getNumber());
            siteJSON.put("mainSessionID", mainSession.getID());
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "Site: ", exception);
        }

        return siteJSON;
    }
}
