package com.mffpdrf.lightproject.Models.DatabaseModels;

import android.util.Log;

import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.Models.DataFormatter;

import org.json.JSONException;
import org.json.JSONObject;

public class DataSavingType implements DataFormatter {
    private Long id;
    private String name;

    public DataSavingType(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public DataSavingType() {
        this.id = null;
        this.name = "";
    }

    public DataSavingType(JSONObject dataSavingTypeJSON) {
        try {
            this.id = dataSavingTypeJSON.getLong("id");
            this.name = dataSavingTypeJSON.getString("name");
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "DataSavingType: ", exception);
        }
    }

    public Long getID() {
        return id;
    }

    public void setID(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject dataSavingTypeJSON = new JSONObject();

        try {
            dataSavingTypeJSON.put("id", id);
            dataSavingTypeJSON.put("name", name);
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "DataSavingType: ", exception);
        }

        return dataSavingTypeJSON;
    }
}