package com.mffpdrf.lightproject.Models.DatabaseModels;

import android.util.Log;

import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.Models.DataFormatter;

import org.json.JSONException;
import org.json.JSONObject;

public class MainData implements DataFormatter {
    private Long id;
    private Long timePassed;
    private Float gpsLatitude;
    private Float gpsLongitude;
    private Microsite microsite;

    public MainData() {
    }

    public MainData(Long id, Long timePassed, Long micrositeID, Float gpsLatitude, Float gpsLongitude) {
        this.id = id;
        this.timePassed = timePassed;
        this.microsite.setID(micrositeID);
        this.gpsLatitude = gpsLatitude;
        this.gpsLongitude = gpsLongitude;
    }

    public MainData(JSONObject mainDataJSON) {
        try {
            this.id = mainDataJSON.getLong("id");
            this.timePassed = mainDataJSON.getLong("timePassed");
            this.microsite = new Microsite();
            this.microsite.setID(mainDataJSON.getLong("micrositeID"));
            this.gpsLatitude = Float.parseFloat(mainDataJSON.getString("gpsLatitude"));
            this.gpsLongitude = Float.parseFloat(mainDataJSON.getString("gpsLongitude"));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "MainData: ", exception);
        }
    }

    public MainData(Long id) {
        this.id = id;
    }

    public Long getID() {
        return id;
    }

    public void setID(Long id) {
        this.id = id;
    }

    public Long getTimePassed() {
        return timePassed;
    }

    public void setTimePassed(Long timePassed) {
        this.timePassed = timePassed;
    }

    public Float getGpsLatitude() {
        return gpsLatitude;
    }

    public void setGpsLatitude(Float gpsLatitude) {
        this.gpsLatitude = gpsLatitude;
    }

    public Float getGpsLongitude() {
        return gpsLongitude;
    }

    public void setGpsLongitude(Float gpsLongitude) {
        this.gpsLongitude = gpsLongitude;
    }

    public Microsite getMicrosite() {
        return microsite;
    }

    public void setMicrosite(Microsite microsite) {
        this.microsite = microsite;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject mainDataJSON = new JSONObject();

        try {
            mainDataJSON.put("JSONType", DatabaseConstants.TABLE_MAIN_DATA);
            mainDataJSON.put("id", id);
            mainDataJSON.put("timePassed", timePassed);
            mainDataJSON.put("gpsLatitude", gpsLatitude);
            mainDataJSON.put("gpsLongitude", gpsLongitude);
            mainDataJSON.put("micrositeID", microsite.getID());
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "MainData: ", exception);
        }

        return mainDataJSON;
    }
}
