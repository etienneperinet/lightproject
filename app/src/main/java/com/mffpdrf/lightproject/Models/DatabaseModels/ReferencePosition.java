package com.mffpdrf.lightproject.Models.DatabaseModels;

import android.util.Log;

import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.Models.DataFormatter;

import org.json.JSONException;
import org.json.JSONObject;

public class ReferencePosition implements DataFormatter {
    private Long id;
    private Float gpsLatitude;
    private Float gpsLongitude;
    private ReferenceSession referenceSession;

    public ReferencePosition(Long id, Float gpsLatitude, Float gpsLongitude, ReferenceSession referenceSession) {
        this.id = id;
        this.gpsLatitude = gpsLatitude;
        this.gpsLongitude = gpsLongitude;
        this.referenceSession = referenceSession;
    }

    public ReferencePosition(Long id, Float gpsLatitude, Float gpsLongitude) {
        this.id = id;
        this.gpsLatitude = gpsLatitude;
        this.gpsLongitude = gpsLongitude;
        this.referenceSession = null;
    }

    public ReferencePosition() {
        this.referenceSession = null;
    }

    public ReferencePosition(JSONObject referencePositionJSON) {
        try {
            this.id = referencePositionJSON.getLong("id");
            this.gpsLatitude = Float.parseFloat(referencePositionJSON.getString("gpsLatitude"));
            this.gpsLongitude = Float.parseFloat(referencePositionJSON.getString("gpsLongitude"));
            this.referenceSession.setID(referencePositionJSON.getLong("referenceDataSessionID"));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "ReferenceDataSession: ", exception);
        }
    }

    public Long getID() {
        return id;
    }

    public Float getGPSLatitude() {
        return gpsLatitude;
    }

    public Float getGPSLongitude() {
        return gpsLongitude;
    }

    public void setID(Long id) {
        this.id = id;
    }

    public void setGPSLatitude(Float gpsLatitude) {
        this.gpsLatitude = gpsLatitude;
    }

    public void setGPSLongitude(Float gpsLongitude) {
        this.gpsLongitude = gpsLongitude;
    }

    public ReferenceSession getReferenceSession() { return referenceSession; }

    public void setReferenceSession(ReferenceSession referenceSession) { this.referenceSession = referenceSession; }
    @Override
    public JSONObject toJSON() {
        JSONObject referencePositionJSON = new JSONObject();

        try {
            referencePositionJSON.put("JSONType", DatabaseConstants.TABLE_REFERENCE_POSITION);
            referencePositionJSON.put("id", id);
            referencePositionJSON.put("gpsLatitude", gpsLatitude);
            referencePositionJSON.put("gpsLongitude", gpsLongitude);
            referencePositionJSON.put("referenceDataSessionID", referenceSession.getID());
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "ReferencePosition: ", exception);
        }

        return referencePositionJSON;
    }
}