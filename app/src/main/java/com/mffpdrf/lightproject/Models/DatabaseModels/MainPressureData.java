package com.mffpdrf.lightproject.Models.DatabaseModels;

import android.util.Log;

import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.Models.DataFormatter;

import org.json.JSONException;
import org.json.JSONObject;

public class MainPressureData implements DataFormatter {
    private Long id;
    private Integer sensorStatus;
    private Float allValues;
    private Float goodValues;
    private Integer numberGoodObservations;
    private Integer numberObservations;
    private Float standardDeviationGood;
    private Float standardDeviationAll;

    public MainPressureData() { }

    public MainPressureData(JSONObject mainPressureDataJSON) {
        try {
            this.id = mainPressureDataJSON.getLong("id");
            this.sensorStatus = mainPressureDataJSON.getInt("sensorStatus");
            this.allValues = Float.parseFloat(mainPressureDataJSON.getString("allValues"));
            this.goodValues = Float.parseFloat(mainPressureDataJSON.getString("goodValues"));
            this.numberGoodObservations = mainPressureDataJSON.getInt("numberGoodObservations");
            this.numberObservations = mainPressureDataJSON.getInt("numberObservations");
            this.standardDeviationGood = Float.parseFloat(mainPressureDataJSON.getString("standardDeviationGood"));
            this.standardDeviationAll = Float.parseFloat(mainPressureDataJSON.getString("standardDeviationAll"));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "MainPressureData: ", exception);
        }
    }

    public Long getID() {
        return id;
    }

    public void setID(Long id) {
        this.id = id;
    }

    public Integer getSensorStatus() {
        return sensorStatus;
    }

    public void setSensorStatus(Integer sensorStatus) {
        this.sensorStatus = sensorStatus;
    }

    public Float getAllValues() {
        return allValues;
    }

    public void setAllValues(Float allValues) {
        this.allValues = allValues;
    }

    public Float getGoodValues() {
        return goodValues;
    }

    public void setGoodValues(Float goodValues) {
        this.goodValues = goodValues;
    }

    public Integer getNumberGoodObservations() {
        return numberGoodObservations;
    }

    public void setNumberGoodObservations(Integer numberGoodObservations) {
        this.numberGoodObservations = numberGoodObservations;
    }

    public Integer getNumberObservations() {
        return numberObservations;
    }

    public void setNumberObservations(Integer numberObservations) {
        this.numberObservations = numberObservations;
    }

    public Float getStandardDeviationGood() {
        return standardDeviationGood;
    }

    public void setStandardDeviationGood(Float standardDeviationGood) {
        this.standardDeviationGood = standardDeviationGood;
    }

    public Float getStandardDeviationAll() {
        return standardDeviationAll;
    }

    public void setStandardDeviationAll(Float standardDeviationAll) {
        this.standardDeviationAll = standardDeviationAll;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject mainPressureDataJSON = new JSONObject();

        try {
            mainPressureDataJSON.put("JSONType", DatabaseConstants.TABLE_MAIN_PRESSURE_DATA);
            mainPressureDataJSON.put("id", id);
            mainPressureDataJSON.put("sensorStatus", sensorStatus);
            mainPressureDataJSON.put("allValues", allValues);
            mainPressureDataJSON.put("goodValues", goodValues);
            mainPressureDataJSON.put("numberGoodObservations", numberGoodObservations);
            mainPressureDataJSON.put("numberObservations", numberObservations);
            mainPressureDataJSON.put("standardDeviationGood", standardDeviationGood);
            mainPressureDataJSON.put("standardDeviationAll", standardDeviationAll);
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "MainPressureData: ", exception);
        }

        return mainPressureDataJSON;
    }
}