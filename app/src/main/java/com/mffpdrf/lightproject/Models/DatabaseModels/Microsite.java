package com.mffpdrf.lightproject.Models.DatabaseModels;

import android.util.Log;

import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.Models.DataFormatter;

import org.json.JSONException;
import org.json.JSONObject;

public class Microsite implements DataFormatter {
    private Long id;
    private String prefix;
    private Integer number;
    private String description;
    private Float gpsLatitude;
    private Float gpsLongitude;
    private Site site;
    private DataSavingType dataSavingType;

    public Microsite() {
    }

    public Microsite(Long id) {
        this.id = id;
    }

    public Microsite(JSONObject micrositeJSON) {
        try {
            this.id = micrositeJSON.getLong("id");
            this.prefix = micrositeJSON.getString("prefix");
            this.number = micrositeJSON.getInt("number");
            this.description = micrositeJSON.getString("description");
            this.gpsLatitude = Float.parseFloat(micrositeJSON.getString("gpsLatitude"));
            this.gpsLongitude = Float.parseFloat(micrositeJSON.getString("gpsLongitude"));
            this.site = new Site();
            this.site.setID(micrositeJSON.getLong("siteID"));
            this.dataSavingType = new DataSavingType();
            this.dataSavingType.setID(micrositeJSON.getLong("dataSavingTypeID"));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "Microsite: ", exception);
        }
    }

    public Long getID() {
        return id;
    }

    public void setID(Long id) {
        this.id = id;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getGpsLatitude() {
        return gpsLatitude;
    }

    public void setGpsLatitude(Float gpsLatitude) {
        this.gpsLatitude = gpsLatitude;
    }

    public Float getGpsLongitude() {
        return gpsLongitude;
    }

    public void setGpsLongitude(Float gpsLongitude) {
        this.gpsLongitude = gpsLongitude;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public DataSavingType getDataSavingType() {
        return dataSavingType;
    }

    public void setDataSavingType(DataSavingType dataSavingType) {
        this.dataSavingType = dataSavingType;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject micrositeJSON = new JSONObject();

        try {
            micrositeJSON.put("JSONType", DatabaseConstants.TABLE_MICROSITE);
            micrositeJSON.put("id", id);
            micrositeJSON.put("prefix", prefix);
            micrositeJSON.put("number", number);
            micrositeJSON.put("description", description);
            micrositeJSON.put("gpsLatitude", gpsLatitude);
            micrositeJSON.put("gpsLongitude", gpsLongitude);
            micrositeJSON.put("siteID", site.getID());
            micrositeJSON.put("dataSavingTypeID", dataSavingType.getID());
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "Microsite: ", exception);
        }

        return micrositeJSON;
    }
}