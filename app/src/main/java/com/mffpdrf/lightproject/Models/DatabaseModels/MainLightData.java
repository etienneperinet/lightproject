package com.mffpdrf.lightproject.Models.DatabaseModels;

import android.util.Log;

import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.Models.DataFormatter;

import org.json.JSONException;
import org.json.JSONObject;

public class MainLightData implements DataFormatter {
    private Long id;
    private Integer sensorStatus;
    private Float allValues;
    private Float goodValues;
    private Float allStandardizedValues;
    private Float goodStandardizedValues;
    private Integer numberGoodObservations;
    private Integer numberObservations;
    private Float standardDeviationGood;
    private Float standardDeviationAll;
    private Float rotationCellX;
    private Float rotationCellY;

    public MainLightData() { }

    public MainLightData(JSONObject mainLightDataJSON) {
        try {
            this.id = mainLightDataJSON.getLong("id");
            this.sensorStatus = mainLightDataJSON.getInt("sensorStatus");
            this.allValues = Float.parseFloat(mainLightDataJSON.getString("allValues"));
            this.goodValues = Float.parseFloat(mainLightDataJSON.getString("goodValues"));
            this.allStandardizedValues = Float.parseFloat(mainLightDataJSON.getString("allStandardizedValues"));
            this.goodStandardizedValues = Float.parseFloat(mainLightDataJSON.getString("goodStandardizedValues"));
            this.numberGoodObservations = mainLightDataJSON.getInt("numberGoodObservations");
            this.numberObservations = mainLightDataJSON.getInt("numberObservations");
            this.standardDeviationGood = Float.parseFloat(mainLightDataJSON.getString("standardDeviationGood"));
            this.standardDeviationAll = Float.parseFloat(mainLightDataJSON.getString("standardDeviationAll"));
            this.rotationCellX = Float.parseFloat(mainLightDataJSON.getString("rotationCellX"));
            this.rotationCellY = Float.parseFloat(mainLightDataJSON.getString("rotationCellY"));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "MainLightData: ", exception);
        }
    }

    public Long getID() {
        return id;
    }

    public void setID(Long id) {
        this.id = id;
    }

    public Integer getSensorStatus() {
        return sensorStatus;
    }

    public void setSensorStatus(Integer sensorStatus) {
        this.sensorStatus = sensorStatus;
    }

    public Float getAllValues() {
        return allValues;
    }

    public void setAllValues(Float allValues) {
        this.allValues = allValues;
    }

    public Float getGoodValues() {
        return goodValues;
    }

    public void setGoodValues(Float goodValues) {
        this.goodValues = goodValues;
    }

    public Integer getNumberGoodObservations() {
        return numberGoodObservations;
    }

    public void setNumberGoodObservations(Integer numberGoodObservations) {
        this.numberGoodObservations = numberGoodObservations;
    }

    public Integer getNumberObservations() {
        return numberObservations;
    }

    public void setNumberObservations(Integer numberObservations) {
        this.numberObservations = numberObservations;
    }

    public Float getStandardDeviationGood() {
        return standardDeviationGood;
    }

    public void setStandardDeviationGood(Float standardDeviationGood) {
        this.standardDeviationGood = standardDeviationGood;
    }

    public Float getStandardDeviationAll() {
        return standardDeviationAll;
    }

    public void setStandardDeviationAll(Float standardDeviationAll) {
        this.standardDeviationAll = standardDeviationAll;
    }

    public Float getRotationCellX() {
        return rotationCellX;
    }

    public void setRotationCellX(Float rotationCellX) {
        this.rotationCellX = rotationCellX;
    }

    public Float getRotationCellY() {
        return rotationCellY;
    }

    public void setRotationCellY(Float rotationCellY) {
        this.rotationCellY = rotationCellY;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject mainLightDataJSON = new JSONObject();

        try {
            mainLightDataJSON.put("JSONType", DatabaseConstants.TABLE_MAIN_LIGHT_DATA);
            mainLightDataJSON.put("id", id);
            mainLightDataJSON.put("sensorStatus", sensorStatus);
            mainLightDataJSON.put("allValues", allValues);
            mainLightDataJSON.put("goodValues", goodValues);
            mainLightDataJSON.put("allStandardizedValues", allStandardizedValues);
            mainLightDataJSON.put("goodStandardizedValues", goodStandardizedValues);
            mainLightDataJSON.put("numberGoodObservations", numberGoodObservations);
            mainLightDataJSON.put("numberObservations", numberObservations);
            mainLightDataJSON.put("standardDeviationGood", standardDeviationGood);
            mainLightDataJSON.put("standardDeviationAll", standardDeviationAll);
            mainLightDataJSON.put("rotationCellX", rotationCellX);
            mainLightDataJSON.put("rotationCellY", rotationCellY);
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "MainLightData: ", exception);
        }

        return mainLightDataJSON;
    }

    public Float getAllStandardizedValues() {
        return allStandardizedValues;
    }

    public void setAllStandardizedValues(Float allStandardizedValues) {
        this.allStandardizedValues = allStandardizedValues;
    }

    public Float getGoodStandardizedValues() {
        return goodStandardizedValues;
    }

    public void setGoodStandardizedValues(Float goodStandardizedValues) {
        this.goodStandardizedValues = goodStandardizedValues;
    }
}