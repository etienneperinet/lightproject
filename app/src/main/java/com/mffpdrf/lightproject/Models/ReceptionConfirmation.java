package com.mffpdrf.lightproject.Models;

import android.util.Log;

import androidx.annotation.Nullable;

import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;

import org.json.JSONException;
import org.json.JSONObject;

public class ReceptionConfirmation implements DataFormatter {
    private String objectName;
    private String objectID;

    public ReceptionConfirmation(String objectName, String objectID) {
        this.objectName = objectName;
        this.objectID = objectID;
    }

    public ReceptionConfirmation(JSONObject lastObjectSentJSON) {
        try {
            this.objectName = lastObjectSentJSON.getString("objectName");
            this.objectID = lastObjectSentJSON.getString("objectID");
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "LastObjectSent: ", exception);
        }
    }

    public ReceptionConfirmation() {
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        Boolean equals = false;
        ReceptionConfirmation lastObjectSent = (ReceptionConfirmation)obj;
        if (this.objectName.equals(lastObjectSent.objectName) && this.objectID.equals(lastObjectSent.objectID)) {
            equals = true;
        }
        return equals;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getObjectID() {
        return objectID;
    }

    public void setObjectID(String objectID) {
        this.objectID = objectID;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject lastObjectSentJSON = new JSONObject();

        try {
            lastObjectSentJSON.put("JSONType", DatabaseConstants.TABLE_LAST_OBJECT_SENT);
            lastObjectSentJSON.put("objectName", objectName);
            lastObjectSentJSON.put("objectID", objectID);
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "toJSON: ", exception);
        }

        return lastObjectSentJSON;
    }
}