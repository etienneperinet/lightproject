package com.mffpdrf.lightproject.Models.DatabaseModels;

import android.util.Log;

import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.Models.DataFormatter;

import org.json.JSONException;
import org.json.JSONObject;

public class Cellphone implements DataFormatter {
    private String id;
    private Float lightSensorSaturation;

    public Cellphone(String id, Float lightSensorSaturation) {
        this.id = id;
        this.lightSensorSaturation = lightSensorSaturation;
    }

    public Cellphone(JSONObject cellphoneJSON) {
        try {
            this.id = cellphoneJSON.getString("id");
            this.lightSensorSaturation = Float.parseFloat(cellphoneJSON.getString("lightSensorSaturation"));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "Cellphone: ", exception);
        }
    }

    public Cellphone(String id) {
        this.id = id;
    }

    public Cellphone() {
        this.id = null;
        this.lightSensorSaturation = -1f;
    }

    public String getID() {
        return id;
    }

    public Float getLightSensorSaturation() {
        return lightSensorSaturation;
    }

    public void setID(String id) {
        this.id = id;
    }

    public void setLightSensorSaturation(Float lightSensorSaturation) { this.lightSensorSaturation = lightSensorSaturation; }

    @Override
    public JSONObject toJSON() {
        JSONObject cellphoneJSON = new JSONObject();
        try {
            cellphoneJSON.put("JSONType", DatabaseConstants.TABLE_CELLPHONE);
            cellphoneJSON.put("id", id);
            cellphoneJSON.put("lightSensorSaturation", lightSensorSaturation);
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "Cellphone: ", exception);
        }

        return cellphoneJSON;
    }
}