package com.mffpdrf.lightproject.Models.DatabaseModels;

import android.util.Log;

import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.Models.DataFormatter;

import org.json.JSONException;
import org.json.JSONObject;

public class ReferenceLightData implements DataFormatter {
    private Long id;
    private Integer sensorStatus;
    private Float allValues;
    private Float goodValues;
    private Float allStandardizedValues;
    private Float goodStandardizedValues;
    private Integer numberGoodObservations;
    private Integer numberObservations;
    private Float standardDeviationGood;
    private Float standardDeviationAll;
    private Float rotationCellX;
    private Float rotationCellY;

    public ReferenceLightData() { }

    public ReferenceLightData(JSONObject referenceLightDataJSON) {
        try {
            this.id = referenceLightDataJSON.getLong("id");
            this.sensorStatus = referenceLightDataJSON.getInt("sensorStatus");
            this.allValues = Float.parseFloat(referenceLightDataJSON.getString("allValues"));
            this.goodValues = Float.parseFloat(referenceLightDataJSON.getString("goodValues"));
            this.allStandardizedValues = Float.parseFloat(referenceLightDataJSON.getString("allStandardizedValues"));
            this.goodStandardizedValues = Float.parseFloat(referenceLightDataJSON.getString("goodStandardizedValues"));
            this.numberGoodObservations = referenceLightDataJSON.getInt("numberGoodObservations");
            this.numberObservations = referenceLightDataJSON.getInt("numberObservations");
            this.standardDeviationGood = Float.parseFloat(referenceLightDataJSON.getString("standardDeviationGood"));
            this.standardDeviationAll = Float.parseFloat(referenceLightDataJSON.getString("standardDeviationAll"));
            this.rotationCellX = Float.parseFloat(referenceLightDataJSON.getString("rotationCellX"));
            this.rotationCellY = Float.parseFloat(referenceLightDataJSON.getString("rotationCellY"));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "ReferenceDataLight: ", exception);
        }
    }

    public Long getID() {
        return id;
    }

    public void setID(Long id) {
        this.id = id;
    }

    public Integer getSensorStatus() {
        return sensorStatus;
    }

    public void setSensorStatus(Integer sensorStatus) {
        this.sensorStatus = sensorStatus;
    }

    public Float getAllValues() {
        return allValues;
    }

    public void setAllValues(Float allValues) {
        this.allValues = allValues;
    }

    public Float getGoodValues() {
        return goodValues;
    }

    public void setGoodValues(Float goodValues) {
        this.goodValues = goodValues;
    }

    public Integer getNumberGoodObservations() {
        return numberGoodObservations;
    }

    public void setNumberGoodObservations(Integer numberGoodObservations) {
        this.numberGoodObservations = numberGoodObservations;
    }

    public Integer getNumberObservations() {
        return numberObservations;
    }

    public void setNumberObservations(Integer numberObservations) {
        this.numberObservations = numberObservations;
    }

    public Float getStandardDeviationGood() {
        return standardDeviationGood;
    }

    public void setStandardDeviationGood(Float standardDeviationGood) {
        this.standardDeviationGood = standardDeviationGood;
    }

    public Float getStandardDeviationAll() {
        return standardDeviationAll;
    }

    public void setStandardDeviationAll(Float standardDeviationAll) {
        this.standardDeviationAll = standardDeviationAll;
    }

    public Float getRotationCellX() {
        return rotationCellX;
    }

    public void setRotationCellX(Float rotationCellX) {
        this.rotationCellX = rotationCellX;
    }

    public Float getRotationCellY() {
        return rotationCellY;
    }

    public void setRotationCellY(Float rotationCellY) {
        this.rotationCellY = rotationCellY;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject referenceLightDataJSON = new JSONObject();

        try {
            referenceLightDataJSON.put("JSONType", DatabaseConstants.CREATE_TABLE_REFERENCE_LIGHT_DATA);
            referenceLightDataJSON.put("id", id);
            referenceLightDataJSON.put("sensorStatus", sensorStatus);
            referenceLightDataJSON.put("allValues", allValues);
            referenceLightDataJSON.put("goodValues", goodValues);
            referenceLightDataJSON.put("allStandardizedValues", allStandardizedValues);
            referenceLightDataJSON.put("goodStandardizedValues", goodStandardizedValues);
            referenceLightDataJSON.put("numberGoodObservations", numberGoodObservations);
            referenceLightDataJSON.put("numberObservations", numberObservations);
            referenceLightDataJSON.put("standardDeviationGood", standardDeviationGood);
            referenceLightDataJSON.put("standardDeviationAll", standardDeviationAll);
            referenceLightDataJSON.put("rotationCellX", rotationCellX);
            referenceLightDataJSON.put("rotationCellY", rotationCellY);
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "ReferenceDataLight: ", exception);
        }

        return referenceLightDataJSON;
    }

    public Float getAllStandardizedValues() {
        return allStandardizedValues;
    }

    public void setAllStandardizedValues(Float allStandardizedValues) {
        this.allStandardizedValues = allStandardizedValues;
    }

    public Float getGoodStandardizedValues() {
        return goodStandardizedValues;
    }

    public void setGoodStandardizedValues(Float goodStandardizedValues) {
        this.goodStandardizedValues = goodStandardizedValues;
    }
}