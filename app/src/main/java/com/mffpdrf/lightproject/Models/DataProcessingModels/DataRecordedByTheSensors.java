package com.mffpdrf.lightproject.Models.DataProcessingModels;

import android.os.Build;
import androidx.annotation.RequiresApi;
import java.time.Instant;

public class DataRecordedByTheSensors {
    private Float data;
    private Long timeRecorded;
    private Boolean goodData;

    public DataRecordedByTheSensors(Float data, Long timeRecorded, Boolean goodData) {
        this.data = data;
        this.timeRecorded = timeRecorded;
        this.goodData = goodData;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public DataRecordedByTheSensors(Float data, Boolean goodData) {
        this.data = data;
        this.timeRecorded = Instant.now().toEpochMilli();
        this.goodData = goodData;
    }

    public DataRecordedByTheSensors() {

    }

    public void setData(Float data) {
        this.data = data;
    }

    public void setTimeRecorded(Long timeRecorded) {
        this.timeRecorded = timeRecorded;
    }

    public void setGoodData(Boolean goodData) {
        this.goodData = goodData;
    }

    public Float getData() {
        return data;
    }

    public Long getTimeRecorded() {
        return timeRecorded;
    }

    public Boolean getGoodData() {
        return goodData;
    }
}