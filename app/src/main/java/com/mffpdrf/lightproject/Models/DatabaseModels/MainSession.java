package com.mffpdrf.lightproject.Models.DatabaseModels;

import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.Models.DataFormatter;

import org.json.JSONException;
import org.json.JSONObject;

public class MainSession implements DataFormatter {
    private Long id;
    private Project project;
    private Cellphone cellphone;
    private Observer observer;
    private ReferenceSession referenceSession;

    public MainSession(Cellphone cellphone, ReferenceSession referenceSession) {
        this.cellphone = cellphone;
        this.referenceSession = referenceSession;
    }

    public ReferenceSession getReferenceSession() {
        return referenceSession;
    }

    public void setReferenceSession(ReferenceSession referenceSession) {
        this.referenceSession = referenceSession;
    }

    public MainSession(Long id, Project project, Cellphone cellphone, Observer observer, ReferenceSession referenceSession) {
        this.id = id;
        this.cellphone = cellphone;
        this.project = project;
        this.observer = observer;
        this.referenceSession = referenceSession;
    }

    public MainSession(Project project, Cellphone cellphone, Observer observer, ReferenceSession referenceSession) {
        this.cellphone = cellphone;
        this.project = project;
        this.observer = observer;
        this.referenceSession = referenceSession;
    }

    public MainSession(JSONObject mainSessionJSON) {
        try {
            this.id = mainSessionJSON.getLong("id");
            this.project = new Project();
            this.project.setNumber(mainSessionJSON.getString("projectNumber"));
            this.cellphone = new Cellphone();
            this.cellphone.setID(mainSessionJSON.getString("cellphoneID"));
            this.observer = new Observer();
            this.observer.setID(mainSessionJSON.getLong("observerID"));
            this.referenceSession = new ReferenceSession();
            this.referenceSession.setID(mainSessionJSON.getLong("referenceSessionID"));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "Session: ", exception);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public MainSession(Long id, Project project, Cellphone mainCellphone, Observer observer) {
        this.id = id;
        this.cellphone = mainCellphone;
        this.project = project;
        this.observer = observer;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public MainSession(Project project, Cellphone cellphone, Observer observer) {
        this.cellphone = cellphone;
        this.project = project;
        this.observer = observer;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public MainSession(Long id) {
        this.id = id;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public MainSession() {

    }

    public Long getID() {
        return id;
    }

    public Project getProject() {
        return project;
    }

    public Observer getObserver() {
        return observer;
    }

    public void setID(Long id) {
        this.id = id;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public void setObserver(Observer observer) {
        this.observer = observer;
    }

    public Cellphone getCellphone() { return cellphone; }

    public void setCellphone(Cellphone cellphone) { this.cellphone = cellphone; }

    @Override
    public JSONObject toJSON() {
        JSONObject mainSessionJSON = new JSONObject();

        try {
            mainSessionJSON.put("JSONType", DatabaseConstants.TABLE_MAIN_SESSION);
            mainSessionJSON.put("id", id);
            mainSessionJSON.put("projectNumber", project.getNumber());
            mainSessionJSON.put("cellphoneID", cellphone.getID());
            mainSessionJSON.put("observerID", observer.getID());
            mainSessionJSON.put("referenceSessionID", referenceSession.getID());
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "MainSession: ", exception);
        }

        return mainSessionJSON;
    }
}