package com.mffpdrf.lightproject.Models.DatabaseModels;

import android.util.Log;

import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.Models.DataFormatter;

import org.json.JSONException;
import org.json.JSONObject;

public class Observer implements DataFormatter {
    private Long id;
    private String employeeCode;
    private String firstName;
    private String lastName;

    public Observer(Long id, String employeeCode, String firstName, String lastName) {
        this.id = id;
        this.employeeCode = employeeCode;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Observer(String employeeCode, String firstName, String lastName) {
        this.employeeCode = employeeCode;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Observer(Long id, String employeeCode) {
        this.id = id;
        this.employeeCode = employeeCode;
        this.firstName = "";
        this.lastName = "";
    }

    public Observer() {
        this.employeeCode = "";
        this.firstName = "";
        this.lastName = "";
    }

    public Observer(JSONObject observerJSON) {
        try {
            this.id = observerJSON.getLong("id");
            this.employeeCode = observerJSON.getString("employeeCode");
            this.firstName = observerJSON.getString("firstName");
            this.lastName = observerJSON.getString("lastName");
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "Observer: ", exception);
        }
    }

    public Long getID() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setID(Long id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public @Override String toString(){
        return firstName + " " + lastName + " (" + employeeCode + ")";
    }

    @Override
    public JSONObject toJSON() {
        JSONObject observerJSON = new JSONObject();

        try {
            observerJSON.put("JSONType", DatabaseConstants.TABLE_OBSERVER);
            observerJSON.put("id", id);
            observerJSON.put("employeeCode", employeeCode);
            observerJSON.put("firstName", firstName);
            observerJSON.put("lastName", lastName);
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "Observer: ", exception);
        }

        return observerJSON;
    }
}