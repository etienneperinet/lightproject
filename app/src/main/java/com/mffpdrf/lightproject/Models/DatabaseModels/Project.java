package com.mffpdrf.lightproject.Models.DatabaseModels;

import android.util.Log;

import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.Models.DataFormatter;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;

public class Project implements DataFormatter {
    private String number;
    private String name;

    public Project(String number, String name) {
        this.number = number;
        this.name = name;
    }

    public Project(JSONObject projectJSON) {
        try {
            this.number = projectJSON.getString("number");
            this.name = projectJSON.getString("name");
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "Project: ", exception);
        }
    }

    public Project() { }

    public String getNumber() { return number; }

    public String getName() {
        return name;
    }

    public void setNumber(String number) { this.number = number; }

    public void setName(String name) {
        this.name = name;
    }

    public @Override String toString(){
        return number + " - " + name;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject projectJSON = new JSONObject();

        try {
            projectJSON.put("JSONType", DatabaseConstants.TABLE_PROJECT);
            projectJSON.put("number", number);
            projectJSON.put("name", name);
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "Project: ", exception);
        }

        return projectJSON;
    }
}