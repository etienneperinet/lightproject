package com.mffpdrf.lightproject.Models.DatabaseModels;

import android.util.Log;

import com.mffpdrf.lightproject.Constants.DatabaseConstants;
import com.mffpdrf.lightproject.Constants.TagConstants;
import com.mffpdrf.lightproject.Models.DataFormatter;

import org.json.JSONException;
import org.json.JSONObject;

public class ReferenceData implements DataFormatter {
    private Long id;
    private Integer secondsPassed;
    private ReferencePosition referencePosition;

    public ReferenceData(Long id, Integer secondsPassed, Float light, ReferencePosition referencePosition) {
        this.id = id;
        this.secondsPassed = secondsPassed;
        this.referencePosition = referencePosition;
    }

    public ReferenceData(Long id, Integer secondsPassed, Float light) {
        this.id = id;
        this.secondsPassed = secondsPassed;
        this.referencePosition = null;
    }

    public ReferenceData() {
        this.secondsPassed = -1;
        this.referencePosition = null;
    }

    public ReferenceData(JSONObject referenceDataJSON) {
        try {
            this.id = referenceDataJSON.getLong("id");
            this.secondsPassed = referenceDataJSON.getInt("secondsPassed");
            this.referencePosition.setID(referenceDataJSON.getLong("referencePositionID"));
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "ReferenceData: ", exception);
        }
    }

    public Long getID() {
        return id;
    }

    public Integer getSecondsPassed() {
        return secondsPassed;
    }

    public void setID(Long id) {
        this.id = id;
    }

    public void setSecondsPassed(Integer secondsPassed) {
        this.secondsPassed = secondsPassed;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject referenceDataJSON = new JSONObject();

        try {
            referenceDataJSON.put("JSONType", DatabaseConstants.TABLE_REFERENCE_DATA);
            referenceDataJSON.put("id", id);
            referenceDataJSON.put("secondsPassed", secondsPassed);
            referenceDataJSON.put("referencePositionID", referencePosition.getID());
        } catch (JSONException exception) {
            Log.e(TagConstants.JSON_ERROR, "ReferenceData: ", exception);
        }

        return referenceDataJSON;
    }

    public ReferencePosition getReferencePosition() {
        return referencePosition;
    }

    public void setReferencePosition(ReferencePosition referencePosition) {
        this.referencePosition = referencePosition;
    }
}